((js-mode . ((indent-tabs-mode . t)
             (tab-width . 2)
             (js-indent-level . 2)))
 (js2-mode . ((indent-tabs-mode . t)
              (tab-width . 2)
              (js2-basic-offset . 2)
              (js2-bounce-indent-p . t))))

;;; EXAMPLE
;; ((nil . ((indent-tabs-mode . t)
;;          (fill-column . 80)))
;;  (c-mode . ((c-file-style . "BSD")
;;             (subdirs . nil)))
;;  ("src/imported"
;;   . ((nil . ((change-log-default-name
;;               . "ChangeLog.local"))))))
