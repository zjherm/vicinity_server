var token = "d3e680024b61160bb24fb43b4d2590a7de03ed661cba5d182f9880d73aa69016";

/*
db.users.insert({ "_id" : ObjectId("559d418791d6efa5381cc50e"), "fullname" : "Rax Wunter", "nickname" : "rax.wunter", "facebookId" : 10153295093454236, "city" : "Moscow, Russia", "lastCome" : ISODate("2015-07-08T15:28:07.248Z"), "email" : "raxpost@gmail.com", "stripeId" : "cus_6ZTPbrjSR9xcTI", "avatarUrl" : "http://graph.facebook.com/10153295093454235/picture?type=large", "isBot" : false, "created" : ISODate("2015-07-08T15:27:45.455Z"), "pushTokens" : [ "d3e680024b61160bb24fb43b4d2590a7de03ed661cba5d182f9880d73aa69016" ], "__v" : 0 });
WriteResult({ "nInserted" : 1 })
*/

/*
db.user.update({nickname:"rax.wunter"},{$set:{pushTokens:["d3e680024b61160bb24fb43b4d2590a7de03ed661cba5d182f9880d73aa69016"]}});
*/

require("./mocks/logger");
var apn = require("apn");
global.mongoose = require('mongoose');
global.config = require("../app/config");
global.mongoose.connect(global.config.mongodb);
global.mongoose.set('debug', function (coll, method, query, doc) {
    //global.logger.debug("MONGO", util.inspect(query, false, null));
});

var Server = require("../app/server/server");
global.server = new Server();
var subscriber = require("../app/server/subscriber");

subscriber.pusher.push = function (token, notification, cb) {
  global.server.emit("push.mock", token, notification);
};

var Post = require("../app/models/post");
var User = require("../app/models/user");
var thePost;
var theUser;
describe("Pushes", function () {

    before(function (done) {
      Post.findOne({}, function (err, post) {
        if (err) return done(err);
        thePost = post;
        console.log(post.authorId);
        User.findOne({_id: post.authorId}, function (err, user) {
          if (err) return done(err);
          theUser = user;
          done();
        })
      });
    });

    it("push post", function (done) {
        this.timeout(30000);
        global.server.on("push.mock", function (data) {
          console.log(arguments);
          done();
        });
        global.server.emit("push.post", {
            authorId: thePost.authorId,
            actor: theUser,
            postId: thePost._id,
            postText: "hey body i mention you @" + theUser.nickname
        });

        //setTimeout(done, 5000);
    });


});
