var config = require("../app/config");
global.mongoose = require('mongoose');
global.logger = console;
console.debug = console.log;
global.mongoose.connect(config.mongodb);
var User = require("../app/models/user");

describe("user", function () {

    it("non exists nickname", function (done) {
        var my_super_nick = "bitch_better_have_my_money_1907";
        User.megaCheckBlyat(my_super_nick, null, false, function (err, nick) {
            nick.should.equal(my_super_nick);
            done(err);
        });
    });

    it("existing", function (done) {

        User.findOne({}, function (err, user) {

            if (!user) return done(new Error("Create at least one test user"));
            var prev = user.nickname;
            user.setUniqNickname({
                first_name : "Rax",
                second_name : "Wunter"
            }, function (err) {
                console.log(user.nickname);
                user.nickname.should.equal("rax.wunter");
                done();

            });

        });

    })


});