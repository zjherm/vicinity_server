global.config = require("../app/config");
var Worker = require("../app/server/worker");
module.exports = {
    server : function (done) {
        var worker = new Worker(config);
        var connected = 0;
        var cb = function (service) {
            connected++;
            console.log(service, "connected");
            if (connected === 2) {
                done();
            }
        };
        worker.init(cb);
    },
    getURI : function (path, query) {
        return url.format({
            protocol : "http",
            hostname : "127.0.0.1",
            port : global.config.port,
            pathname : "/api" + path,
            query : query
        });
    }
};