var request = require("request");
var async = require("async");
var url;
var lat = 55.755826;
var lon = 37.617300;
var geolib = require("geolib");
var prev = [];
var total = 0;
var key = 'AIzaSyCnjRK5tMZIwXIeMyDM5oWqcpwiXIzzP48';
var token = true;
url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+lat+'%2C' + lon +'&key=' + key + '&pagetoken=&types=airport%7Cart_gallery%7Catm%7Cbakery%7Cbank%7Cbar%7Cbeauty_salon%7Cbook_store%7Ccafe%7Ccampground%7Ccasino%7Ccemetery%7Cchurch%7Ccity_hall%7Cclothing_store%7Ccourthouse%7Cembassy%7Cfood%7Cgym%7Chealth%7Clibrary%7Clodging%7Cmeal_delivery%7Cmeal_takeaway%7Cmovie_theater%7Cmuseum%7Cnight_club%7Cpark%7Cplace_of_worship%7Crestaurant%7Cshopping_mall%7Cspa%7Cstadium%7Cstore%7Cuniversity%7Cveterinary_care%7Czoo&rankby=distance&' + token;
describe("distances", function () {

    it("collect", function (done) {
        this.timeout(0);

        async.whilst(
            function () { return token && ++total < 3 },
            function (callback) {
                request(url, function (err, res, body) {
                    var data = JSON.parse(body);
                    if (data.next_page_token) token = data.next_page_token;
                    else token = false;

                    url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=' + key + '&pagetoken=' + token;
                    prev = prev.concat(data.results.map(function (item) {
                        item.geometry.name = item.name;
                        return item.geometry;
                    }));
                    //console.log(prev);
                    setTimeout(callback, 2000);
                    //callback(err);
                });

            },
            function (err) {
                //console.log(prev);
                done(err);
            }
        );
    });


    it("should be in order", function () {
        var p;
        prev.forEach(function (item) {
            var d = geolib.getDistance(
                {latitude: lat, longitude: lon},
                {latitude: item.location.lat, longitude: item.location.lng}
            );

            console.log(d, item.name);


        });

    })

});