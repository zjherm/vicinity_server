require("./mocks/logger");
var assert = require("assert");
var Worker = require("../app/server/worker");
var request = require("request");
var url = require("url");
var util = require("util");
global.config = require("../app/config");

var json = require("../app/config/swagger.json");

data = {};

var handleData = {
    "locations" : {
        lat: 44,
        lon: 44
    },
    "createPost" : {
        lat: 44,
        lon: 44
    },
    "getPosts" : {
        lat: 44,
        lon: 44
    }
};

Object.keys(json.paths).forEach(function (path) {
    Object.keys(json.paths[path]).forEach(function (method) {
        var item = json.paths[path][method];
        //console.log("ITEM", item, json.paths[path], method);
        var params = item.parameters.reduce(function (prev, p) {
            if (p.required) {
                prev[p.name] = p.defaultValue || "test";
            }
            return prev;
        }, {});
        data[path+"|"+method] = {
            path: path,
            method: method.toUpperCase(),
            data: handleData[item.operationId] || params,
            operationId: item.operationId,
            model : item.responses["200"].schema
        };
    });
});

console.log(data);

var getURI = function (path, query) {
    return url.format({
        protocol : "http",
        hostname : "127.0.0.1",
        port : global.config.port,
        pathname : "/api" + path,
        query : query
    });
};
var sessionId;
var deffered = {};
var ignoreList = {
    gender:1
};

var replaceDeferred = function (item) {
    console.log("Comes ", item, deffered);
    Object.keys(item).forEach(function (field) {
        if (deffered[field]) item[field] = deffered[field];
    });
};

var getNested = function (schema) {
    if (!schema["$ref"]) console.log(schema);
    var ref = schema["$ref"].replace("#/definitions/", "");
    return json.definitions[ref];
};

var validate = function (respData, schema) {
    if (!respData) return console.warn("No data for schema", schema);
    if (schema.type) return checkPref(schema); // solid schema
    if (schema.$ref) schema = getNested(schema); // solid schema

    var props = schema.properties;

    if (!props) throw new Error(util.inspect(schema));

    Object.keys(props).forEach(function (prop) {
        var pref = props[prop];
        checkPref(pref, prop);
    });

    function checkPref(pref, prop) {
        var dataPiece = respData[prop];
        if (pref.$ref) return validate(dataPiece, getNested(pref));

        if (ignoreList[prop]) return;

        if (dataPiece === undefined) {
            throw new Error("No `" + prop + "` field in response." + util.inspect(respData));
        }
        if (pref.type === "array") {
            assert.ok(Array.isArray(dataPiece));
            if (pref.items && pref.items.$ref) {
                validate(dataPiece[0], getNested(pref.items));
            }
        } else if (dataPiece === null) {

        } else {
            if (typeof dataPiece !== pref.type) throw new Error("Type `" + typeof dataPiece + "` in " + prop + " is invalid. Expected " + pref.type);
        }
    }

};

describe("swagger paths", function () {

    it("run", function (done) {
        //this.timeout(500);
        var worker = new Worker(config);
        var connected = 0;
        var cb = function (service) {
            connected++;
            console.log(service, "connected");
            if (connected === 2) {
                done();
            }
        };
        worker.init(cb);
    });

    Object.keys(data).forEach(function (key) {
        var item = data[key];
        it(item.method + " " + item.path, function (done) {

            var headers = {
                "Content-type" : "application/x-www-form-urlencoded"
            };

            if (sessionId) {
                headers["Authorization"] = sessionId;
            }

            var opts = {
                uri : getURI(item.path),
                method : item.method.toUpperCase(),
                headers: headers
            };

            replaceDeferred(item.data);
            console.log(item.method + " " + item.path + " ", item.data);

            if (item.method !== "GET") opts.form = item.data;
            else opts.uri = getURI(item.path, item.data);

            //console.log(opts);
            request(opts, function (err, response, body) {
                //console.log(body);
                body = JSON.parse(body);
                if (err) return done(err);
                if (body.error) return done(new Error(body.error.code + ":" + body.error.message));

                validate(body, item.model);

                if (body.data.sessionId) {
                    sessionId = body.data.sessionId;
                }
                if (item.operationId === "getPosts") {
                    deffered["postId"] = body.data[0]._id;
                }

                done();

            });

        });
    });




});