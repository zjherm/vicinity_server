var config = require("../app/config");
global.mongoose = require('mongoose');
global.logger = console;
console.debug = console.log;
global.mongoose.connect(config.mongodb);
var Post = require("../app/models/post");

describe("Exposing magic", function () {

	it("ok..", function (done) {
		Post.findOne({_id: "55acf312e444e8480e13f5ac"}, function (err, post) {
			if (!post) throw new Error("no post");
			(post.likes.length > 0).should.be.true;
			done();
		});
	});

});