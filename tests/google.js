var config = require("../app/config");
var Google = require("../app/wrappers/gplaces");
global.logger = console;
console.debug = console.log;

describe("google places", function () {

    it("query", function (done) {
        var g = new Google({
            apiKey: config.googlePlaces.apiKey
        });
        g.query(55.755826, 37.617300, "basil", null, function (err, results, token) {
            results.should.be.ok;
            done();

        });
    });

    it("near", function (done) {
        var g = new Google({
            apiKey: config.googlePlaces.apiKey
        });
        g.near(55.755826, 37.617300, null, function (err, results, token) {
            console.log(results);
            results.should.be.ok;
            done();

        });
    })


});



var installApp = app.directive("installApp", function (){
    return {
        restrict: 'E',
        scope : {},
        templateUrl : 'js/directives/installApp.html',
        link : function(scope, element, attrs) {
            scope.buttonText = "Install",
                scope.installed = false,

                scope.download = function() {
                    element.toggleClass('btn-active');
                    if(scope.installed) {
                        scope.buttonText = "Install";
                        scope.installed = false;
                    } else {
                        scope.buttonText = "Uninstall";
                        scope.installed = true;
                    }
                }
        }
    }
});