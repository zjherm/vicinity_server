var async = require("async");
var Schema = global.mongoose.Schema;
var util = require("util");
var _ = require('lodash');

var RestorePasswordTokenSchema = new Schema({
		userId: Schema.Types.ObjectId,
		token: String,
		confirmed: {type: Boolean, default: false},
		created: {type: Date, default: new Date}
	}
);

//EmailConfirmationTokenSchema.index(
//	{created: 1},
//	{expireAfterSeconds: 3600}
//);

var RestorePasswordToken = mongoose.model('RestorePasswordToken', RestorePasswordTokenSchema);

module.exports = RestorePasswordToken;
