var _ = require('lodash');
var Schema = global.mongoose.Schema;
var Comment = require("./comment");
var async = require("async");
var stopwords = require("../util/stopwords");
var Gplaces = require("../wrappers/gplaces");
var Place = require("./place");
var User = require("./user");
var Notification = require("./notification");

var PostSchema = new Schema({
	"location": {
		type: {type: String},
		coordinates: [Number]
	},
	"ts": {type: Date},
	"photo": {type: String},
	"companyId": Schema.Types.ObjectId,
	"authorId": {type: Schema.Types.ObjectId, ref: 'User'},
	"description": {type: String},
	"likes": [Schema.Types.ObjectId],
	"wheneverLiked": [Schema.Types.ObjectId],
	"viewedUsers": [Schema.Types.ObjectId],
	"isCheckedIn": {type: Boolean},
	"comments": [Comment.schema],
	"subscribers": [Schema.Types.ObjectId],
	"unsubscribers": {type: [Schema.Types.ObjectId], default: []},
	"placeId": {type: String}, // google place id
	"facebookPostId": {type: String},
	"facebookPlaceId": {type: Number},
	"isPostedOnFacebook": {type: Boolean},
	"facebookPostType": {type: String, default: 'post'},
	"deleted": { type: Number, default: 0}, // bit mask 0 - not deleted, 10 - author deleted, 01 - post deleted, 11 - deleted both
	"isLandscapePhoto": {type: Boolean, default: false},
	"notifiedUsers": {type: [Schema.Types.ObjectId], default: []},
	"isDeal": {type: Boolean, default: false},
});

PostSchema.index({location: '2dsphere'});
PostSchema.index({description: 'text'});

//PostSchema.post('save', function (doc) {
//	console.log('post save');
//	console.log(this);
//	console.log(arguments);
//});
//PostSchema.post('update', function (next) {
//	console.log('post update');
//	console.log(this);
//	console.log(arguments);
//	next()
//});
//PostSchema.post('findOneAndUpdate',function (next) {
//	console.log('post findOneAndUpdate');
//	console.log(this);
//	console.log(arguments);
//	next()
//})

var Post = mongoose.model('Post', PostSchema);

Post.isNotDeletedExpr = function() {
	return { $in: [0, null] };
};

Post.isDealExpr = function(isDeal) {
	return isDeal
		? true
		: { $in: [0, null] };
};

Post.updateNotifications = function ( currentUser = {}, postId, cb) {
	Notification.update({ postId: postId,
												"receivers.userId": currentUser.user._id,
												"receivers.viewed": false },
											{$set :{ "receivers.$.viewed": true}},
											{ multi: true },
											cb);
};


Post.nearby = function (deals, lat, lon, distance, currentUser = {}, cb) {
    if(deals === undefined) {
        var posts =	Post.find({
            location: {
                $near: {
                    $geometry: {type: "Point", coordinates: [lon, lat]},
                    $maxDistance: parseInt(distance) || 500
                }
            },
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr(),
        }).sort({
            ts: -1
        }).exec(cb);
    } else {
        var posts =	Post.find({
            location: {
                $near: {
                    $geometry: {type: "Point", coordinates: [lon, lat]},
                    $maxDistance: parseInt(distance) || 500
                }
            },
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr(),
            isDeal: Post.isDealExpr(deals),
        }).sort({
            ts: -1
        }).exec(cb);
    }
};

Post.hashtag = function (deals, hashtag, currentUser = {}, cb) {
    if(deals === undefined) {
        hashtag = stopwords.getHashtag(hashtag);
        var posts = Post.find({
            $text: {
                $search: hashtag
            },
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr(),
        }).sort({
            ts: -1
        }).exec(function (err, results) {
            if (err) return cb(err);
            results = results.filter(function (item) {
                return item.description.indexOf(hashtag) !== -1;
            });
            cb(null, results);
        });
    } else {
        hashtag = stopwords.getHashtag(hashtag);
        var posts = Post.find({
            $text: {
                $search: hashtag
            },
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr(),
            isDeal: Post.isDealExpr(deals),
        }).sort({
            ts: -1
        }).exec(function (err, results) {
            if (err) return cb(err);
            results = results.filter(function (item) {
                return item.description.indexOf(hashtag) !== -1;
            });
            cb(null, results);
        });
    }
};

Post.findByAuthorId = (deals, authorId, cb) => {
    if(deals === undefined) {
        Post.find({
            authorId: authorId,
            deleted: Post.isNotDeletedExpr(),
        }).sort({
            ts: -1
        }).exec(cb);
    } else {
        Post.find({
            authorId: authorId,
            deleted: Post.isNotDeletedExpr(),
            isDeal: Post.isDealExpr(deals),
        }).sort({
            ts: -1
        }).exec(cb);
    }


};

Post.findLikedByAuthorId = (deals, authorId, cb) => {
    if(deals === undefined) {
        Post.find({
            likes: authorId,
            deleted: Post.isNotDeletedExpr(),
        }).sort({
            ts: -1
        }).exec(cb);
    } else {
        Post.find({
            likes: authorId,
            deleted: Post.isNotDeletedExpr(),
            isDeal: Post.isDealExpr(deals),
        }).sort({
            ts: -1
        }).exec(cb);
    }
};

Post.getByPlaceId = (deals, gplaceId, currentUser = {}, cb) => {

	if(deals === undefined) {
        Post.find({
            placeId: gplaceId,
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr()
        }).sort({
            ts: -1
        }).exec(cb);
	} else {
        Post.find({
            placeId: gplaceId,
            authorId: {
                $nin: currentUser.blockedUsers
            },
            deleted: Post.isNotDeletedExpr(),
            isDeal: Post.isDealExpr(deals)
        }).sort({
            ts: -1
        }).exec(cb);
	}
};

// оберточка хуерточка
Post.prototype.getPublicObject = function (req, cb) {

	// subscribed ?
	var session = req.session;
	var userId = req.session.userId
	var lat = req.query.lat;
	var lon = req.query.lon;
	var o = this.toObject();
	delete o.__v;
	o.location = {
		lon: this.location.coordinates[0],
		lat: this.location.coordinates[1]
	};
	o.likes = this.likes.length;

	o.viewed = o.viewedUsers.map(function (oid) {
			return oid.toString();
		}).indexOf(req.session.userId) !== -1;
	delete o.viewedUsers;
	delete o.notifiedUsers;

	o.isLiked = this.likes.indexOf(session.userId) !== -1;
	o.ts = Math.round(o.ts.getTime() / 1000);
	o.description = stopwords.normalize(o.description);
	o.subscribed = !!_.find(o.subscribers, id => String(id) === String(userId) );

	async.parallel([
		(_cb) => {
			if (!o.authorId) return _cb();
			User.findOne({_id: o.authorId}, function (err, user) {
				if (err) return cb(err);
				if (!user) return _cb("Post author hasn't found. Inconsistent database state");
				o.author = user.getPublicObject();
				delete o.authorId;
				_cb();
			});
		},
		(_cb) => {
			var g = new Gplaces(global.config.googlePlaces);
			if (o.placeId && o.placeId.length && o.placeId.length > 3) {
				Place.getByGooglePlaceId(o.placeId, function (err, place) {
					if (err) return _cb(null);
					if (place) {
						place.withDistance({
							lat: lat,
							lon: lon
						});
						o.place = place;
					} else {
						o.place = {}
					}
					_cb(null, o);
				});
			} else {
				global.logger.error(`No placeId in post _id: ${o._id}, placeId: ${o.placeId}`)
				o.place = {}
				return _cb(null, null);
			}
		},
		(_cb) => {
			o.commentsNum = o.comments.length;
			var coms = null;
			if (!req.returnAllComments) {
				var activeComments = _.filter(o.comments, function(com) { return !com["status"] || com.status == "Active"; });
				coms = activeComments.slice(activeComments.length - 2, activeComments.length);
				o.commentsNum = activeComments.length;
			} else {
				coms = o.comments;
			}

			if (session.userStatus == "Locked") {
				o.commentsNum = 0;
				coms = [];
			}
			async.map(coms, function (rawData, mapNext) {
				var comment = new Comment(rawData);
				comment.getPublicObject(mapNext);
			}, function (err, comments) {
				o.comments = comments;
				_cb(err);
			});
		}

	], function (err) {
		//if (err) return cb(null, o) // fixing broken feed, if one is broken
		cb(null, o);
	});

};

module.exports = Post;
