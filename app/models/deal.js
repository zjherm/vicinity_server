var geolib = require("geolib");
var async = require("async");
var _ = require('lodash')
var Schema = global.mongoose.Schema;
var Notification = require("./notification");
var util = require("util");
var Gplaces = require('../wrappers/gplaces');
var geocoder = require('../wrappers/geocoder');
var Company = require('./company');


var dealSchema = new Schema({

	title: {required: true, type: String, default: ""},
	description: {required: false, type: String, default: ""},
	startTime: {required: true, type: Date, default: null, get: getTimeStamp},
	endTime: {required: true, type: Date, default: null, get: getTimeStamp},
	imageUrl: {type: String, default: ""},
	type: {type: Number},
	price: {type: String, default: ""},
	_company: {type: Schema.Types.ObjectId, ref: 'Company'},
	location: {
		type: {type: String},
		coordinates: [Number]
	},
	distance: {type: Number, default: null},
	updatedAt: {type: Date, default: null, get: getTimeStamp},
	createdAt: {type: Date, default: Date.now, get: getTimeStamp}

}, {
	id: false,
	versionKey: false,

	toObject: {
		getters: true,
		setters: true,
		virtuals: true,
		minimize: false,
		retainKeyOrder: true,
		transform: dealToObject
	},
	toJSON: {
		getters: true,
		setters: true,
		virtuals: true,
		minimize: false,
		retainKeyOrder: true,
		transform: dealToObject
	}
});
dealSchema.index({location: "2dsphere"});
dealSchema.index({title: "text"});
dealSchema.index({description: "text"});

dealSchema.post('update', function () {
	this.update({}, {
		$set: {
			updatedAt: new Date(),
		}
	});
});

var Deal = mongoose.model('Deal', dealSchema);

var createDeal = Deal.create.bind(Deal)

Deal.create = (params, cb) => {
	console.log('Deal.create');
	var deal = new Deal(params);
	deal.save((err, deal) => {
		if (err) return cb(err);
		var companyId = deal._company
		Company.findOneAndUpdate({
			_id: companyId
		}, {
			$addToSet: {
				deals: deal._id
			}
		}, (err, company) => {
			if (err) return cb(err);

			cb(err, deal);

		})
	})
}

Deal.search = function (searchParams, cb) {
	searchParams || (searchParams = {})
	var lat = searchParams.lat;
	var lon = searchParams.lon;
	var distance = searchParams.distance;
	var limit = searchParams.limit;
	var offset = searchParams.offset;

	if (lat && lon) return Deal.getByLocation(lat, lon, distance, limit, offset, cb)
	if (searchParams.companyId) return Deal.getByCompanyId(searchParams.companyId, limit, offset, cb)

	Deal
		.find(searchParams)
		.skip(offset)
		.limit(limit)
		.populate('_company', '-deals')
		.exec(cb)
}

Deal.getById = function (dealId, cb) {
	Deal.findOne({_id: dealId})
		.populate('_company', '-deals')
		.exec(cb);
}

Deal.getByCompanyId = (companyId, limit, offset, cb) => {
	Deal
		.find({_company: companyId})
		.skip(offset)
		.limit(limit)
		.populate('_company', '-deals')
		.exec(cb)
};

Deal.getByLocation = function (lat, lon, distance, limit, offset, cb) {
	distance -= 0;
	lat -= 0;
	lon -= 0;
	limit -= 0;
	offset -= 0;
	if (!distance || distance < 0)    distance = 0;
	if (!limit || limit < 0)        limit = 20;
	if (!offset || offset < 0)        offset = 0;

	var point = {
		type: "Point",
		coordinates: [lon, lat]
	};
	var nearOptions = {
		query: {},
		near: point,
		maxDistance: distance, // in meters
		spherical: true,
		distanceField: "distance"
	}
	Deal
		.aggregate()
		.near(nearOptions)
		.skip(offset)
		.limit(limit)
		.exec(function (err, deals) {
			if (err) return cb(err)
			deals = deals.map(deal => (new Deal(deal)))
			Deal.populate(deals, {path: '_company', select: '-deals'}, (err, deals) => {
				if (err) return cb(err)
				cb(null, deals)
			})
		});
}

Deal.__remove = function (dealId, cb) {
	Deal.remove({id: dealId}, cb);
};

function dealToObject(doc, ret, options) {
	if (!ret.updatedAt) delete ret.updatedAt
	if (_.isNull(ret.distance)) delete ret.distance
	ret.location = {
		lon: ret.location.coordinates[0],
		lat: ret.location.coordinates[1]
	}

	return ret
}

function getTimeStamp(val) {
	if (!val) return 0
	return +(new Date(val))
}

module.exports = Deal;
