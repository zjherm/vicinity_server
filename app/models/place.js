var _ = require('lodash');
var geolib = require("geolib");
var Gplaces = require('../wrappers/gplaces');
var geocoder = require('../wrappers/geocoder');
var Schema = global.mongoose.Schema;
var PlaceSchema = new Schema({
		"location": {lat: {type: Number}, lon: {type: Number}},
		"name": {type: String},
		"land": {type: String},
		"placeId": {type: String},
		"type": {type: String},
		rating: {
			type: Number, max: 5, default: 0, set: (val) => {
				if (!!!val || _.isNaN(+val)) return 0
				return val
			}
		},
		"address": {type: String},
		"distance": {type: Number},
		"fake": {type: Boolean, default: false},
		"companyId": Schema.Types.ObjectId,
		__v: {type: Number, select: false}
	},
	{
		versionKey: false
	}
);

PlaceSchema.index({placeId: 1});
//PlaceSchema.index({location: '2dsphere'});

var Place = mongoose.model('Place', PlaceSchema);

/**
 * Append field `distance` calculated from o.location and given location
 * @param o
 * @param location
 */
var appendDistance = function (o, location) {
	if (location && location.lat && location.lon) {
		o.distance = geolib.getDistance(
			{latitude: location.lat, longitude: location.lon},
			{latitude: o.location.lat, longitude: o.location.lon}
		);
	} else {
		o.distance = null;
	}
};

/**
 * Create instance from google data (!sync)
 * @param googleData
 * @param location
 * @return Object new instance
 */
Place.fromGoogle = function (googleData, location) {
	var o = {};

	o.location = {
		lat: googleData.geometry.location.lat,
		lon: googleData.geometry.location.lng
	};
	o.name = googleData.name;
	o.land = googleData.land; // additional not native field
	o.placeId = googleData.place_id || null;
	o.type = Place.getType(googleData.types || null);
	o.address = googleData.vicinity || googleData.formatted_address || null;
	if (googleData.rating) {
		o.rating = googleData.rating;
	} else {
		let reviews = googleData.reviews || [];
		o.rating = +(_.reduce(reviews, (p, n) => p + n.rating, 0) / reviews.length ).toFixed(2);
	}

	appendDistance(o, location);

	return new Place(o);

};

Place.fromCompany = function (companyData) {
	var o = {};
	companyData.location || (companyData.location = {})
	o.location = {
		lat: companyData.location.lat,
		lon: companyData.location.lon
	};
	o.name = companyData.companyName;
	o.land = companyData.regionName;
	o.placeId = companyData.googlePlaceId || null;
	o.type = Place.getType(companyData.types || null);
	o.rating = companyData.rating || null;
	o.address = companyData.address || null;
	o.distance = companyData.distance || 0;
	o.companyId = companyData._id || null;

	return new Place(o);
};

Place.getType = function (gTypes) {
	if (!gTypes) return null;
	return "restaurants";
};

Place.prototype.withDistance = function (location) {
	appendDistance(this, location);
};

/**
 * Getting google place by id with caching
 * @param placeId
 * @param cb
 */
Place.getByGooglePlaceId = function (placeId, cb) { // TODO implement this with caching
	var self = this;
	Place.findOne({placeId: placeId}, function (err, place) {
		if (err) return cb(err);

		if (place) {
			cb(null, place);
		} else {
			var gPlaces = new Gplaces(global.config.googlePlaces);
			gPlaces.getByPlaceId(placeId, function (err, gPlaceObj) {
				if (err) return cb(err)

				if (gPlaceObj && gPlaceObj.address_components) {

					gPlaceObj.land = geocoder.parseRegionName(gPlaceObj.address_components);
					var gplace = Place.fromGoogle(gPlaceObj)
					cb(null, gplace);

				} else if (gPlaceObj) {
					geocoder.getCity(gPlaceObj.geometry.location.lat, gPlaceObj.geometry.location.lng, (err, landName) => {
						gPlaceObj.land = landName;
						var gplace = Place.fromGoogle(gPlaceObj)
						cb(null, gplace);
					});
				} else {
					cb(null, null);
				}
			})
		}
	});
}

module.exports = Place;
