var async = require("async");
var Schema = global.mongoose.Schema;
var util = require("util");
var _ = require('lodash');

var EmailConfirmationTokenSchema = new Schema({
		userId: Schema.Types.ObjectId,
		token: String,
		confirmed: {type: Boolean, default: false},
		restored: {type: Boolean, default: false},
		created: {type: Date, default: new Date},
		email: String
	}
);

//EmailConfirmationTokenSchema.index(
//	{created: 1},
//	{expireAfterSeconds: 3600}
//);

var EmailConfirmationToken = mongoose.model('EmailConfirmationToken', EmailConfirmationTokenSchema);

module.exports = EmailConfirmationToken;
