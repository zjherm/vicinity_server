var _ = require('lodash');
var Schema = global.mongoose.Schema;

var NotificationSchema = new Schema({
	ts : {type: Date, default: Date.now},
	text : {type: String},
	eventText : {type: String},
	actorId : {type: Schema.Types.ObjectId},
	actor : Schema.Types.Mixed,
	postId : {type: Schema.Types.ObjectId},
	notifType : {type: String},
	friendId : {type: Schema.Types.ObjectId},

	receivers : [{
		userId: {type: Schema.Types.ObjectId},
		viewed : {type: Boolean, "default": false}
	}]
});

/* Proper indexes */
NotificationSchema.index({"actorId": 1}); /* TS here? */
NotificationSchema.index({"receivers.userId": 1, ts: -1});
NotificationSchema.index({"receivers.viewed": 1, "receivers.userId": 1, ts: -1 });

var Notification = mongoose.model('Notification', NotificationSchema);

Notification.getPublicObject = function (notif, userId, cb) {
	if (_.isFunction(userId)) {
		cb = userId;
		userId = null;
	}

	var notificationObject = notif.toObject();
  notificationObject.actor = notificationObject.actor || {};
	notificationObject.actor.userId = notificationObject.actorId;

	notificationObject.ts = Math.round(notificationObject.ts.getTime() / 1000);
	notificationObject.eventText = notificationObject.eventText.replace("%user%", ( notificationObject.actor.nickname ? notificationObject.actor.nickname : notificationObject.actor.fullname));

	notificationObject.wasRead = false;
	if (userId && _.isArray(notificationObject.receivers)) {
		let receiver = _.find(notificationObject.receivers, x => x.userId.equals(userId));
		if (receiver) {
        notificationObject.wasRead = receiver.viewed;
    }
	}

	delete notificationObject.receivers;
	delete notificationObject.actorId;

	cb(null, notificationObject);
};

Notification.findOrCreate = function (query, skeleton, cb) {
	Notification.findOne(query, (err, obj) => {
		if (err || obj) {
			cb(err, obj);
		}
		else {
			obj = new Notification(skeleton);
			cb(null, obj); // Yes, we return it as is unsaved
		}
	});
};

/* TODO: Improve this */
Notification.skel = function (param, cb) {
	return param;
};

Notification.make = function (param, cb) {
	var skel = Notification.skel(param);
	var makeAndSave = () => {
		var obj = new Notification(skel);
		obj.save((err) => {
			cb(err, obj);
		});
	};

	if (skel.actorId && !skel.actor) {
		var User = require('./user');

		User.getById(skel.actorId, (err, user) => {
			if (user) {
				skel.actor = user.getPublicObject();
			} else {
				skel.actor = {
					_id: skel.actorId
				};
				global.logger.error("Actor user not found", skel.actorId, "; err =", err);
			}
			makeAndSave();
		});
	}
	else {
		makeAndSave();
	}
};

Notification.prototype.getPayload = function () {
	var o = this.toObject();
	var userName = "Someone";
	if (o.actor.nickname)
		userName = o.actor.nickname;
	else if (o.actor.fullname)
		userName = o.actor.fullname;

	return {
		postId: o.postId,
		actorId: o.actor._id,
		type: o.notifType,
		text: o.text,
		commentText: o.eventText.replace("%user%", userName),
		friendId: o.friendId,
		actor: {
			_id: o.actor._id,
			nickname: o.actor.nickname ? o.actor.nickname : ""
		}
	};
};

Notification.removeByPostID = function (postId, cb) {
	Notification.remove({ postId: postId }, cb);
};

module.exports = Notification;
