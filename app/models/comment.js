var Schema = global.mongoose.Schema;
var User = require("./user");

var commentStatuses = ['Active', 'Removed', 'Deleted'];

var CommentSchema = new Schema({
    userId : {type: Schema.Types.ObjectId, ref: 'User'},
    ts : {type: Date, default: Date.now},
    text : {type: String},
    status: {type: String, default: commentStatuses[0], enum: commentStatuses}
});

var Comment = mongoose.model('Comment', CommentSchema);

Comment.prototype.getPublicObject = function (cb) {
    var o = this.toObject();
    User.findOne({_id: this.userId}, function (err, user) {
        if (err)
            return cb(err);

        if(user.userStatus == "Deleted")
            o.status = "Deleted";

        o.user = user && user.getPublicObject();
        o.ts = Math.round(o.ts.getTime() / 1000);

        delete o.userId;
        cb(null, o);
    });
};

module.exports = Comment;