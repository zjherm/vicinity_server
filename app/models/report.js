var Schema = global.mongoose.Schema;
var async = require("async");
var User = require("./user");

var reportTypes = [
    'InappropriateContent', 
    'RudeOrAbusive', 
    'SendingSpam', 
    'Scammer',
    'SpamScam',
    'ExplicitContent',
    'Abusive'
];

var reportTargets = [
    'user', 
    'post', 
    'comment'
];

var reportStatuses = ['opened', 'resolved'];

var ReportSchema = new Schema({
    ts: {type: Date, default: Date.now},
    senderId: {type: Schema.Types.ObjectId, ref: 'User'},
    userId: {type: Schema.Types.ObjectId, ref: 'User'},
    reportType: {type: String, enum: reportTypes},
    status: {type: String, default: reportStatuses[0], enum: reportStatuses},
    target: {type: String, enum: reportTargets},
    postId: {type: Schema.Types.ObjectId, ref: "Post"},
    commentId: {type: Schema.Types.ObjectId}
},
    {
        toJSON: {virtuals: true}
    }
);

// dataContext for user & post targets
ReportSchema.virtual('dataContext').get(function () {
         if(this["target"]) {
             if (this.target == "post") // show post description
                 return (this["postId"] && this.postId["description"]) ? this.postId.description : "";
             else if(this.target == "user") // show reported user name
                 return (this["userId"] && this.userId["fullname"]) ? this.userId.fullname : "";
         }

        return "";
    });

var Report = mongoose.model('Report', ReportSchema);

Report.prototype.getPublicObject = function (req, cb) {
    var o = this.toObject();
    o.ts = Math.round(o.ts.getTime() / 1000);

    async.parallel([
        (_cb) => {
            if (!o.senderId) return _cb();
            User.findOne({_id: o.senderId}, function (err, user) {
                if (err) return cb(err);
                if (!user) return _cb("Report sender hasn't found. Inconsistent database state");
                o.sender = user.getPublicObject();
                delete o.senderId;
                _cb();
            });
        },
        (_cb) => {
            if (!o.userId) return _cb();
            User.findOne({_id: o.userId}, function (err, user) {
                if (err) return cb(err);
                if (!user) return _cb("Report sender hasn't found. Inconsistent database state");
                o.user = user.getPublicObject();
                delete o.userId;
                _cb();
            });
        }
    ], function (err) {
        if (err) return cb(err)
        o.reportText = o.sender.fullname + " reported about " + o.user.fullname + " (" + o.reportType + ")";
        cb(null, o);
    });
};

// attach dataContext for comment target
Report.prototype.toDetailsObject = function (comment) {
    var res = this.toObject();
    if(comment) {
        res.commentId = comment;
        res.dataContext = comment.text;
    }
    else { // deleted comment?
        res.commentId = null;
        res.dataContext = "";
    }

    return res;
};

module.exports = Report;
