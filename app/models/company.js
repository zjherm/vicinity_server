var geolib = require("geolib");
var async = require("async");
var _ = require('lodash')
var Schema = global.mongoose.Schema;
var Notification = require("./notification");
var util = require("util");
var Gplaces = require('../wrappers/gplaces');
var geocoder = require('../wrappers/geocoder');

var periodSchema = new Schema({
	"open": {
		"day": {type: Number, default: 0},
		"time": {type: String, default: "0000"},
	},
	"close": {
		"day": {type: Number, default: 0},
		"time": {type: String, default: "0000"},
	}
}, {_id: false, id: false})

var ratingSchema = new Schema({
	count: {type: Number, default: 0}

}, {_id: false, id: false});

var companySchema = new Schema({
	companyName: {type: String, default: "", required: true},
	address: {type: String, default: "", required: true},
	email: {type: String, default: ""},
	categoryId: {type: Number},
	defaultLiveDealImage: {type: String, default: ""},
	tariff: {type: Number, min: 0, max: 4},
	phone: {type: String, default: ""},
	priceLevel: {type: Number, min: 0, max: 4, default: 0},
	password: {type: String, default: "", select: false},
	adminName: {type: String, default: "", select: false},
	webSiteUrl: {type: String, default: ""},
	googlePlaceId: {type: String, default: ""},			// (if company populated from Google Places)
	location: {type: {type: String}, coordinates: [Number]},
	regionName: {type: String, default: ""},
	avatarUrl: {type: String, default: ""},
	galleryArray: [String],			// (each company will have gallery with photos, not exist at current UI. Photos will be populated from Google Places if selected, if not then business owner can add/edit Gallery on the Web App)
	openingHours: {
		"openNow": {type: Boolean, default: false},
		"periods": [periodSchema]
	},
	filledWithGoogle: {type: Boolean, default: false},
	rating: {type: Number, max: 5, default: 0, set: (val) => {
		if (!!!val || _.isNaN(+val)) return 0
		return val
	}},
	userRatingsTotal: Number,
	distance: {type: Number, default: null},
	deals: [{type: Schema.Types.ObjectId, ref: 'Deal'}],
	updatedAt: {type: Date, default: null, get: getTimeStamp},
	createdAt: {type: Date, default: Date.now, get: getTimeStamp}
}, {
	id: false,
	versionKey: false,

	toObject: {
		getters: true,
		setters: true,
		virtuals: true,
		minimize: false,
		retainKeyOrder: true,
		transform: companyToObject
	},
	toJSON: {
		getters: true,
		setters: true,
		virtuals: true,
		minimize: false,
		retainKeyOrder: true,
		transform: companyToObject
	}
});

companySchema.index({googlePlaceId: 1});
companySchema.index({location: "2dsphere"});
companySchema.index({companyName: "text"});
companySchema.post('update', function () {
	this.update({}, {
		$set: {
			updatedAt: new Date(),
		}
	});
});

var Company = mongoose.model('Company', companySchema);

Company.search = function (searchParams, cb) {
	searchParams || (searchParams = {})
	var gPlaceId = searchParams.googlePlaceId;
	var lat = searchParams.lat;
	var lon = searchParams.lon;
	var distance = searchParams.distance;
	var limit = searchParams.limit;
	var offset = searchParams.offset;
	if (gPlaceId) return Company.getByPlaceIdWithCatching(gPlaceId, cb)

	if (lat && lon) return Company.getByLocation({lat, lon, distance, limit, offset}, cb)

	cb()
}

Company.getById = function (companyId, cb) {
	Company.findOne({_id: companyId})
		//.populate('deals')
		.exec(cb);
}

Company.getByPlaceId = function (gPlaceId, cb) {

	/* Find and return existing
	 * or
	 * create and return new one
	 * */

	Company.findOne({googlePlaceId: gPlaceId}, function (err, company) {
		if (err) return cb(err);

		if (company) {
			cb(null, [company]);
		} else {
			var gPlaces = new Gplaces(global.config.googlePlaces);
			gPlaces.getByPlaceId(gPlaceId, function (err, gPlaceObj) {
				if (err) return cb(err)
				var gCompany = Company.fromGoogle(gPlaceObj)
				cb(err, [gCompany]);
			})
		}
	});
};

Company.getByPlaceIdWithCatching = function (gPlaceId, cb) {
	Company.findOne({googlePlaceId: gPlaceId}, function (err, company) {
		if (err) return cb(err);
		if (company) {
			cb(null, [company]);
		} else {
			var gPlaces = new Gplaces(global.config.googlePlaces);
			gPlaces.getByPlaceId(gPlaceId, function (err, gPlaceObj) {
				if (err) return cb(err)
				var gCompany = Company.fromGoogle(gPlaceObj)
				gCompany.save((err, gCompany) => {
					cb(err, [gCompany]);
				})
			})
		}
	});
};

Company.updateByGooglePlaceId = function () {
	// TODO after update with google filledWithGoogle must be set to TRUE
}

Company.fromGoogle = function (googleData) { // TODO check for last update was not far then one week
	var o = {};

	var location = googleData

	o.companyName = googleData.name || "-";
	o.email = "-";
	o.defaultLiveDealImage = "-"; // TODO don`t know where to get it
	o.categoryId = 0; // TODO don`t know where to get it
	o.tariff = 0; // TODO don`t know where to get it
	o.address = googleData.vicinity || googleData.formatted_address || null;
	o.phone = googleData.international_phone_number || googleData.international_phone_number || "-";
	o.priceLevel = googleData.price_level || 0;
	o.webSiteUrl = googleData.website || "";
	o.googlePlaceId = googleData.place_id || null;
	o.location = {
		type: "Point",
		coordinates: [
			_.get(googleData, 'geometry.location.lng'),
			_.get(googleData, 'geometry.location.lat')
		]
	};
	o.galleryArray = (new Gplaces(global.config.googlePlaces)).getPhotoArray(googleData.photos);
	o.regionName = Company.getRegioinName(googleData.address_components);
	o.openingHours = Company.formatOpeningHours(googleData.opening_hours);
	o.filledWithGoogle = true
	o.userRatingsTotal = googleData.user_ratings_total || 0
	if (googleData.rating) {
		o.rating = googleData.rating;
	} else {
		let reviews = googleData.reviews || [];
        if (reviews.length > 0) {
		    o.rating = +(_.reduce(reviews, (p, n) => p + n.rating, 0 ) / reviews.length ).toFixed(2);
        } else {
            o.rating = 0;
        }
	}
	return new Company(o);
};

Company.formatOpeningHours = function (opening_hours = {}) {
	var result = {}
	if (opening_hours.open_now) result.openNow = true
	else result.openNow = false
	result.periods = opening_hours.periods
	return result
};

Company.getRegioinName = function (googleData) { // TODO implement this
	var regionName;
	regionName = geocoder.parseRegionName(googleData)
	return regionName;
};

Company.getByLocation = function ({lat, lon, distance, limit, offset, text=''}, cb = ()=> {
}) {
	//console.log('Company.getByLocation')
	//console.log(lat, lon, distance, limit, offset, text);
	distance -= 0;
	lat -= 0;
	lon -= 0;
	limit -= 0;
	offset -= 0;
	if (!distance || distance < 0)    distance = 0;
	if (!limit || limit < 0)        limit = 20;
	if (!offset || offset < 0)        offset = 0;

	var point = {
		type: "Point",
		coordinates: [lon, lat]
	};
	var nearOptions = {
		query: {},
		near: point,
		maxDistance: distance || 1000, // in meters
		spherical: true,
		distanceField: "distance"
	}
	if (text) {
		nearOptions.query.companyName = new RegExp(text, 'ig');
		//nearOptions.query.address = new RegExp( text, 'ig' );
		//nearOptions.query.regionName = new RegExp( text, 'ig' );
	}

	Company
		.aggregate()
		.near(nearOptions)
		.skip(offset)
		.limit(limit)
		.exec(function (err, res) {
			//console.log('companies')
			//console.log(res)
			if (err) return cb(err)
			var companies = res.map(company => (new Company(company)).toObject())
			cb(null, companies)
		});
}

Company.__remove = function (companyId, cb) {
	Company.remove({id: companyId}, cb);
};

function companyToObject(doc, ret, options) {
	delete ret.password
	delete ret.adminName
	if (!ret.updatedAt) delete ret.updatedAt
	if (_.isNull(ret.distance)) delete ret.distance
	ret.location = {
		lon: ret.location.coordinates[0],
		lat: ret.location.coordinates[1]
	}
	//ret.openingHours.openNow = calculateIsOpened(ret.openingHours)

	return ret
}

function getTimeStamp(val) {
	if (!val) return 0;
	return +(new Date(val))
}

function calculateIsOpened(openingHours = {}) {
	var now = new Date();
	var nowTime24 = `${now.getHours()}${now.getMinutes()}`
	var today = now.getDay();
	var periods = openingHours.periods || []
	var period = _.where(periods, {
		open: {day: today}
	})
	if (period[0]) {
		var isOpen = !!period.open.time && period.open.time < nowTime24
		var notClosed = !!period.close && !!period.close.time && nowTime24 < period.close.time
		if (isOpen && notClosed) return true
		else return false
	} else return false
}

module.exports = Company;
