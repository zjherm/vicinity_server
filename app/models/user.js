/*
 * great search pattern
 *
 * /(?=.*word1)(?=.*word2)(?=.*word3)/m
 * /(?=.*gg)(?=.*ma)/m
 *
 * .test('word1 word2 word3') === true
 */

var _ = require('lodash');
var async = require("async");
var util = require("util");
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Notification = require("./notification");

/* DEPENDENCIES
* -	Session.userId
* - Post.authorId
* - Comment.userId
* - Notification.actorId
* - EmailConfirmationToken.userId
* - RestorePasswordToken.userId
*/

var userStatuses = [
    'Normal',
    'Blocked',
    'Deleted'
];

// non camel case, because of postgres past
var UserSchema = new Schema({
    fullname: String,
    nickname: String,
    password: String,
    isBusiness: {type: Boolean, default: false},
    businessCategory: {type: Number, default: 0},
    businessAddress: String,
    city: String,
    state: String,
    country: String,
    email: String,
    pendingEmail: String,
    gender: String,
    stripeId: String,
    pushTokens: [String],
    avatarUrl: String,
    created: {type: Date, default: Date.now },
    lastCome: Date,
    bio: String,
    isBot: {type: Boolean, default: false},
    isEmailConfirmed: {type: Boolean, default: false},
    //notificationsSeen: {type: Number, default: 0},
    facebookToken: {type: String},
    facebook: Schema.Types.Mixed,
    friends: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    blockedUsers: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    viewedProfiles: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    invitedUsers: [String],
    userStatus: {
        type: String,
        enum: userStatuses,
        default: 'Normal'
    },
    isFriendPostedPushNotificationEnabled: { type: Boolean, default: true },
    isUserPostLikedPushNotificationEnabled: { type: Boolean, default: true },
    isUserPostCommentedPushNotificationEnabled: { type: Boolean, default: true },
    isOtherPostCommentedPushNotificationEnabled: { type: Boolean, default: true },
    isUserMentionedPushNotificationEnabled: { type: Boolean, default: true },
    isPostWithinVicinityPushNotificationEnabled: { type: Boolean, default: true },
    postWithinVicinityPushNotificationDistance: { type: Number, default: 50.0 },
    pushNotificationLocation: {
			lat: {type: Number},
			lon: {type: Number},
			geo: {
				type: {type: String, default: "Point"},
				coordinates: [Number]
			}
		}
}, {
    toObject: {
        //getters: true,
        //setters: true,
        //virtuals: true,
        //minimize: false,
        //retainKeyOrder: true,
        transform: userToObject
    },
    toJSON: {
        //getters: true,
        //setters: true,
        //virtuals: true,
        //minimize: false,
        //retainKeyOrder: true,
        transform: userToObject
    }
});

UserSchema.index({"facebook.id": 1});
UserSchema.index({nickname: "text"});
UserSchema.index({fullname: "text"});
UserSchema.index({"pushNotificationLocation.geo": "2dsphere"});

UserSchema.pre('remove', function (next) {
	var Post = _.get(global, 'mongoose.models.Post', {});
	var Session = _.get(global, 'mongoose.models.Session', {});
	var Comment = _.get(global, 'mongoose.models.Comment', {});
	var Notification = _.get(global, 'mongoose.models.Notification', {});
	var EmailConfirmationToken = _.get(global, 'mongoose.models.EmailConfirmationToken', {});
	var RestorePasswordToken = _.get(global, 'mongoose.models.RestorePasswordToken', {});
    var Report = _.get(global, 'mongoose.models.Report', {});

	// removing all deps
	async.parallel([
		(_cb) => {
			Post.remove({
				"authorId" : this._id
			}, (err, res) => {
				console.log('remove user`s posts');
				console.log(err, res.result);
				_cb(err, {Post:res.result})
			})
		},
		(_cb) => {
			Session.remove({
				"userId" :  this._id
			}, (err, res) => {
				console.log('remove user`s sessions');
				console.log(err, res.result);
				_cb(err, {Session:res.result});
			})
		},
		(_cb) => {
			Comment.remove({
				"userId" :  this._id
			}, (err, res) => {
				console.log('remove user`s comments');
				console.log(err, res.result);
				_cb(err, {Comment:res.result});
			})
		},
		(_cb) => {
            Notification.remove({
				"actorId" :  this._id
			}, (err, res) => {
				console.log('remove user`s notifications');
				console.log(err, res.result);
				_cb(err, {Notification:res.result});
			})
		},
		(_cb) => {
            Notification.remove({
				"receivers.userId" :  this._id
			}, (err, res) => {
				console.log('remove notifications addressed to user');
				console.log(err, res.result);
				_cb(err, {Notification:res.result});
			})
		},
		(_cb) => {
			EmailConfirmationToken.remove({
				"userId" :  this._id
			}, (err, res) => {
				console.log('remove user`s emailConfirmationTokens');
				console.log(err, res.result);
				_cb(err, {EmailConfirmationToken:res.result});
			})
		},
		(_cb) => {
			RestorePasswordToken.remove({
				"userId" :  this._id
			}, (err, res) => {
				console.log('remove user`s restorePasswordTokens');
				console.log(err, res.result);
				_cb(err, {RestorePasswordToken:res.result});
			})
		},
        (_cb) => {
            Report.remove({
                "senderId" :  this._id,
                "userId" : this._id
            }, (err, res) => {
                console.log('remove user`s reports');
                console.log(err, res.result);
                _cb(err, {Report:res.result});
            })
        },
	], (err, res) => {
		console.log('ALL USER`S DATA REMOVED')
		console.log('err: ');console.log(err);
		console.log('res: ');console.log(res);
		if (err) return next(err);
		next(res);
	})
});

UserSchema.pre('save', function(next) {
  if (this.isNew &&
			((this.pushNotificationLocation.lon === undefined)
			 || (this.pushNotificationLocation.lat === undefined))) {
    this.pushNotificationLocation.geo = undefined;
  }
  next();
});

var User = mongoose.model('User', UserSchema);

User.createFindOptions = function(options, isNormal) {
    var find = { $and: [ { email : {$ne: "info@getvicinityapp.com"} } ] };

    if(isNormal)
        find.$and.push({userStatus : "Normal"});
    if(options)
        find.$and.push(options);

    return find;
};

User.getById = (userId, cb) => {
    User.findOne({_id: userId}, cb);
}

// асинхронно создат уникальный nickname и присвоит
User.prototype.setUniqNickname = function (fb, cb) {
    var nick;
    var fbName;
    var self = this;
    if (fb.first_name && fb.second_name) {
        nick = util.format("%s.%s", fb.first_name.toLowerCase(), fb.second_name.toLowerCase());
    } else {
        fbName = (fb.first_name ? fb.first_name : (fb.second_name ? fb.second_name : fb.name)) || "testUser";
        nick = fbName.replace(" ", ".").toLowerCase() || "anonymous";
    }

    User.megaCheckBlyat(nick, this._id, false, function (err, nick) {
        if (err) return cb(err);
        self.nickname = nick;
        cb();
    });
};

// TODO: Not used, wipe
//User.findByPartNickName = function (text, cb) {
//    text = text.replace(/[^a-zA-Z0-9_\.]/g, "");
//    text = text.replace(".", "\\.");
//    if (text.length == 0) return cb(null, [])
//    var r = new RegExp(text);
//    User.find({nickname: r}, cb);
//};

User.search = ({text, email}, cb) => {
    var searchParams = {};

    if (text) {
        let regExpString = text.split(' ').join('|')
        let regExp = new RegExp(regExpString, 'gi');
        searchParams.$or = [ {fullname: regExp}, { nickname: regExp}];
    }

    if (email) {
        let emails = email.replace(/\s/g, '').split(',');
        searchParams.email = { $in: emails };
    }

    if (searchParams.email || searchParams.$or) {
        User.find(User.createFindOptions(searchParams, true), cb);
    } else {
        cb(null, []);
    }
}

// TODO: not used, wipe
//User.findByNickName = function (nickName, cb) {
//    nickName = nickName.replace(/[^a-zA-Z0-9_\.]/g, "");
//    nickName = nickName.replace(".", "\\.");
//    if (nickName.length == 0)
//        return cb(null, null);
//
//    User.findOne(User.createFindOptions( {nickname: nickName} ), cb);
//};

// Дп простит меня господь за эту хуйню
/**
 *
 * @param nick current nickname
 * @param id userId
 * @param justCheck boolean weather just check existing or generate new
 * @param cb
 */
User.megaCheckBlyat = function (nick, id, justCheck, cb) {
    var exist = true;
    var counter = 0;

    async.whilst(
        function () {
            return exist;
        },
        function (callback) {
            User.findOne({nickname: nick, _id: {$ne: id}}, function (err, user) {
                exist = user;
                if (exist) {
                    if (justCheck) {
                        return cb({
                            code: "NICKNAME_ALREADY_EXISTS"
                        });
                    }
                    nick = util.format("%s%s", nick, ++counter);
                }
                callback(err);
            });
        },
        function (err) {
            cb(err, nick);
        }
    );
};

/**
 * Create new user from facebook data
 * @param fb - facebook user data
 * @param cb - callback
 */
User.create = function (fbUser, cb) {

    var fields = {
        fullname: fbUser.name || "",
        nickname: "",
        facebook: fbUser,
        city: (fbUser.location && fbUser.location.location && fbUser.location.location.city) || null,
        state: (fbUser.location && fbUser.location.location && fbUser.location.location.state) || null,
        country: (fbUser.location && fbUser.location.location && fbUser.location.location.country) || null,
        lastCome: new Date(),
        email: fbUser.email,
        bio: '',
        gender: fbUser.gender,
        stripeId: fbUser.stripeId,
        avatarUrl: "https://graph.facebook.com/" + fbUser.id + "/picture?width=1080&height=1080",
    };

    // https://trello.com/c/NDqg0Met/70-fb-email
    if (fbUser.email) fields.isEmailConfirmed = true;

    global.logger.debug(fields);

    var user = new User(fields);
    user.save(function (err) {
        cb(err, user);
    });
    /*user.setUniqNickname(fb, function (err) {
     if (err) return cb(err);
     user.save(function (err) {
     cb(err, user);
     });
     });*/
};

/**
 * Update attributes in User and Session collection and session object in memory
 * @param {object} attrs - fields to update in db
 * @param {object} session - session object for update in memory
 * @param cb
 */
User.setAttributes = function (attrs, session, cb) {

    Object.keys(attrs).forEach(function (field) {
        session[field] = attrs[field];
    });

    async.parallel([
        User.update.bind(User, {_id: session.userId}, attrs),
        session.save.bind(session)
    ], cb);
};

User.updateProfile = function (session, data, cb) {
    var userId = session.userId;

    // prevent 'isEmailConfirmed' field from updating directly
    if (data.isEmailConfirmed)
        delete data.isEmailConfirmed;

    var update = function () {
        Object.keys(data).forEach(function (key) {
            session.user[key] = data[key];
        });

        async.parallel([
          (next) => {
						User.findOneAndUpdate({_id: userId}, {$set: data}, {new: true}, (err, user) => {
							Notification.update({actorId: userId}, {$set: {actor: user.getPublicObject()}}, next);
						});
					},
          session.save.bind(session)
        ], cb);
    };

    if (data.nickname) { // trying to change nickname
        User.megaCheckBlyat(data.nickname, userId, true, function (err) {
            if (err)
                return cb(err);
            update();
        });
    }
    else {
        update();
    }
};

User.prototype.getPublicObject = function (currentUser = {}) {

    var user = {
        userId: this._id,
        name: this.name,
        facebook: this.facebook,
        gender: this.gender,
        bio: this.bio,
        fullname: this.fullname,
        email: this.email,
        pendingEmail: this.pendingEmail,
        avatarUrl: this.avatarUrl,
        nickname: this.nickname,
        isBusiness: this.isBusiness,
        businessCategory: this.businessCategory,
        businessAddress: this.businessAddress,
        city: this.city,
        state: this.state,
        country: this.country,
        isEmailConfirmed: this.isEmailConfirmed,
        userStatus: this.userStatus
    };

    if (currentUser.friends) {
        user.isFriend = !!_.find(currentUser.friends, (friend) => {
            return friend.equals(this._id)
        });
    }

    if (currentUser.blockedUsers) {
        user.isBlocked = !!_.find(currentUser.blockedUsers, (blockedUser) => {
            return blockedUser.equals(this._id);
        });
    }

    return user;
};

User.getCountUnviewedNotifications = function (userId, cb) {
  Notification.count({"receivers.userId": mongoose.mongo.ObjectId(userId), "receivers.viewed": false},function(err, count) {
    cb(count);
  });
};


User.__remove = (userIds = [], cb =()=> {}) => {
    if (!userIds.length) return cb(null, null);
    User.find({
        _id: { $in: userIds }
    }, (err, users) => {
		if (err) return cb(err);
        let remove = (user, _cb) => {
            user.remove(_cb)
        }
        async.map(users,remove, cb)
    })
};


User.updateNotifications = function ( currentUser = {}, userId, cb) {
	Notification.update({ actorId: userId, postId: {$exists:false},
												"receivers.userId": currentUser.user._id,
												"receivers.viewed": false },
											{$set :{ "receivers.$.viewed": true}},
											{ multi: true },
											cb);
};

User.prototype.getPushNotificationSettings = function () {
    var settings = {
        isFriendPostedPushNotificationEnabled: this.isFriendPostedPushNotificationEnabled,
        isUserPostLikedPushNotificationEnabled: this.isUserPostLikedPushNotificationEnabled,
        isUserPostCommentedPushNotificationEnabled: this.isUserPostCommentedPushNotificationEnabled,
        isOtherPostCommentedPushNotificationEnabled: this.isOtherPostCommentedPushNotificationEnabled,
        isUserMentionedPushNotificationEnabled: this.isUserMentionedPushNotificationEnabled,
        isPostWithinVicinityPushNotificationEnabled: this.isPostWithinVicinityPushNotificationEnabled,
        postWithinVicinityPushNotificationDistance: this.postWithinVicinityPushNotificationDistance
    };
    return settings;
};

User.updatePushNotificationSettings = function (userId, data, cb) {
    var settings = {
        isFriendPostedPushNotificationEnabled: data.isFriendPostedPushNotificationEnabled,
        isUserPostLikedPushNotificationEnabled: data.isUserPostLikedPushNotificationEnabled,
        isUserPostCommentedPushNotificationEnabled: data.isUserPostCommentedPushNotificationEnabled,
        isOtherPostCommentedPushNotificationEnabled: data.isOtherPostCommentedPushNotificationEnabled,
        isUserMentionedPushNotificationEnabled: data.isUserMentionedPushNotificationEnabled,
        isPostWithinVicinityPushNotificationEnabled: data.isPostWithinVicinityPushNotificationEnabled,
        postWithinVicinityPushNotificationDistance: data.postWithinVicinityPushNotificationDistance
    };
    User.findOneAndUpdate({_id: userId}, {$set: settings}, {new: true}, (err, user) => {
        if (err) return cb(err, user);
        cb(null, user);
    });
};

User.updatePushNotificationLocation = function (userId, data, cb) {
    var location = {
        pushNotificationLocation: {
          lat: data.lat,
          lon: data.lon,
          geo: {
            type:"Point",
            coordinates: [data.lon, data.lat]
					}
				}
		};
    User.findOneAndUpdate({_id: userId}, {$set: location}, {new: true}, (err, user) => {
        if (err) return cb(err, user);
        cb(null, user);
    });
};

function userToObject(doc, ret, options) {
    delete ret.password

    return ret
}


module.exports = User;
