var Schema = global.mongoose.Schema;
var STATUS = require('../response-statuses');
var _ = require('lodash');
var User = require("./user");
var async = require("async");

var SessionSchema = new Schema({
	user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
	deleted: {type: Boolean, default: false},
	created: {type: Date, default: Date.now}
});

//SessionSchema.index(
//	{created: 1},
//	{ expireAfterSeconds: global.config.sessionTTLMinutes * 60 } // TTL should be synced with FB token expiration
//);

SessionSchema.virtual('userId').get(function() { return this.user._id; });
SessionSchema.virtual('fullname').get(function() { return this.user.fullname; });
SessionSchema.virtual('nickname').get(function() { return this.user.nickname; });
SessionSchema.virtual('isBusiness').get(function() { return this.user.isBusiness; });
SessionSchema.virtual('businessCategory').get(function() { return this.user.businessCategory; });
SessionSchema.virtual('businessAddress').get(function() { return this.user.businessAddress; });
SessionSchema.virtual('city').get(function() { return this.user.city; });
SessionSchema.virtual('state').get(function() { return this.user.state; });
SessionSchema.virtual('country').get(function() { return this.user.country; });
SessionSchema.virtual('email').get(function() { return this.user.email; });
SessionSchema.virtual('pendingEmail').get(function() { return this.user.pendingEmail; });
SessionSchema.virtual('stripeId').get(function() { return this.user.stripeId; });
SessionSchema.virtual('gender').get(function() { return this.user.gender; });
SessionSchema.virtual('avatarUrl').get(function() { return this.user.avatarUrl; });
SessionSchema.virtual('bio').get(function() { return this.user.bio; });
SessionSchema.virtual('facebook').get(function() { return this.user.facebook; });
SessionSchema.virtual('facebookToken').get(function() { return this.user.facebookToken; });
SessionSchema.virtual('invitedUsers').get(function() { return this.user.invitedUsers; });
SessionSchema.virtual('friends').get(function() { return this.user.friends; });
SessionSchema.virtual('blockedUsers').get(function() { return this.user.blockedUsers; });
SessionSchema.virtual('isEmailConfirmed').get(function() { return this.user.isEmailConfirmed; });
SessionSchema.virtual('userStatus').get(function() { return this.user.userStatus; });

SessionSchema.set('toJSON', { getters: true, virtuals: true });

var Session = mongoose.model('Session', SessionSchema);

Session.prototype.getPublicObject = function (callback) {
	return {
		user: {
			gender: this.user.gender,
			fullname: this.user.fullname,
			nickname: this.user.nickname,
			email: this.user.email,
			pendingEmail: this.user.pendingEmail,
			bio: this.user.bio,
			avatarUrl: this.user.avatarUrl,
			isBusiness: this.user.isBusiness,
			businessCategory: this.user.businessCategory,
			businessAddress: this.user.businessAddress,
			city: this.user.city,
			state: this.user.state,
			country: this.user.country,
			userId: this.user._id,
			stripeId: this.user.stripeId,
			facebook: this.user.facebook,
			friends: this.user.friends,
			blockedUsers: this.user.blockedUsers,
			isEmailConfirmed: this.user.isEmailConfirmed,
			userStatus: this.user.userStatus
		},
		sessionId: this._id
	};

};

/**
 * Admin site authentication middleware (see app-admin)
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
Session.adminMiddleware = function (req, res, next) {
	var sid = req.get("Authorization") || req.query["sessionId"];
	if (!sid)
		return next(STATUS.unauthorized);

	Session.findOne({_id: sid})
		.populate('user')
		.exec((err, session) => {
		if (err)
			return next(err);

		var ADMIN_EMAIL = "info@getvicinityapp.com";
		if (!session || session.deleted || (session.user && (session.user.userStatus == "Deleted" || session.user.email != ADMIN_EMAIL)))
			return next(STATUS.unauthorized);

		req.session = session;
		next();
	});
};

Session.middleware = function (req, res, next) {

	var sid = req.get("Authorization") || (req.cookies && req.cookies.sessionId);
	console.log(sid);

	if (sid) {
		if (sid.indexOf("Basic") !== -1) {
			sid = req.cookies.sessionId;
		}
		Session.findOne({
			_id: sid
		})
		.populate('user')
		.exec(function (err, session) {
			if (err) {
				return next(err);
			}
			if (!session || session.deleted || (session.user && session.user.userStatus == "Deleted")) {
				next(STATUS.unauthorized);
			} else if (session.user && session.user.userStatus == "Blocked") {
				return next(STATUS.userBlocked);
			} else {
				req.session = session;
				next();
			}
		});

	} else {
		next(STATUS.unauthorized);
	}
};

/**
 * Create session
 * @param {object} user - extended user data
 * @param {function} cb
 */
Session.create = (user, cb) => {
	// TODO method to just proper UPDATE current session and just CREATE new

	let session = new Session({
		user: user._id,
		created: Date.now(),
		deleted: false
	});

	session.save((err, item) => {
		if (err) cb(err);
		Session.findOne({
			_id: item._id
		})
		.populate('user')
		.exec(function (err, _item) {
			cb(err, _item);
		});
	});
};


module.exports = Session;
