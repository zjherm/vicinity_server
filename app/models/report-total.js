/**
 * Created by Roman Bespalov on 11/27/15.
 */
var Schema = global.mongoose.Schema;
var async = require("async");
var User = require("./user");

var ReportTotalSchema = new Schema({
    _id: {type: String },
    user : { type: Schema.Types.ObjectId, ref: 'User'},
    target : {type: String },
    count: Number
});


var ReportTotal = global.mongoose.model( "ReportTotal", ReportTotalSchema, null );


module.exports = ReportTotal;
