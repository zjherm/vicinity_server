var SwaggerTest = require('mocha-swagger-tests');
global.config = require("./config");
global.logger = console;
console.debug = console.log;
var Worker = require('./server/worker');
var worker = new Worker(global.config);
var i=0;
worker.init(function () {

    if (++i === 2) {

        var swaggerTest = new SwaggerTest({swaggerPath: './config/swagger.json'});
        swaggerTest.run();

    }

});
