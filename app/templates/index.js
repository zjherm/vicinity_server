var jade = require('jade');
var path = require('path');
var t = jade.compileFile;

module.exports = {
	emails: {
		confirmation: t(path.join(__dirname,'./emails/email-confirmation.jade')),
		resetPassword: t(path.join(__dirname,'./emails/reset-password.jade'))
	}
}