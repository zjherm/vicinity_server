module.exports = {
	userExists: {
		code: 100000,
		message: 'User already exists'
	},
	emailExists: {
		code: 100001,
		message: 'User with this email already exists'
	},
	nicknameExists: {
		code: 100002,
		message: 'User with this nickname already exists'
	},
	userNotFound: {
		code: 100003,
		message: 'User not found'
	},
	unauthorized: {
		code: 100004,
		message: 'Unauthorized request'
	},
	loginInvalid: {
		code: 100005,
		message: 'Incorrect login or password'
	},
	forbidden: {
		code: 100006,
		message: 'Forbidden'
	},
	notImplemented: {
		code: 100007,
		message: 'Not implemented'
	},
	fbUserExists: {
		code: 100008,
		message: 'User with this facebook account already exists'
	},
	emailTokenInvalid: {
		code: 100009,
		message: 'Token invalid or already was used'
	},
	fbTokenInvalid: {
		code: 100010,
		message: 'Facebook token is invalid'
	},
	invalidPassword: {
		code: 100011,
		message: 'Invalid password'
	},
	noFbAccessToken: {
		code: 100012,
		message: 'There is no facebook access token'
	},
	noFbAccount: {
		code: 100013,
		message: 'There is no linked facebook account'
	},
	noEmailInFbAccount: {
		code: 100014,
		message: 'There is no email in your Facebook account or it is not confirmed. You can authorize with login and pass, then link  your Facebook account.'
	},
	facebookExists: {
		code: 100015,
		message: "User with this facebook account already exists"
	},
	serverProblems: {
		code: 100016,
		message: "There is some problem on server"
	},
	yourselfToFriends: {
		code: 100017,
		message: "You can not add yourself to list of your friends."
	},
	restorePasswordTokenInvalid: {
		code: 100018,
		message: "Token invalid or already was used."
	},
	userBlocked: {
		code: 100019,
		message: "User is blocked."
	},
	userLocked: {
		code: 100020,
		message: "User is locked."
	},
}
