var _ = require('lodash');
var async = require("async");
var util = require("util");
var mongoose = require('mongoose');
global.mongoose = mongoose;
var config = require("./config");
global.config = config;
var logger = require("./logger")();

mongoose = require('mongoose');
mongoose.connect(config.mongodb, (err, conn) => {
});
console.log(config.mongodb);
// mongoose.set('debug', (coll, method, query, doc) => {
//   logger.debug("MONGO", util.inspect(query, false, null));
// });

var db = mongoose.connection;
db.once("open", () => {

	var Users = require('./models/user');
	var Notifications = require('./models/notification');

	var Schema = global.mongoose.Schema;
	var UserNotificationSchema = new Schema({
		userId: {
			type: Schema.Types.ObjectId,
			ref: 'User'
		},
		notificationId: {
			type: Schema.Types.ObjectId,
			ref: 'Notification'
		},
		viewed: {type: Boolean, default: false}
	});
	var UserNotifications = mongoose.model('UserNotification', UserNotificationSchema);

	async.waterfall([
		/* Forcing required indexes */
		(next) => {
			var users = db.collection('users');
			users.ensureIndex({"pushNotificationLocation.geo": "2dsphere"});
			var notifications = db.collection('notifications');
			notifications.ensureIndex({"actorId": 1}); /* TS here? */
			notifications.ensureIndex({"receivers.userId": 1, ts: -1});
			notifications.ensureIndex({"receivers.viewed": 1, "receivers.userId": 1, ts: -1 });
			console.log("Indexes are set!");
			next();
		},
		/* Update pushLocation to GEO data */
		(next) => {
			var Users = require('./models/user');
			Users.find({
				"pushNotificationLocation.lon": {$exists: true},
				"pushNotificationLocation.lat": {$exists: true},
				"pushNotificationLocation.geo": {$exists: false}
			}, (err, users) => {
				console.log("Found", users.length, "users to migrate...");
				async.eachLimit(users, 1, (user, cb) => {
					if ((user.pushNotificationLocation === undefined)
							|| (user.pushNotificationLocation.lon === undefined)
							|| (user.pushNotificationLocation.lat === undefined)) {
						// console.log("Skipping user: ", user._id);
						return cb();
					}
					user.pushNotificationLocation.geo = {
						type: "Point",
						coordinates: [ user.pushNotificationLocation.lon, user.pushNotificationLocation.lat ]
					};

					user.save(cb);
				}, () => {
					console.log("pushLocation data updated!");
					next();
				});
			});
		}
		/* Merge Notification and UserNotification */
		, (next) => {
			var mergePosts = (type, cb) => {
				async.waterfall([
					(next) => {
						console.log("Fetching posts IDs");
						Notifications.distinct('postId', {notifType: type}, next);
					},
					(postIDs, next) => {
						/* for each postID ...*/
						async.eachLimit(postIDs, 1, (postId, cb) => {
							console.log("Processing postID", postId);
							/* ...find related notifications of certain type... */
							Notifications.find({notifType: type, postId: postId}, (err, notifs) => {
								if (err) return cb();
								if (notifs.length == 0) {
									return cb();
								}
								/* ...from found extract the first as primary and go through the rest... */
								var primaryNotification = notifs[0];
								if (_.isArray(primaryNotification.receivers) && (primaryNotification.receivers.length > 0) && (notifs.length == 1)) {
									/* Skipping already migrated notification */
									return cb();
								}
								console.log("Processing notifications...");
								async.eachLimit(notifs, 1, (notif, cb) => {
									/* ...for each non-primary notification, find all the usernotifications... */
									UserNotifications.find({notificationId: notif._id}, (err, userNotifications) => {
										/* ...embed data into the primary notification... */
										_.forEach(userNotifications, (userNotify) => {
											primaryNotification.receivers.push({
												userId: userNotify.userId,
												viewed: userNotify.viewed
											});
										});
										/* ...and remove the non-needed any more non-primary notification */
										if (primaryNotification != notif) {
											notif.remove(cb);
										}
										else {
											cb();
										}
									});
								}, () => {
									// console.log("Saving primary notification");
									primaryNotification.save(cb);
								});
							});
						}, next);
					}
				], () => {
					console.log("Done migrating posts of type", type);
					cb();
				});
			};
			var mergeComments = (type, cb) => {
				async.waterfall([
					(next) => {
						console.log("Fetching comment notifications");
						Notifications.aggregate({"$match" : { notifType: type }}, {
							"$group": {
								"_id": { postId: '$postId', actorId: '$actorId', text: '$text' }
							}
						}, next);
					},
					(data, next) => {
						/* for each distinct comment notification ...*/
						async.eachLimit(data, 1, (dist, cb) => {
							console.log("Processing comment group", dist);
							/* ...find related notifications of certain type... */
							Notifications.find({
								notifType: type,
								postId: dist._id.postId,
								actorId: dist._id.actorId,
								text: dist._id.text
							}, (err, notifs) => {
								if (err) return cb();
								if (notifs.length == 0) {
									return cb();
								}
								/* ...from found extract the first as primary and go through the rest... */
								var primaryNotification = notifs[0];
								if (_.isArray(primaryNotification.receivers) && (primaryNotification.receivers.length > 0) && (notifs.length == 1)) {
									/* Skipping already migrated notification */
									return cb();
								}
								console.log("Processing notifications...");
								async.eachLimit(notifs, 1, (notif, cb) => {
									console.log("Processing notification", notif._id);
									/* ...for each non-primary notification, find all the usernotifications... */
									UserNotifications.find({notificationId: notif._id}, (err, userNotifications) => {
										/* ...embed data into the primary notification... */
										_.forEach(userNotifications, (userNotify) => {
											primaryNotification.receivers.push({
												userId: userNotify.userId,
												viewed: userNotify.viewed
											});
										});
										/* ...and remove the non-needed any more non-primary notification */
										if (primaryNotification != notif) {
											notif.remove(cb);
										}
										else {
											cb();
										}
									});
								}, () => {
									// console.log("Saving primary notification");
									primaryNotification.save(cb);
								});
							});
						}, next);
					}
				], () => {
					console.log("Done migrating comments of type", type);
					cb();
				});
			};

			async.parallelLimit([
				async.apply(mergePosts, 'mention.post'),
				async.apply(mergePosts, 'post.connection'),
				async.apply(mergePosts, 'post.vicinity'),

				async.apply(mergeComments, 'comment'),
				async.apply(mergeComments, 'mention.comment'),
				async.apply(mergeComments, 'comment.publish')
			], 1, () => {
				console.log("Notifications are migrated");
				next();
			});
		}
		/* Populate Notification with actor */
		, (next) => {
			Notifications.find({actorId: { $exists: true }, actor: {$exists: false} }, (err, notifs) => {
				console.log("Found", notifs.length, "notifications to migrate...");
				async.eachLimit(notifs, 1, (notify, cb) => {
					// console.log(notify.actorId);
					if (notify.actorId === undefined) {
						// console.log("Skipping notification: ", notify._id);
						return cb();
					}
					// console.log("Updating notification: ", notify._id, notify.actor);
					Users.findById(notify.actorId, (err, user) => {
						if (err) {
							// console.log("user was not found");
							return cb();
						}
						// console.log('user was found');
						notify.actor = user.getPublicObject();
						notify.save(cb);
					});
				}, () => {
					console.log("Notifications are populated with actor");
					next();
				});
			});
		},
		/* Drop userNotifications field in User */
		(next) => {
			db.collection('users').update({}, { $unset: { userNotifications: "" } }, {multi: true}, () => {
				console.log("userNotifications field in User is dropped");
				next();
			});
		},
		(next) => {
			db.collection('users').update({}, { $unset: { notifications: "" } }, {multi: true}, () => {
				console.log("notifications field in User is dropped");
				next();
			});
		},
		(next) => {
		/* Drop UserNotifications */
			mongoose.connection.db.dropCollection('usernotifications', () => {
				console.log("userNotifications collection is dropped");
				next();
			});
		}
	], () => {
		console.log("Migration is finished!");
		process.exit(0);
	});
});
