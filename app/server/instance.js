/**
 * Created by Roman Bespalov on 12/24/15.
 */

var Worker = require("./worker");

global.config = require("../config");

(new Worker(global.config)).init();
