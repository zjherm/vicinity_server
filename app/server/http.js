var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var SwaggerUI = require("../middleware/app-swagger-ui");
var VicinityApi = require("../middleware/app-vicinity");
var AdminApi = require("../middleware/app-admin");

var config = global.config;
var app = express();

// apply basic auth for SwaggerUI root only
app.use(function(req, res, next) {
	if(req.path != "/") // here the Swagger UI  to be expected
		return next();

	var auth;
	if (req.headers.authorization) { // only accepting basic auth
		auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
	}

	if (!auth || auth[0] !== 'info@getvicinityapp.com' || auth[1] !== '$WAGGER') {
		res.statusCode = 401;
		res.setHeader('WWW-Authenticate', 'Basic realm="Sorry, you are not welcome here"');
		res.end('Unauthorized');
	}
	else {
		next();
	}
});

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://' + config.host + ':9000');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Set-Cookie, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Cache-Control', 'no-cache,no-store,max-age=0,must-revalidate');
	next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());


// hanlde User IP address -> __requestIp
app.use((req, res, next) =>
{
	var clientIp = req.headers['x-forwarded-for'] ||
	     req.connection.remoteAddress ||
	     req.socket.remoteAddress ||
	     req.connection.socket.remoteAddress;
	if (clientIp) {
		global.__requestIp = clientIp;
		req.__requestIp = clientIp;
	}
	else {
		console.error(`<<<<<< USER IP ADDRESS NOT DEFINED >>>>>> clientIp: ${ clientIp }`);
	}
	next();
});

// output middleware
var result = (req, res) => {
	var output =
	{
		status: "success",
		data: req.result,
		error: null
	};

	output.pagination = req.pagination;
	res.json(output);
};

// swagger configuration
var adminConfig = SwaggerUI.createConfig(config.root + "/app/config/swagger-admin.json");
var vicinityConfig = SwaggerUI.createConfig(config.root + "/app/config/swagger.json");
if(config.showSwagger) {
	adminConfig.api = `${config.host}:${config.port}` + "/admin";
	vicinityConfig.api = `${config.host}:${config.port}`;
}

// 1. /admin -> Admin site
app.use("/admin", (new AdminApi(adminConfig)).getRouter());

// 2. / -> Vicinity site
app.use("/", (new VicinityApi(vicinityConfig)).getRouter(), result);

module.exports = app;
