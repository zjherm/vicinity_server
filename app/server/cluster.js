#!/usr/bin/env node
var cluster = require('cluster');
var numOfCPUs = require('os').cpus().length;
var Worker = require("./worker");

if (cluster.isMaster) {
    // Master, spawn workers
    for (var i = 0; i < numOfCPUs; ++i) {
        cluster.fork();
    }

    cluster.on('exit', function () {

		cluster.fork();
    });

} else {
    // Worker process
    // GLOBALS

    global.config = require("../config");
	global.logger = require("../logger")();

    var domain = require('domain').create();
    var exceptionsNum = 0;
    domain.on("error", function (err) {
        if (++exceptionsNum > 3) return; // cycle problem
        global.logger.err(arguments); // log and sms
    });
    process.title = process.argv[0] + ": Worker process";

    var worker = new Worker(global.config);
    //domain.run(function() {
        worker.init();
    //});
}