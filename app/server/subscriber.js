var async = require('async');
var geolib = require("geolib");
var _ = require('lodash');

var User = require("../models/user");
var Post = require("../models/post");
var Notification = require("../models/notification");

var push = require("../wrappers/push");

var findMentionsInText = function (text, cb) {
	text = text || "";
	var mentions = text.match(/@([a-z0-9A-Z\.]+)/ig);

	if (!mentions || !_.isArray(mentions)) {
		return cb(null, []);
	}

	User.find({
		nickname: {
			$in:	_.map(mentions, function (match) {
				return match.slice(1);
			})
		}
	}, function (err, users) {
		if (err) {
			global.logger.error(err);
			return cb(err);
		}
		if (!users) {
			global.logger.warning("Mentioned users not found", m);
			cb(null, []);
		}
		cb(null, users);
	});
};

global.server.on("push.comment", function (data) {
	var authorId = String(_.get(data, 'author._id'));
	var actorId = String(_.get(data, 'actor.userId'));
	 // remove author from subscribed users list
	var isAuthorSubscribe = !!_.find(data.subscribers, subscriber => subscriber == authorId);
	var isAuthorUnsubscribe = !!_.find(data.unsubscribers, unsubscriber => unsubscriber == authorId);
	var isOwnPost = data.authorId.equals(data.actor.userId);

	_.remove(data.subscribers, subscriber => subscriber == authorId);
	if (!_.get(data, 'actor.nickname')) {
		data.actor.nickname = 'anonym';
	}
	if (!_.get(data, 'author.nickname')) {
		data.author.nickname = 'anonym';
	}

	// console.log("Comment text: " + data.commentText);

	findMentionsInText(data.commentText, (err, users) => {
		/* Send 'mention.comment' push to everyone except author
		 * and explicitly unsubscribed users */
		var mentionCommentRecepients = _.filter(users, (user) => {
			return (actorId.toString() != user._id.toString())
				&& !_.find(data.unsubscribers, (unsubscribed) =>
					unsubscribed.toString() == user._id.toString()
				);
		});
		mentionCommentRecepients = _.map(mentionCommentRecepients, '_id');

		Notification.make({
			postId: data.postId,
			actorId: actorId,
			notifType: "mention.comment",
			eventText: "%user% mentioned you in a comment",
			text: data.commentText
		}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
			push.pushForNotification(notification, mentionCommentRecepients);
		});

		/* Send 'comment' event to the author */
		var isAuthorMentioned = !!_.find(users, user => user._id == authorId);
		// console.log("Is Author Subscribe = " + isAuthorSubscribe);
		// console.log("Is Author mentioned = " + isAuthorMentioned);
		// console.log("Is Own Post = " + isOwnPost);
		if (!isAuthorUnsubscribe && isAuthorSubscribe && !isOwnPost && !isAuthorMentioned) {
			Notification.make({
				postId: data.postId,
				actorId: data.actor.userId,
				notifType: "comment",
				eventText: `${data.actor.nickname} commented on your post`,
				text: data.commentText
			}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
				push.pushForNotification(notification, [data.authorId]);
			});
		}

		/* Send 'comment.publish' event to subscribers that weren't mentioned in comment text */
		var messageToSubscribedUser = `${data.actor.nickname} also commented on ${data.author.nickname}'s post.`;
		if (data.actor.nickname == data.author.nickname) {
			messageToSubscribedUser = `${data.actor.nickname} also commented on their post.`;
		}
		var commentPublishRecepients = _.filter(data.subscribers, (subscriberId) => {
			var isSubscriberMentioned = !!_.find(users, user => user._id.equals(subscriberId));
			// console.log("Is Subscriber Mentioned = " + isSubscriberMentioned);
			// console.log("SubscriberId = " + subscriberId);
			// console.log("Users = " + users);
			return (!subscriberId.equals(data.actor.userId) && !isSubscriberMentioned);
		});

		if (commentPublishRecepients.length > 0) {
			Notification.make({
				postId: data.postId,
				actorId: data.actor.userId,
				notifType: "comment.publish",
				eventText: messageToSubscribedUser,
				text: data.commentText
			}, (err, notification) => {
				if (err) {
					console.log(err);
					return;
				}
				push.pushForNotification(notification, commentPublishRecepients);
			});
		}
	});
});

global.server.on("push.post", function (data) {
	var authorId = data.authorId;
	var postId = data.postId;

	async.waterfall([
		(next) => {
			User.getById(authorId, (err, user) => {
				next(null, user);
			});
		},
		(user, next) => {
			var actor = user.getPublicObject();
			async.parallel({
				/* Fetch all related Notification objects */
				mentionPost: (next) => {
					Notification.findOrCreate({
						actorId: authorId,
						postId: postId,
						notifType: 'mention.post'
					}, Notification.skel({
						actorId: authorId,
						actor: actor,
						postId: postId,
						notifType: 'mention.post',

						eventText: "%user% mentioned you in a post",
						text: data.postText
					}), next);
				},
				postConnection: (next) => {
					Notification.findOrCreate({
						actorId: authorId,
						postId: postId,
						notifType: 'post.connection'
					}, Notification.skel({
						actorId: authorId,
						actor: actor,
						postId: postId,
						notifType: 'post.connection',

						eventText: "Your connection %user% has posted",
						text: data.postText
					}), next);
				},
				postVicinity: (next) => {
					Notification.findOrCreate({
						actorId: authorId,
						postId: postId,
						notifType: 'post.vicinity'
					}, Notification.skel({
						actorId: authorId,
						actor: actor,
						postId: postId,
						notifType: 'post.vicinity',

						eventText: "%user% just posted in your Vicinity",
						text: data.postText
					}), next);
				},
				/* User friends */
				friends: (next) => {
					User.find({friends: data.authorId}, next);
				},
				/* Geographically near users (in 85km radius) */
				geoVicinity: (next) => {
					if (data.postLocation === undefined ||
							data.postLocation.lat === undefined ||
							data.postLocation.lon === undefined) {
						/* No GEO information in post at all. No users */
						next(null, []); return;
					}

					User.find({
						_id : { $ne : authorId },
						'pushNotificationLocation.geo': {
							$near : {
								$geometry: {
									type: "Point" ,
									coordinates: [ data.postLocation.lon , data.postLocation.lat ]
								},
								$maxDistance: 85000 // 85km
							}
						}
					}, (err, users) => {
						if (err) next(null, []);
						else next(null, users);
					});
				}
			}, (err, res) => {
				next(null, res);
			});
		},
		(res, next) => {
			var notifiedUsers = [];
			/* FUCKING _.unionWith missing in our lodash */
			var fuckUnion = (arr) => {
				var i = 0; var j = 0; var usr1,usr2;
				for(i = 0; i < arr.length; i++) {
					usr1 = arr[i]; usr2 = null;
					for(j = 0; j < notifiedUsers.length; j++) {
						if (usr1.userId.equals(notifiedUsers[j].userId)) {
							usr2 = notifiedUsers[j];
							break;
						}
					}
					if (usr2 === null) {
						notifiedUsers.push(usr1);
					}
				}
			};

			var mentionPostRec = res.mentionPost.receivers || [];
			var postConnectionRec = res.postConnection.receivers || [];
			var postVicinityRec = res.postVicinity.receivers || [];

			fuckUnion(mentionPostRec);
			fuckUnion(postConnectionRec);
			fuckUnion(postVicinityRec);

			notifiedUsers = _.map(notifiedUsers, 'userId');

			/* For GEO: leave only those where post is in their radius settings */
			res.geoVicinity = _.filter(res.geoVicinity, (user) => {
				if (user.pushNotificationLocation === undefined ||
						user.pushNotificationLocation.lat === undefined ||
						user.pushNotificationLocation.lon === undefined) {
					return false;
				}

				var distance = geolib.getDistance(
					{latitude: data.postLocation.lat, longitude: data.postLocation.lon},
					{latitude: user.pushNotificationLocation.lat, longitude: user.pushNotificationLocation.lon}
				);
				var filterDistance = user.postWithinVicinityPushNotificationDistance * 1609.344;
				return (distance < filterDistance);
			});

			findMentionsInText(data.postText, (err, users) => {
				next(null, notifiedUsers, res, users);
			});
		},
		(notifiedUsers, res, mentioned, next) => {
			/* Mention notifications */
			var notifyMentioned = _.filter(mentioned, (user) => {
				return !_.find(notifiedUsers, (item) => item.equals(user._id));
			});

			/* Friend notifications */
			var notifyFriends = _.filter(res.friends, (user) => {
				return !_.find(notifiedUsers, (item) => item.equals(user._id))
					&& !_.find(notifyMentioned, (item) => item.equals(user._id));
			});

			/* Vicinity notifications */
			var notifyVicinity = _.filter(res.geoVicinity, (user) => {
				return !_.find(notifiedUsers, (item) => item.equals(user._id))
					&& !_.find(notifyMentioned, (item) => item.equals(user._id))
					&& !_.find(notifyFriends, (item) => item.equals(user._id));
			});

			async.parallel([
				(next) => {
					push.pushForNotification(res.mentionPost, notifyMentioned, () => {
						next(null);
					});
				},
				(next) => {
					push.pushForNotification(res.postConnection, notifyFriends, () => {
						next(null);
					});
				},
				(next) => {
					push.pushForNotification(res.postVicinity, notifyVicinity, () => {
						next(null);
					});
				}
			], next);
		}
	]);
});

global.server.on("push.like", function (data) {
	// console.log("push.like");
	// console.log(data);

	var actorId = String(_.get(data, 'actor.userId'));
	var isOwnPost = data.authorId.equals(data.actor.userId);

	var isUnsubscribed = !!data.unsubscribers.some(function (unsubcribed) {
		return unsubcribed.toString() == data.authorId.toString();
	});

	if (!isOwnPost && !isUnsubscribed) {
		Notification.make({
			postId: data.postId,
			actorId: actorId,
			notifType: "like",
			eventText: "%user% liked your post",
			text: data.postText
		}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
			push.pushForNotification(notification, [data.authorId]);
		});
	}
});

global.server.on("push.friend", function (data) {
	// console.log("push.friend");
	// console.log(data);
	var userId = _.get(data, 'friendId');

	Notification.make({
		actorId: _.get(data, 'actor._id'),
		notifType: "connection",
		eventText: "%user% has added you as a connection",
		friendId: data.friendId
	}, (err, notification) => {
		if (err) {
			console.log(err);
			return;
		}
		push.pushForNotification(notification, [userId]);
	});
});

global.server.on("push.block", function (data) {
	// console.log("push.block");
	// console.log(data);
	var userId = _.get(data, 'userId');

	Notification.make({
		notifType: "user.block",
		eventText: "Your account has been blocked."
	}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
		push.pushForNotification(notification, [userId]);
	});
});

global.server.on("push.delete", function (data) {
	// console.log("push.delete");
	// console.log(data);
	var userId = _.get(data, 'userId');

	Notification.make({
		notifType: "user.delete",
		eventText: "Your account has been deleted."
	}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
		push.pushForNotification(notification, [userId]);
	});
});

global.server.on("push.confirm_email", function (data) {
	// console.log("push.confirm_email");
	// console.log(data);
	var userId = _.get(data, 'userId');

	Notification.make({
		notifType: "user.confirm_email",
		eventText: "Your email address is confirmed."
	}, (err, notification) => {
			if (err) {
				console.log(err);
				return;
			}
		push.pushForNotification(notification, [userId]);
	});
});

module.exports = {
	pusher: push
};
