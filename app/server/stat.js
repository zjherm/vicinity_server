var async = require("async");
var User = require("./../models/user");
var Post = require("./../models/post");

var Stat = function () {

};

Stat.prototype = {

    fillWithUser : function (items, cb) {

        async.each(items, function (item, next) {

            // временный костыль
            if (typeof item !== 'object') item = {ts: Date.now(), userId: item};

            User.findById(item.userId, function (err, user) {
                if (err) return next(err);

                delete item.userId;
                item.user = user;
                next();
            });

        }, cb);

    },


    getPost : function (postId, cb) {

        var resultPost;
        var self = this;

        async.waterfall([

            Post.findOne.bind(Post, {_id : postId}),
            function (post, next) {
                if (!post) next("POST_NOT_FOUND");

                resultPost = post;
                self.fillWithUser(resultPost.wow, next);
            },
            function (next) {
                self.fillWithUser(resultPost.mutual, next)
            }

        ], function (err, results) {
            cb(err, resultPost);
        });

    }

};

module.exports = new Stat;