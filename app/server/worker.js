var util = require("util");
global.config = require("../config");
global.logger = require("../logger")();

var Worker = function (config) {
    this.config = config;
};

Worker.prototype.init = function (cb) {
    cb = cb || function () {};

    // Mongo
    global.mongoose = require('mongoose');
    global.mongoose.connect(this.config.mongodb);
    global.mongoose.set('debug', function (coll, method, query, doc) {
        global.logger.debug("MONGO", util.inspect(query, false, null));
    });
    global.mongoose.connection.once("open", cb.bind(null, null, "mongo", global.mongoose.connection));

    // Express
    var app = require('./http.js');
    var httpServer = app.listen(this.config.port, '0.0.0.0', function () {
		var address = httpServer.address();
        global.logger.info( address.address + ":" + address.port + "/");
        cb(null, "express", httpServer);
    });
    this.app = app;

    var Server = require("./server");
    global.server = new Server();
    require("./subscriber");
};

module.exports = Worker;