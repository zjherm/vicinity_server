/**
 * Created by romanb on 11/16/15.
 */
var express = require("express");
var ODataUtils= require("../util/odata-utils");
var SwaggerUI = require("./app-swagger-ui");

var AdminUsersController = require("../controllers/admin-users-controller");
var AdminPostsController = require("../controllers/admin-posts-controller");
var AdminReportsController = require("../controllers/admin-reports-controller");
var AdminCommentsController = require("../controllers/admin-comments-controller");

var adminAuth = require('../models/session').adminMiddleware;

var AdminApi = function(swaggerConfig) {
    this.json = swaggerConfig.json;
    this.config = swaggerConfig;
};

AdminApi.prototype.getRouter = function() {
    var router = express.Router();
    var apiRouter = express.Router();
    router.use("/api", apiRouter);

    // api root -> returns API metadata
    apiRouter.get("/", (req, res, next) => {
            res.send(ODataUtils.createResult([{"name": "User", "url": "users"}]));
    });

    apiRouter.options("/*", (req, res, next) => {
        res.sendStatus(200);
    });

    apiRouter.use(adminAuth);

    apiRouter.use("/users", (new AdminUsersController(null).router()));
    apiRouter.use("/posts", (new AdminPostsController(null).router()));
    apiRouter.use("/reports", (new AdminReportsController(null).router()));
    apiRouter.use("/comments", (new AdminCommentsController(null).router()));

    // admin root -> show API explorer
    if(SwaggerUI.canShow(this.config))
        router.use(new SwaggerUI(this.config).getRouter());

    // stop routing here
    router.all("/*", (req, res, next) => { res.send("What are you looking for, buddy?"); });

    return router;
};

module.exports = AdminApi;