/**
 * Created by romanb on 11/16/15.
 */
var express = require("express");

var SwaggerUI = require("./app-swagger-ui");

var Swagger = require("../swagger");
var controller = require('../controller');
var authorizations = { api_key : require('../models/session').middleware };

var VicinityApi = function(swaggerConfig) {
    this.json = swaggerConfig.json;
    this.config = swaggerConfig;
};

VicinityApi.prototype.getRouter = function()
{
    var router = express.Router();

    if(SwaggerUI.canShow(this.config))
        router.use((new SwaggerUI(this.config)).getRouter());

    var swagger = new Swagger(this.json, controller, authorizations);

    router.use(swagger.validate.bind(swagger));
    router.use(swagger.handle.bind(swagger));
    router.use(swagger.output.bind(swagger));

    return router;
};

module.exports = VicinityApi;