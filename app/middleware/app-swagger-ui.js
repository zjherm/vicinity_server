/**
 * Created by romanb on 11/16/15.
 */
var express = require("express");
var serveStatic = require('serve-static');


/**
 *
 * @param config { path : "swagger UI to load",
 *                              start : "swagger UI start page",
 *                              json : "API configuration to display",
 *                              api : "API host to call"
 *          } see SwaggerUI.createConfig()
 * @constructor
 */
var SwaggerUI = function(config)
{
    this.config = config;
};

SwaggerUI.prototype =
{
    getRouter: function () {
        var router = express.Router();
        var config = this.config;

        // output static content in dist folder
        router.use(serveStatic(config.path, {'index': [config.start]}));

        // outputs API json configuration (see dist/index.html)
        router.get("/swagger.json", (req, res) => {
            let swaggerConfig = require(config.json);
            swaggerConfig.host = config.api; // set proper API root
            res.json(swaggerConfig);

            //region log >> swaggerConfig.host
            console.log("json: " + config.json);
            //endregion
        });

        return router;
    }
};

/**
 * Creates swagger config JSON with default values
 * @param json
 * @returns {{path: (string|*), start: string, api: null, json: *}}
 */
SwaggerUI.createConfig = function(json)
{
    var config =	{
        path: global.config.swaggerPath,
        start: global.config.swaggerStart,
        api: null,
        json: json
    };

    return config;
};


SwaggerUI.canShow = function(config)
{
    return (config && config.api && config.path && config.start && config.json);
};

module.exports = SwaggerUI;







