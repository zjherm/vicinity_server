/**
 * Created by Roman Bespalov on 11/19/15.
 */
var async = require("async");
var ODataUtils = require("../util/odata-utils");

/**
 *
 * @param model
 * @param selectOptions = {selectFields, filterFields, defaultOrder}
 * @param filter [optional]
 */
var OdataMongooseHandler = function(model, selectOptions, filter) {
    this.model = model;
    this.selectOptions = selectOptions;
    this.filter = filter || [];
};

OdataMongooseHandler.prototype =
{
    /**
     * Returns Express router to handle OData requests for the mongoose model, passed via constructor
     */
    process: function (req, res, callback) {
        var opt = this.selectOptions;

        var whereFilter = ODataUtils.createWhereStatement(req, opt.filterFields);
        if(whereFilter)
            this.filter.push(whereFilter);

        var totalFilter = this.combineFilters(this.filter) || {};

        // build & execute main query
        async.waterfall([
                (callback) => { // 1. retrive items count
                    ODataUtils.execCount(req, this.model, totalFilter, (err, count) => {
                        if(err)
                            return callback(err);
                        callback(null, count);
                    });
                },
                (count, callback) => { // 2. retrive items
                    let query = this.model.find(totalFilter);

                    ODataUtils.attachQueryParams(query, req, opt);

                    if (opt.populate && opt.populate.length) {
                        opt.populate.forEach((item) => {
                            query.populate(item.field, item.select);
                        });
                    }

                    query.exec((err, result) => {
                        if (err)
                            return callback(err);

                        if (opt.isSingleFieldResult) {
                            var isArrayField = Array.isArray(result);
                            result = isArrayField ? result[0][opt.selectFields] : result[opt.selectFields];
                            if (count != -1)
                                count = (result && Array.isArray(result)) ? result.length : 0;
                        }

                        callback(null, result, count);
                    });
                }
            ],
            (err, result, count) => {
                if(callback) // override result output
                    callback(err, result, count);
                else {
                    if (err)
                        res.send(err);
                    else
                        res.json(ODataUtils.createResult(result || [], count));
                }
            }
        );
    },

    combineFilters : function(filters) {
        if(!filters || !filters.length)
            return null;

        var result = null;
        if(filters.length > 1)
            result = {"$and": filters };
        else
            result = filters[0];

        return result;
    }

};

module.exports = OdataMongooseHandler;