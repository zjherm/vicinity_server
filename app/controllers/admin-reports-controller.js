/**
 * Created by Roman Bespalov on 11/27/15.
 */
/**
 * Created by Roman Bespalov on 11/22/15.
 */
var util = require('util');
var async = require("async");

var Post = require("../models/post");
var Report = require("../models/report");
var ReportTotal = require("../models/report-total");
var ODataController = require("./odata-controller");
var ODataUtils = require("../util/odata-utils");

function override(child, fn) {
    child.prototype[fn.name] = fn;
    fn.inherited = child.super_.prototype[fn.name];
}

var AdminReportsController = function(selectOptions) {
    AdminReportsController.super_.call(this, selectOptions || AdminReportsController.DefaultSelectOptions);
};

util.inherits(AdminReportsController, ODataController);

///////////////////////////////////////////////////////   OVERRIDES  /////////////////////////////////////////////////////////////////

override(AdminReportsController, function processFuncReq(req, res, resource) { // GET as function
    switch (resource.func) {
        case "groupped":
            this.getGrouppedReports(req, res, resource);
            break;
        case  "details":
            this.getDetailsReports(req, res, resource);
            break;
    }
});

override(AdminReportsController, function processPostReq(req, res,  id, operation) { // POST function
    switch (operation) {
        case "resolve":
            this.userResolve(req, res, id);
            break;
    }
});

override(AdminReportsController, function get_Model() {
    return Report;
});

override(AdminReportsController, function get_ModelDisplayName() {
    return "Report";
});

///////////////////////////////////////////////////////   MAIN STUFF  /////////////////////////////////////////////////////////////////

/**
 * Resolves report by setting corresp status on items
 * @param req
 * @param res
 * @param id
 */
AdminReportsController.prototype.userResolve = function(req, res, id) {
    var groupId = AdminReportsController.parseGroupId(id);
    this.get_Model().update({"$and" : [ {userId:groupId.userId }, { target: groupId.target }]},
        { $set : { status: 'resolved' } }, {multi: true}, (err, result) => {
        this.output(err, req, res, result);
    });
};

/**
 * Returns report (items groupped by userId and target)
 * @param req
 * @param res
 * @param resource
 */
AdminReportsController.prototype.getGrouppedReports = function(req, res, resource) {
    this.get_Model().aggregate([
            {$match: {status: 'opened' || ''}},
            {$group: { _id: {userId:"$userId", target:"$target"}, count: {$sum: 1}}}, // group by userId & target
            {$sort: {count: -1}}
        ],
        (err, results) => {
            results = results.map((result) => {
                var rep = new ReportTotal();
                rep._id = AdminReportsController.createGroupId(result._id.userId, result._id.target); // combined item ID
                rep.user = result._id.userId;
                rep.target = result._id.target;
                rep.count = result.count;
                return rep;
            });

            ReportTotal.populate(results, {
                    path: "user",
                    select: "fullname nickname email"
                },
                (err, result) => {
                    if (err)
                        res.send(err);
                    else {
                        var count = AdminReportsController.getResultCount(result);
                        res.json(ODataUtils.createResult(result, count));
                    }
                });
        }
    );
};


/**
 * Report details
 * @param req
 * @param res
 * @param resource
 */
AdminReportsController.prototype.getDetailsReports = function(req, res, resource) {
    var groupId = AdminReportsController.parseGroupId(resource.filter.userId); // {ObjectId}_{target}
    var populateOptions = [{path: "senderId", select: "fullname email"}, {path: "userId", select: "fullname email avatarUrl userStatus"}];

    if (groupId.target == "post" || groupId.target == "comment") // populate posts
        populateOptions.push({path: "postId", select: "description photo placeId deleted"});

    this.get_Model() // find not resolved reports
        .find({"$and": [{userId: groupId.userId}, {target: groupId.target}]}).where("status").equals("opened")
        .populate(populateOptions)
        .exec((err, reports) => {
            if (groupId.target == "comment") { // populate comments
                async.map(reports, (report, callback) => {
                        Post.findOne({_id: report.postId, "comments._id": report.commentId}, {_id: 0, "comments.$": 1})
                            .exec((err, result) => {
                                callback(null, report.toDetailsObject((result && result.comments.length > 0) ? result.comments[0] : null));
                            });
                    },
                    (err, mapped) => {
                        var count = AdminReportsController.getResultCount(mapped);
                        res.json(ODataUtils.createResult(mapped, count));
                    }
                );
            }
            else {
                var count = AdminReportsController.getResultCount(reports);
                res.json(ODataUtils.createResult(reports, count));
            }
        });
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


AdminReportsController.getResultCount = function(result) {
    return (result && Array.isArray(result)) ? result.length : 0;
};

AdminReportsController.createGroupId = function(userId, target) {
    return userId + "_" + target;
};

AdminReportsController.parseGroupId = function(id) {
    var arr = id.split("_", 2);
    return {userId: arr[0], target:arr[1]};
};

AdminReportsController.DefaultSelectOptions = {
    selectFields: "",
    filterFields : [],
    populate: ["userId"],
    defaultOrder : {ts: -1} };

module .exports = AdminReportsController;