/**
 * Created by Roman Bespalov on 12/7/15.
 */

var AdminUtils = function() {

};

AdminUtils.prototype = {};

/**
 * Returns expression used to set Post.deleted field.
 * @param isAuthor whether post's author deleted (10) or post itself (01)
 * @param isDeleted is deleted
 * @returns {{$bit: {deleted: *}}}
 */
AdminUtils.getPostDelededUpdateExpr = function(isAuthor, isDeleted) {
    var operation = isAuthor ? (isDeleted ? { or: 1} : { and: 2})
        : (isDeleted ? { or : 2 } : { and: 1 });

    return { $bit: { deleted: operation}};
};

module.exports = AdminUtils;