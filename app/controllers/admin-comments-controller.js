/**
 * Created by Roman Bespalov on 12/7/15.
 */
/**
 * Created by Roman Bespalov on 11/22/15.
 */
var util = require('util');
var async = require("async");

var Post = require("../models/post");
var ODataController = require("./odata-controller");

function override(child, fn) {
    child.prototype[fn.name] = fn;
    fn.inherited = child.super_.prototype[fn.name];
}

// post comments stuff
var AdminCommentsController = function(selectOptions) {
    AdminCommentsController.super_.call(this, selectOptions || AdminCommentsController.DefaultSelectOptions);
};

util.inherits(AdminCommentsController, ODataController);

//region /////////////////////////////////   OVERRIDES   ///////////////////////////////////////////////

// methodName.inherited.call(this, par);
override(AdminCommentsController, function processPatchReq(req, res, next, id) {
    this.patchComment(req, res, next, id);
});

override(AdminCommentsController, function deleteObject(req, res, next, id, instanceName) {
    res.sendStatus(404);
});

override(AdminCommentsController, function get_Model() {
    return Post; // Comments are array in the post now, so the main model to extract its values is Post
});

override(AdminCommentsController, function get_ModelDisplayName() {
    return "Comment";
});

//endregion


/**
 * Updates comment field.
 * @param req
 * @param res
 * @param next
 * @param id
 */
AdminCommentsController.prototype.patchComment = function(req, res, next, id) {
    var data = req.body;
    this.get_Model().update({"comments._id": id}, {$set: AdminCommentsController._toUpdateExpession(data)}, (err, result) => {
        this.output(err, req, res, result);
    });
};

// Data output format: {"comments.$.[fieldName]": [value]}
AdminCommentsController._toUpdateExpession = function(data) {
    var expr = {};
    if (data)
        for (var propName in data)
            expr["comments.$." + propName] = data[propName];

    return expr;
};



AdminCommentsController.DefaultSelectOptions = { };

module .exports = AdminCommentsController;