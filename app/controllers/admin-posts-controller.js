/**
 * Created by Roman Bespalov on 11/22/15.
 */
var util = require('util');
var async = require("async");

var Post = require("../models/post");
var Place = require("../models/place");
var AdminUtils = require("./admin-utils");
var ODataUtils = require("../util/odata-utils");
var ODataController = require("./odata-controller");
var AdminUsersController = require("./admin-users-controller");

function override(child, fn) {
    child.prototype[fn.name] = fn;
    fn.inherited = child.super_.prototype[fn.name];
}

var AdminPostsController = function(selectOptions) {
    AdminPostsController.super_.call(this, selectOptions || AdminPostsController.DefaultSelectOptions);
};

util.inherits(AdminPostsController, ODataController);



//region //////////////////////////////////////////////    OVERRIDES   ////////////////////////////////////////////

/**
 * GET methodName.inherited.call(this, par);
 */
override(AdminPostsController, function processRegularReq(req, res, resource, callback) {
    processRegularReq.inherited.call(this, req, res, resource, (err, result, count) => {
        if(err)
            res.send(err);
        else {
            async.map(result, (post, callback) => {
                Place.findOne({placeId: post.placeId }, (err, place) => {
                    var obj = post.toObject();
                    if(place && (place.name || place.land)) // fake place, has not real Google PlaceId
                        obj.placeId = place.name || place.land;
                    else
                        obj.placeId = "google:" + obj.placeId;

                    callback(null, obj);
                });
            },
                (err, items) => {
                    res.json(ODataUtils.createResult(items || [], count));
                }
            );
        }
    });
});

/**
 * GET function
 */
override(AdminPostsController, function processFuncReq(req, res, resource) {
    switch (resource.func) {
        case  "likes":
            this.findPostLikes(req, res, resource);
            break;
    }
});

/**
 * POST
 */
override(AdminPostsController, function processPostReq(req, res, id, operation) {
    if (operation == "toggledeleted")
        this.togglePostDeleted(req, res, id);
    else
        res.status(404).send("Not found: " + operation);
});

/**
 * DELETE - do not delete posts
 */
override(AdminPostsController, function deleteObject(req, res, next, id, instanceName) {
    res.sendStatus(404);
});

override(AdminPostsController, function get_Model() {
    return Post;
});

override(AdminPostsController, function get_ModelDisplayName() {
    return "Post";
});

//endregion


AdminPostsController.prototype.findPostLikes = function(req, res, resource) {
    var postId = resource.filter._id;
    Post.findOne({_id: postId}, (err, post) => {
        if (!err) {
            var filter = { _id : { "$in" : post.likes }};
            (new AdminUsersController(null)).processRegularReq(req, res, filter); // return users
        }
        else
            throw err;
    });
};


AdminPostsController.prototype.togglePostDeleted = function(req, res, id) {
    var self = this;
    var doUpdate = function (id, isOn, callback) {
        self.get_Model().update({"_id": id}, AdminUtils.getPostDelededUpdateExpr(false, isOn), callback);
    };

    this._toggleUpdate(req, res, id, doUpdate);
};


AdminPostsController.prototype._toggleUpdate = function(req, res, id, updateFunc) {
    async.waterfall([
        (callback) => {
            updateFunc(id, true, (err, result) => {
                if (err)
                    callback(err, null);
                else
                    callback(null, result);
            });
        },
        (result, callback) => {
            if (!result.nModified) {
                updateFunc(id, false, (err, result) => {
                    if (err)callback(err, null);
                    else
                        callback(null, result);
                });
            }
            else
                callback(null, result);
        }
    ], (err, result) => {
        this.output(err, req, res, result);
    });
};




AdminPostsController.DefaultSelectOptions = {
    selectFields: "description ts photo authorId isCheckedIn placeId isPostedOnFacebook deleted deteledDisplay",
    filterFields : ["description"],
    populate : [{field:"authorId", select:"fullname email"}],
    defaultOrder : {ts: -1}
};

module .exports = AdminPostsController;