/**
 * Created by Roman Bespalov on 11/23/15.
 */
var util = require('util');
var express = require("express");
var ODataUtils = require("../util/odata-utils");
var ODataMongooseHandler = require("../middleware/odata-mongoose-handler");
var async = require("async");

/**
 *
 * @param selectOptions = {
 *              selectFields: "fullname nickname email city created",
                filterFields : ["fullname", "nickname", "email"],
                defaultOrder : {created: -1} }
 * @constructor
 */
var ODataController = function(selectOptions) {
    this.selectOptions = selectOptions || {};
};

ODataController.prototype =
{
    router : function()
    {
        var router = express.Router();

        // GET
        router.get("/:filter?/:operation?", (req, res, next) => {
            var params = ODataUtils.parseGETUriParams(req.params.filter || "", req.params.operation || "" );
            if(params.func) {
                // hanle function request (result model can differs from controller's model) => users/_id(1234123)/$likes
                this.processFuncReq(req, res, params);
            }
            else if(params.filter && params.field) { // returns single field value => users/_id(1234123)/friends
                this.processSingleFieldReq(req, res, this.get_Model(), params.filter, params.field);
            }
            else { // regular request over current model => users/ || users/_id(1234123)
                this.processRegularReq(req, res, params.filter);
            }
        });

        // DELETE
        router.delete("/:id/:instance?", (req, res, next) => {
            var params = ODataUtils.parseDELETEUriParams(req.params.id, req.params.instance);
            if (!params.id) {
                res.sendStatus(404); // nothing to delete
                return;
            }

            this.deleteObject(req, res, next, params.id, params.instance);
        });

        // POST
        router.post("/:id/:operation?", (req, res, next) => {
            var params = ODataUtils.parsePOSTUriParams(req.params.id, req.params.operation);
            if (!params.id) {
                res.sendStatus(404); // nothing to post
                return;
            }

            this.processPostReq(req, res, params.id, params.operation);
        });

        // PATCH
        router.patch("/:id/:instance?", (req, res, next) => {
            var params = ODataUtils.parsePATCHUriParams(req.params.id, req.params.instance);
            if (!params.id) {
                res.sendStatus(404); // undefined entity
                return;
            }

            if(!req.body) {
                res.sendStatus(200); // nothing to patch
                return;
            }

            this.processPatchReq(req, res, next, params.id, params.instance);
        });

        return router;
    },


    /**
     * Services SHOULD support PATCH as the preferred means of updating an entity
     * The semantics of PATCH, as defined in [RFC5789], is to merge the content in the request payload with the [entity’s] current state,
     * applying the update only to those components specified in the request body
     * @param req
     * @param res
     * @param next
     * @param id
     */
    processPatchReq : function(req, res, next, id, instance) {
        var data = req.body;
        var modelName = this.get_ModelDisplayName();

        async.waterfall([
            (callback) => { // extract an item to patch
                this.get_Model().findOne({_id: id}, (err, item) => {
                    if (err)
                        return callback(err);

                    if(!item) // check item exists
                        return callback(Error("Can't modify. " + modelName + ' with id:"' + id + '" is not found.'), null);

                    callback(null, item); // next
                });
            },
            (item, callback) => {
                var errorMessage = this.validatePatchData(item, data); // validate patched item
                if (errorMessage)
                    return callback(Error(errorMessage));

                res.item = item; // save not modified item

                this.get_Model().update({_id: id}, {$set: data}, (err, updateResult) => { // apply update
                    if (err)
                        return callback(err);

                    callback(null, updateResult); // next
                });
            },
            (updateResult, callback) => {
                if(updateResult.nModified > 0 && this.needHandleItemPatched(res.item, data, id)) {
                    this.get_Model().findOne({_id: id}, (err, newItem) => { // extract new item, if the one was modified and need to be handled
                        if (err)
                            return callback(err);

                        this.onItemPatched(res.item, newItem, data, id, (err, result) =>
                        {  if(err) callback(err); }); // perform additional post-modified steps
                    });
                }

                callback(null, updateResult); // next
            }
        ],
            (err, updateResult) => {
                this.output(err, req, res, updateResult); // pass update result to output
            }
        );
    },

    validatePatchData: function(item, dataToPatch) {
        return null;
    },

    needHandleItemPatched: function(item, data, id) {
        return false;
    },

    onItemPatched: function(item, newItem, data, id, errorCallback) {

    },

    /**
     * Handle POST requests
     * @param req
     * @param res
     * @param id
     * @param operation
     */
    processPostReq : function(req, res, id, operation) {
        throw new Error("ODataController function: " + operation + " - is not implemented in base class!");
    },

    /**
     * Objects deleting default implementation
     * @param req
     * @param res
     * @param next
     * @param id
     * @param instanceName
     */
    deleteObject : function(req, res, next, id, instanceName) {
        var model = this.resolveDeleteModel(instanceName);
        if (!model) {
            res.sendStatus(404);
            return;
        }

        model.findOne({_id: id}, (err, item) => {
            if(err)
                res.send(err);
            if(!item)
                res.sendStatus(200);

            item.remove((err) => {
                res.send(err || 200);
            });
        });
    },

    resolveDeleteModel: function(instanceName) {
            return !instanceName ? this.get_Model() : null;
    },

    /**
     * Handle GET request as custom function
     * @param req
     * @param res
     * @param params
     */
    processFuncReq: function(req, res, params) {
        throw new Error("ODataController function: " + params.func + " - is not implemented in base class!");
    },

    /**
     * GET fingle field value (can be simple type or array)
     * @param req
     * @param res
     * @param resourceFilter
     * @param resourceField
     * @param model
     */
    processSingleFieldReq : function(req, res, model, resourceFilter, resourceField) {
        // override controller's model default select options
        var options = {
            selectFields : resourceField, filterFields : null, defaultOrder : null, isSingleFieldResult : true
        };

        // TODO: move to PostsController!!!
        if(resourceField == "comments")
            options["populate"] = [{field:"comments.userId", select:"fullname email"}];

        (new ODataMongooseHandler(model, options, resourceFilter ? [resourceFilter] : [])).process(req, res);
    },


    output : function (err, req, res, result) {
        if (err) {
            var _err = {
                code: err.code || 500,
                message: err.message || util.format(err)
            };

            if (global.config.env !== "production") {
                if (err.errors) _err.errors = err.errors;
                if (err.stack) _err.stack = err.stack;
            }

            res.json({
                status: "error",
                data: null,
                error: _err
            });
        }
        else {
            res.json({
                status: "success",
                data: result,
                error: null
            });
        }
    },

    /**
     *  Handle regular "select" GET request = model/field(value), where resourceFilter = field(value)
     * @param req
     * @param res
     * @param resourceFilter
     */
    processRegularReq: function(req, res, resourceFilter, callback) {
        var filters = [];
        if(resourceFilter)
            filters.push(resourceFilter);

        var controllerFilter = this.getRegularReqFilter();
        if(controllerFilter)
            filters.push(controllerFilter);

        (new ODataMongooseHandler(this.get_Model(), this.selectOptions, filters)).process(req, res, callback);
    },

    getRegularReqFilter : function() {
        return null;
    },

    // Returns default model
    get_Model : function() {
        throw new Error("ODataController.get_Model is not implemented!");
    },

    get_ModelDisplayName : function() {
        throw new Error("ODataController.get_ModelDisplayName is not implemented!");
    }
};


module.exports = ODataController;