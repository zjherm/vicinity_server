/**
 * Created by romanb on 11/17/15.
 */
var util = require('util');
var async = require("async");
var User = require("../models/user");
var Post = require("../models/post");

var AdminUtils = require("./admin-utils");
var ODataController = require("./odata-controller");

// Вызов метода родительского класса -> methodName.inherited.call(this, par);
function override(child, fn) {
    child.prototype[fn.name] = fn;
    fn.inherited = child.super_.prototype[fn.name];
}

var AdminUsersController = function(selectOptions) {
    AdminUsersController.super_.call(this, selectOptions || AdminUsersController.DefaultSelectOptions);
};

util.inherits(AdminUsersController, ODataController);


override(AdminUsersController, function validatePatchData(item, data) {
    var result = null;
    var itemName = this.get_ModelDisplayName();

    // prevent deleted user status modifications
    if(data["userStatus"] && (item["userStatus"] == "Deleted"))
        result = "Can't modify deleted " + itemName + " status.";

    return result;
});


override(AdminUsersController, function needHandleItemPatched(user, data, id) {
    if(data["userStatus"] && (data.userStatus != user.userStaus))
        return true; // inform to handle itemPatched if userStatus gonna change

    return false;
});


override(AdminUsersController, function onItemPatched(user, newUser, data, userId, errorCallback) {
    if(data["userStatus"] && (user.userStatus != newUser.userStaus)) // if userStatus changed
        this.onUserStatusChanged(newUser, userId, errorCallback);
});


AdminUsersController.prototype.onUserStatusChanged = function(user, userId, errorCallback) {
    var userStatus = user.userStatus;
    var isDirtyUser = userStatus != "Normal";

    // mark posts as deleted for blocked/deleted authors, and not deleted otherwise
    Post.update({authorId: userId}, AdminUtils.getPostDelededUpdateExpr(true, isDirtyUser), {multi: true}, (err, result) => {
        if(err)
            errorCallback(err);
        else {
            var pushType = "";
            if (userStatus == "Blocked") {
                pushType = "push.block";
            } else if (userStatus == "Deleted") {
                pushType = "push.delete";
            }
            if (pushType != "") {
                global.server.emit(pushType, {
                    userId: userId
                });
            }
        }
    });
};


override(AdminUsersController, function processFuncReq(req, res, resource) {
    switch (resource.func) {
        case  "friends":
            this.findUserFriends(req, res, resource);
            break;
        case  "blocked":
            this.findUserBlocked(req, res, resource);
            break;
    }
});

AdminUsersController.prototype.findUserFriends = function(req, res, resource) {
    var userId = resource.filter._id;
    User.findOne({_id: userId}, (err, user) => {
        if (!err) {
            var filter = { _id : { "$in" : user.friends }};
            this.processRegularReq(req, res, filter);
        }
        else
            throw err;
    });
};

AdminUsersController.prototype.findUserBlocked = function(req, res, resource) {
    var userId = resource.filter._id;
    User.findOne({_id: userId}, (err, user) => {
        if (!err) {
            var filter = { _id : { "$in" : user.blockedUsers }};
            this.processRegularReq(req, res, filter);
        }
        else
            throw err;
    });
};


override(AdminUsersController, function getRegularReqFilter() {
    // exclude admin from all results TODO: move admin to separate table
    return { email : {$ne: "info@getvicinityapp.com"}};
});

override(AdminUsersController, function get_Model() {
    return User;
});

override(AdminUsersController, function get_ModelDisplayName() {
    return "User";
});

AdminUsersController.DefaultSelectOptions = { // user controller configuration
    selectFields: "fullname nickname email userStatus created city country gender avatarUrl bio isEmailConfirmed",
    filterFields : ["fullname", "nickname", "email"],
    defaultOrder : {created: -1} };

module .exports = AdminUsersController;