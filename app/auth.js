/**
 * Authorization helper
 * @type {Session|exports}
 */
var Session = require("./models/session");
var User = require("./models/user");
var async = require("async");
var url = require('url');
var _ = require("lodash");
var crypto = require('crypto');
var Facebook = require("./wrappers/facebook");
var Stripe = require('stripe');
var Mail = require("./wrappers/mail");
var EmailConfirmationToken = require("./models/_tokens-email-confirm");
var STATUS = require('./response-statuses')
var templates = require('./templates');
var mailchimp = require("./wrappers/mailchimp");

var Auth = function (token) {
	if (token) {
		this.social = new Facebook(token);
	}
};


Auth.prototype.login = function (login, pass, cb) {

	pass = crypto.createHash('md5')
		.update(pass)
		.digest('hex')

	async.waterfall([
		(_cb) => {

			User.findOne({
				$and: [
					{
						$or: [
							{nickname: {$eq: login}},
							{email: {$eq: login}}
						]
					},
					{password: {$eq: pass}},
					{userStatus: {$nin:["Deleted"]}}
				]
			}, _cb);

		}, (user, _cb) => {
			if (!user) return _cb(null, null);
			if (!user.email) return _cb(null, user);
			var stripe = Stripe(global.config.stripe.apiKey);
			stripe.customers.create({
				email: user.email
			}, (err, customer) => {
				console.log('stripe connection error')
				if (err) return _cb(null, user);
				user.stripeId = customer.id;
				_cb(null, user);
			});

		}, (user, _cb) => {
			if (user) {
				Session.create(user, _cb);
			} else {
				_cb(null, null)
			}

		}
	], cb)

};

Auth.prototype.loginWithFbToken = function (data, next) { // with account creation if new
	var fbData;
	var _this = this;
	async.waterfall([
		(cb) => {
			_this.social.getMyProfile(cb);

		}, (fbUser, cb) => {
			fbData = fbUser;

			User.findOne({
				"facebook.id": fbUser.id,
				"userStatus": {
					$nin:["Deleted"]
				}
			}, (err, user) => {
				cb(err, {user, fbUser})
			});
		}, ({user, fbUser}, cb) => {

			if (user) { // authorize old user with linked facebook

				return cb(null,{user, fbUser})

			} else if (fbUser.email) { // try to authorize old user without linked facebook, then link with facebook

				User.findOne({
					"email": fbUser.email,
					"userStatus": {
						$nin:["Deleted"]
					}
				}, (err, user) => {
					cb(err, {user, fbUser})
				});

			} else { // no user with this facebook account, no email in facebook account - nothing to do
				return next(STATUS.noEmailInFbAccount)
			}

		}, ({user, fbUser}, cb) => {

			if (user) { // there IS user in DB with 'facebook.id' == facebook.id or 'email' == facebookId.email
				if ( _.get(user, 'facebook.id') ) {
					cb(null, user);
				} else {  // old user without linked facebook account
					user.facebook = fbUser;
					user.save((err, user) => {
						cb(null,user)
					})
				}
			} else { // there is NO user in DB with this FacebookId
				mailchimp.scheduleEmail(fbUser.email, function() {
					console.log("Mailchimp campaign scheduled");
				});

				User.create(fbData, function (err, user) {
					if (err) return next(err);

					// https://trello.com/c/NDqg0Met/70-fb-email
					//_this.confirmEmail({
					//	userId: user._id || user.userId,
					//	userName: user.fullname,
					//	email: user.email
					//}, (err, info) => {
					//	//console.log('email confirmation')
					//	//console.log(err, info)
					//})
					// do not wait for response from email confirmation

					cb(null, user);
				});
			}
		}, (user, cb) => {
			if (!user) return cb(null, null);
			if (!user.email) return cb(null, user);
			var stripe = Stripe(global.config.stripe.apiKey);
			stripe.customers.create({
				email: user.email
			}, (err, customer) => {
				if (err) return cb(err);
				user.stripeId = customer.id;
				user.save((err, user) => {
					cb(null, user);
				})
			});

		}, (user, cb) => {
			user.facebookToken = _this.social.token;
			user.save();
			Session.create(user, cb);
		}
	], next);
};

Auth.prototype.confirmEmail = ({userId, userName, email}, cb) => {

	async.waterfall([
		(_cb) => {
			crypto.randomBytes(48, (ex, buf) => {
				let token = buf.toString('hex');
				_cb(null, token)
			});

		}, (token, _cb) => {
			EmailConfirmationToken.create({
				userId: userId,
				token: token,
				email: email,
				createdAt: new Date()
			}, (err, confirmation) => {
				_cb(err, confirmation)
			})
		}, (confirmation, _cb) => {
			let query = {
				userId: String(userId),
				token: confirmation.token,
			}
			if (userName) query.username = userName.split(' ')[0]

			let link = url.format({
				protocol: 'http',
				hostname: global.config.mail.urlHost,
				port: global.config.mail.urlPort,
				pathname: '/api/email-confirm',
				query: query
			});

			let mail = new Mail({
				emails: [email],
				letter: {
					subject: 'E-Mail Confirmation from Vicinity - The Local Network',
					html: templates.emails.confirmation({link})
				}
			});
			mail.send(_cb);
		}
	], (err, res) => {
		//if (err) console.log('confirmEmail error', err);
		cb(err, res);
	})

}

module.exports = Auth;
