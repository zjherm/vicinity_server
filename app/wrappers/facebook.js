/**
 * From hello 1.0
 * @type {FB|exports}
 */
var fb = require('fb');
var async = require("async");
var _ = require('lodash');
var request = require('request');


var postParams = {
	"Текст": {
		url: "me/feed",
		data: {
			message: 'Текст'
		}
	},
	"текст с чекином": {
		"url": "me/feed",
		"data": {
			message: "текст с чекином",
			place: 561069384007478
		}
	},
	"Чекин": {
		"url": "me/feed",
		"data": {
			place: 561069384007478
		}
	},
	"Чекин с фото": {
		"url": "me/feed",
		"data": {}
	},
	"фото": {
		"url": "me/feed",
		"data": {}
	},
	"фото с чекином": {
		"url": "me/feed",
		"data": {}
	},
	"фото с текстом и чекином": {
		"url": "me/feed",
		"data": {}
	}
}

// fb user api docs https://developers.facebook.com/docs/graph-api/reference/user/

var Facebook = function (accessToken) {
	if (accessToken) {
		this.token = accessToken;
		fb.setAccessToken(accessToken);
	}
};

var faceBookPostTypes = {
	full: 'full',
	image: 'image',
	place: 'place',
	note: 'note'
};

Facebook.prototype = {

	postTypes: faceBookPostTypes,

	getMyProfile: function (cb) {
		global.logger.info('getMyProfile', this.token);

		if (this.token === '123') return cb(null, {id: '10153295093454236'});

		fb.api('me', {fields: ['id', 'name', 'email', 'gender', 'location{location}']}, function (res) {
			if (!res || res.error) {
				global.logger.err(res);
				cb({
					code: "FACEBOOK_ERROR",
					message: res ? (res.error ? res.error.message : 'FB request error') : 'FB Request error'
				});
			}
			else {
				global.logger.debug("Facebook profile:", res);
				cb(null, res);
			}
		});
	},

	getFriendsFromFacebook: function (fbId, cb) {
		fb.api(`${fbId}/friends`, {fields: ['id', 'name']}, function (res) {
			if (!res || res.error) {
				cb({
					code: "FACEBOOK_ERROR",
					message: res ? (res.error ? res.error.message : 'FB request error') : 'FB Request error'
				});
			}
			else {
				cb(null, res.data || null);
			}
		});
	},

	// TODO: not used, remove
	//getFriends: function (fbId, cb) {
	//	var self = this;
	//	neo.getMine(fbId, function (err, results) {
	//		if (err) return cb(err);
	//		if (!results.length) {
	//			self.getFriendsFromFacebook(cb);
	//		} else {
	//			cb(err, results);
	//		}
	//	});
	//},

	// TODO: not used, remove
	//commonFriends: function (userId, friendId, cb) {
	//	neo.getCommon(userId, friendId, cb);
	//},

	sendNotification: function (fbIds, text, session, cb) {
		global.logger.debug("Notifications", fbIds, text, session);
		text = text || "";
		fb.api("oauth/access_token", {
			client_id: global.config.fb.appId,
			client_secret: global.config.fb.appSecret,
			grant_type: "client_credentials"
		}, function (data) {
			global.logger.debug("Notifications", data);
			async.each(fbIds, function (fbId, next) {
				fb.api(fbId + "/notifications", "POST", {
					template: session.nickname + " приглашает вас в чат Hello! " + text,
					href: session.pageUrl,
					ref: session.pageUrl,
					access_token: data.access_token
				}, function (res) {
					next(res.error);
				});
			}, cb);
		});
	},

	checkPermissions: function (cb) {
		fb.api('me/permissions', function (res) {
			var friends = false,
				email = false;

			if (res.error) {
				global.logger.error(res.error);
				return;
			}

			res.data.forEach(function (permission) {
				if (permission.permission === "email" && permission.status === "granted") email = true;
				if (permission.permission === "user_friends" && permission.status === "granted") friends = true;
			});

			if (friends && email) {
				cb();
			} else {
				fb.api('me/permissions', "delete", function () {
					cb({code: "FB_BAD_PERMISSIONS"});
				});
			}
		});
	},

	// TODO: not used, wipe
	//updateFriends: function (fbUser, cb) {
	//	fb.api("me/friends", function (results) {
	//		neo.update(fbUser, results.data, cb);
	//	});
	//},

	createWallPost: function (postParams, cb) {
		var postType = this.definePostType(postParams);
		console.log('FB wallpost', postType + " , ", postParams.facebookPostType);
		var tasks = [
			this.preparePostOptions.bind(this, postType, postParams)
		];
		switch (postType) {
			case this.postTypes.full :
				tasks.push(this.createFullPost);
				break;
			case this.postTypes.image :
				tasks.push(this.createImagePost);
				break;
			case this.postTypes.place :
				tasks.push(this.createPlacePost);
				break;
			case this.postTypes.note :
				tasks.push(this.createNotePost);
				break;
			default:
				tasks.push(this.createNotePost)
		}
		async.waterfall(tasks, cb)
	},
	createFullPost: function (postParams, cb) {
		var url = (postParams.facebookPostType == 'photo') ? '/me/photos': 'me/feed'
		var postData = postParams.data;

		console.log("FB createFullPost", postData, ' '+url);
		fb.api('/me/photos', 'post', postData, function (response) {
			cb(response.error, response)
		});
	},
	createPlacePost: function (postParams, cb) {
		var postData = postParams.data;

		console.log("FB createPlacePost", postData);
		fb.api('/me/feed', 'post', postData, function (response) {
			cb(response.error, response);
		});
	},
	createImagePost: function (postParams, cb) {
		var postData = postParams.data;

		console.log("FB createImagePost", postData);

		fb.api('/me/photos', 'post', postData, function (response) {
			cb(response.error, response)
		});
	},
	createNotePost: function (postParams, cb) {
		var postData = postParams.data;

		console.log("FB createNotePost", postData);
		fb.api('/me/feed', 'post', postData, function (response) {
			cb(response.error, response);
		});
	},

	updateWallPost: function (postParams, cb) {
		var postType = this.definePostType(postParams);
		console.log('updateWallPost', postType);
		if (!postParams.id) return cb('no post id ' + postId);
		var tasks = [
			this.preparePostOptions.bind(this, postType, postParams)
		];
		switch (postType) {
			case this.postTypes.full :
				tasks.push(this.updateFullPost);
				break;
			case this.postTypes.image :
				tasks.push(this.updateImagePost);
				break;
			case this.postTypes.place :
				tasks.push(this.updatePlacePost);
				break;
			case this.postTypes.note :
				tasks.push(this.updateNotePost);
				break;
			default:
				tasks.push(this.updateNotePost)
		}
		async.waterfall(tasks, cb)
	},
	updateFullPost: function (postParams, cb) {
		var postData = postParams.data || {};
		var postId = postParams.id;

		postData = _.pick(postData, function (val) {
			if (_.isNull(val)) {
				return true
			} else {
				return !_.isEmpty(val)
			}
		});

		console.log("FB updateFullPost: id=" + postId);
		console.log(postData);
		fb.api('/' + postId, 'post', postData, function (response) {
			cb(response.error, response);
		});
	},
	updatePlacePost: function (postParams, cb) {
		var postData = postParams.data || {};
		var postId = postParams.id;

		postData = _.pick(postData, function (val) {
			if (_.isNull(val)) {
				return true
			} else {
				return !_.isEmpty(val)
			}
		});

		console.log("FB updatePlacePost: id=" + postId);
		console.log(postData);
		fb.api('/' + postId, 'post', postData, function (response) {
			cb(response.error, response);
		});
	},
	updateImagePost: function (postParams, cb) {
		var postData = postParams.data || {};
		var postId = postParams.id;

		if (!imageUrl) return cb(null);
		imageUrl += "";
		var imageOptions = {
			url: imageUrl,
			caption: postParams.description,
			no_story: true
		};
		console.log("FB updateImagePost: id=" + postId);
		console.log(postData);
		fb.api('/me/photos', 'post', imageOptions, function (result) {
			if (result.error) {
				global.logger.error(result.error);
				result = {}
			}

			cb(null, result)
		});
	},
	updateNotePost: function (postParams, cb) {
		var postData = postParams.data || {};
		var postId = postParams.id;

		postData = _.pick(postData, function (val) {
			if (_.isNull(val)) {
				return true
			} else {
				return !_.isEmpty(val)
			}
		});

		console.log("FB updateNotePost: id=" + postId);
		console.log(postData);
		fb.api('/' + postId, 'post', postData, function (response) {
			cb(response.error, response);
		});
	},

	deleteWallPost: function (postParams, cb) {
		var postData = postParams.data || {};
		var postId = postParams.id;
		console.log("FB deleteWallPost: id=" + postId);
		console.log(postData);
		if (!postId) return cb('no post id');
		global.logger.debug("FB DELETE POST", postParams);
		fb.api('/' + postId, 'delete', function (response) {
			cb(response.error, response);
		});
	},

	definePostType: function (params) {
		var withImage = !!(params.photo && params.photo.length);
		var withPlace = !!(params.facebookPlaceId && params.isCheckedIn);
		var withMessage = !!(params.postMessage && params.postMessage.length);

		if (withImage && withPlace) return this.postTypes.full;
		else if (withImage) return this.postTypes.image;
		else if (withPlace) return this.postTypes.place;
		else if (withMessage) return this.postTypes.note;
		else return undefined
	},
	preparePostOptions: function (postType, postParams, cb) {
		postParams.postMessage || (postParams.postMessage = "")

		console.log('preparePostOptions');
		console.log('postType', postType);
		console.log('postParams', postParams);

		//var postData, resultPostData;
		//
		//var testData = {
		//	photo: '161876520824189',
		//	place: '561069384007478'
		//};

		// TODO remove next string after local testing !!!!!!
		//if (postParams.photo) postParams.photo =  "http://static.guim.co.uk/sys-images/Arts/Arts_/Pictures/2015/3/2/1425321380162/In-the-Middle-Somewhat-El-009.jpg";

		// https://developers.facebook.com/docs/graph-api/reference/v2.4/user/feed
		var facebookPostData = _.defaultsDeep({
			// either link, place, or message must be supplied

			message: "",

			link: undefined,
				// additional link fields
				picture: undefined,
				name: undefined,
				caption: undefined,
				description: undefined,

			place: undefined,

			tags: undefined, // csv[string]
			object_attachment: undefined
			//privacy: {
			//	value: undefined,  // enum{'EVERYONE', 'ALL_FRIENDS', 'FRIENDS_OF_FRIENDS', 'CUSTOM', 'SELF'}
			//	allow: undefined, // string|csv[string]
			//	deny: undefined // string|csv[string]
			//},
		});

		// https://developers.facebook.com/docs/graph-api/reference/photo#u_0_1y
		var facebookImageData = _.defaultsDeep({
			caption: "", // string; The description of the photo
			url: "", // string; URL
			place: undefined, // place tag, Page ID of a place associated with the photo
			no_story: false, // boolean; if true, this will suppress the News Feed story that is automatically generated
			published: true, // boolean;
			attempt: 3
			//id: 161917570820084,
			//vault_image_id: "161917570820084",
		});

		var postMsg = postParams.postMessage.length > 0 ? postParams.postMessage + " - via Vicinity. Download at get.vcnty.co/fb" : "";
		switch (postType) {
			case this.postTypes.full :
				facebookImageData.caption = postParams.postMessage;
				facebookImageData.url = postParams.photo;
				facebookImageData.place = postParams.facebookPlaceId;
				if (_.isEmpty(compactObj(facebookImageData))) return cb('no_data')
				cb(null,{data:facebookImageData});
				break;
			case this.postTypes.image :
				facebookImageData.caption = postParams.postMessage;
				facebookImageData.url = postParams.photo;
				if (_.isEmpty(compactObj(facebookImageData))) return cb('no_data')
				cb(null,{data:facebookImageData});
				break;

			case this.postTypes.place :
				facebookPostData.message = postMsg;
				facebookPostData.place = postParams.facebookPlaceId;
				if (_.isEmpty(compactObj(facebookPostData))) return cb('no_data')
				cb(null,{data:facebookPostData});
				break;

			default:
				facebookPostData.message = postMsg;
				if (_.isEmpty(compactObj(facebookPostData))) return cb('no_data')
				cb(null,{data:facebookPostData});
		}

	}
};

function compactObj (obj) {
	return _.transform(obj, (result, val, key) => {
		val ? (result[key] = val) : (delete result[key])
	});
}

function uploadImageToFB(imageUrl, cb) {
	if (!imageUrl) return cb(null);
	imageUrl += "";
	var imageOptions = {
		url: imageUrl,
		no_story: true
	};
	fb.api('/me/photos', 'post', imageOptions, function (result) {
		if (result.error) {
			global.logger.error(result.error);
			result = {}
		}

		cb(null, result)
	});
}

module.exports = Facebook;