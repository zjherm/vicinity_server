var _ = require('lodash');
var async = require('async');
var User = require("../models/user");
var Notification = require("../models/notification");
var apn = require('apn');

function Push() {

}

Push.fn = Push.prototype;

Push.fn.pushForNotification = function (notif, userIDs, next = ()=>{}) {
	if (_.isEmpty(userIDs)) {
		next(); return;
	}

	var self = this;
	var payload = notif.getPayload();

	var alert = payload.commentText;

	var contentAvailable; var sound;
	if (payload.type == 'user.block' || payload.type == 'user.delete' || payload.type == 'user.confirm_email') {
		contentAvailable = true;
		alert = null;
		sound = null;
	} else {
		contentAvailable = false;
	}

	async.waterfall([
		(next) => {
			User.find({_id: {$in: userIDs}}, next);
		},
		(users, next) => {
			// if (err) {
			// 	global.logger.error('Error while fetching users for push notifications:', err);
			// 	next(err);
			// }
			async.each(users, (user, next) => {
				if (notif.actorId && (user._id == notif.actorId)) {
					next(); return; // self liker
				}

				var addUserNotification = () => {
						if (!_.isArray(notif.receivers)) {
							notif.receivers = [];
						}
						notif.receivers.push({ userId: user._id, viewed: false });
						next();
				};

				if ((payload.type == 'post.connection' && !user.isFriendPostedPushNotificationEnabled) || /*Your connection johnsmith posted: word up|Your connection johnsmith posted an image|Your connection johnsmith checked into <location>*/
						(payload.type == 'like' && !user.isUserPostLikedPushNotificationEnabled) || /*johnsmith liked your post*/
						(payload.type == 'comment' && !user.isUserPostCommentedPushNotificationEnabled) || /*johnsmith commented: word up*/
						(payload.type == 'comment.publish' && !user.isOtherPostCommentedPushNotificationEnabled) || /*johnsmith also commented: word up*/
						(payload.type == 'mention.comment' && !user.isUserMentionedPushNotificationEnabled) || /*johnsmith mentioned you in a post: word up @elijahk4*/
						(payload.type == 'post.vicinity' && !user.isPostWithinVicinityPushNotificationEnabled) || /*johnsmith posted in your Vicinity|johnsmith posted in your Vicinity: word up|johnsmith posted in your Vicinity*/
						false ) {
							addUserNotification(); return;
						}

				User.getCountUnviewedNotifications(user._id, function(unreadNotificationsCount) {
					async.each(user.pushTokens, (token, cont) => {
						self.push(token, {
							alert: alert,
							payload: payload,
							sound: sound,
							badge: unreadNotificationsCount + 1, // +1 is for current notification
							contentAvailable: contentAvailable
						}, () => {
							cont();
						});
					}, () => {
						addUserNotification();
					});
				});
			}, () => {
					notif.save(() => {
						next();
					});
			});
		}], next);
};

Push.fn.push = function (token, notification, cb) {
	global.logger.debug("Notifications", token, notification);
	if (!global.config.apn) {
		return global.logger.error("Create local_config.json with apn section. It is required");
	}
	if (!token) {
		return global.logger.warn("No push token for user");
	}
	//token = new Buffer(token, "hex");

	try {


		var service = new apn.Connection(global.config.apn);
		service.on('connected', function () {
			global.logger.debug("Connected");
		});

		service.on('transmitted', function (notification, device) {
			global.logger.debug("Notification transmitted to:" + device.token.toString('hex'));
		});

		service.on('transmissionError', function (errCode, notification, device) {
			global.logger.error("Notification caused error: " + errCode + " for device ", device, notification);
			if (errCode == 8) {
				global.logger.debug("A error code of 8 indicates that the device token is invalid. This could be for a number of reasons - are you using the correct environment? i.e. Production vs. Sandbox");
			}
		});

		service.on('timeout', function () {
			global.logger.error("Connection Timeout");
		});

		service.on('disconnected', function () {
			global.logger.debug("Disconnected from APNS");
		});

		service.on('socketError', global.logger.error);

		var myDevice = new apn.Device(token);

		var note = new apn.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
		note.sound = notification.sound;
		note.alert = notification.alert;
		note.payload = notification.payload;
		note.badge = notification.badge;
		note.contentAvailable = notification.contentAvailable;

		global.logger.debug("APNS data: expiry = " + note.expiry + "; sound = " + note.sound + "; alert = " + note.alert + "; payload = " + note.payload + "; badge = " + note.badge + "; contentAvailable = " + note.contentAvailable + ";");

		service.pushNotification(note, myDevice);
	} catch (e) {
		global.logger.error(e);
		return cb('error');
	}
	cb(null, 'ok');
};


module.exports = new Push;
