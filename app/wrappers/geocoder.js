var geocoder = require('geocoder');
var util = require("util");

var Geo = {

	getCity: function (lat, lon, cb) {

		geocoder.reverseGeocode(lat, lon, function (err, data) {

			if (err) return cb(err);

			if (data && data.results && data.results.length) {

				var city, country, last, state, county;

				data.results[0].address_components.forEach(function (item) {

					if (item.types.indexOf("locality") !== -1) {
						city = item.long_name;
					}

					if (item.types.indexOf("country") !== -1) {
						country = item.long_name;
					}

					if (item.types.indexOf("administrative_area_level_1") !== -1) {
						state = item.long_name;
					}

					if (item.types.indexOf("administrative_area_level_2") !== -1) {
						county = item.long_name;
					}

					last = item.long_name;

				});

				cb(null, city || county || state || country || last || "Moon");
			} else {
				err = util.format("Place with coordinates %s %s not found", lat, lon);
				global.logger.debug(err);
				cb(null,null); // TODO change to correct exception
			}

		}, {language: 'en'});

	},

	parseRegionName: function (googleAddressComponents) {
		//console.log('parseLandName')
		if (googleAddressComponents) {

			var city, region, country, last;

			googleAddressComponents.forEach(function (item) {

				if (item.types.indexOf("locality") !== -1) {
					city = item.long_name;
				}

				if (item.types.indexOf("sublocality") !== -1) {
					region = item.long_name;
				}

				if (item.types.indexOf("country") !== -1) {
					country = item.long_name;
				}

				last = item.long_name;

			});
			return city || region || country || last || "Moon";

		} else return undefined
	}

};

module.exports = Geo;