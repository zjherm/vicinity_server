/**
 * Prepare swagger json for https://github.com/topliceanu/mongoose-gen format
 * Required schemas replaced by fieldnameId and should be populated in mongoose
 * If you want to use nested objects, define it straight inside parent object.
 * @param model
 */
var generator = require('mongoose-gen');
var swaggerToMongoose = function (model) {

    this.spec = require("../config/swagger.json");
    this.model = model;
};

swaggerToMongoose.prototype.compile = function (){
    var swaggerable = this.buildObj(this.spec.definitions[this.model]);
    return generator.convert(swaggerable);
};

swaggerToMongoose.prototype.buildObj = function (raw) {

    var self = this;

    return Object.keys(raw.properties).reduce(function (prev, field) {
        var item = raw.properties[field];

        if (item.schema && item.schema.$ref) {
            item = self.getRef(item.schema.$ref);
        }
        if (item.type === "array" && item.items) {
            if (item.items.schema) {
                item = [self.getRef(item.items.schema.$ref)];
            }
        }
        if (item.format === "date-time" || item.format === "date") {
            item = {type: "date"};
        }

        if (item._id) delete item._id;

        prev[field] = item;
        return prev;

    }, {});

};

swaggerToMongoose.prototype.getRef = function (ref) {

    var parts = ref.split("/");
    var item = this.spec[parts[1]][parts[2]];
    return this.buildObj(item);

};

module.exports = swaggerToMongoose;
