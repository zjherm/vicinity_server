/**
 *
 * @param items
 * @param params offset, limit
 * @constructor
 */
var Paginate = function (items, params) {

    var offset = params.offset || 0;
    var limit = params.limit|| 10;
    return items.slice(offset, offset*1 + limit*1);

};

module.exports = Paginate;