var request = require('request');
var async = require('async');

var mailchimpURL = global.config.mailchimp.apiURL;

var sendCampaign = function(campaignId, cb) {
	var date = new Date();
	date.setTime( date.getTime() + 259200000 );
	var year = "" + date.getUTCFullYear();
	var month = "" + (date.getUTCMonth() + 1);
	if (month.length < 2) {
		month = "0" + month;
	}
	var day = "" + date.getUTCDate();
	if (day.length < 2) {
		day = "0" + day;
	}
	var hours = "" + date.getUTCHours();
	if (hours.length < 2) {
		hours = "0" + hours;
	}
	var minutes = "" + date.getUTCMinutes();
	if (minutes.length < 2) {
		minutes = "0" + minutes;
	}
	var seconds = "" + date.getUTCSeconds();
	if (seconds.length < 2) {
		seconds = "0" + seconds;
	}

	var dateString = year + "-" + month + "-" + day + " " + hours + ":" + "00" + ":" + "00";

	console.log("Campaing ID: " + campaignId);
	console.log("Scheduled date: " + dateString);
	var urlPath = mailchimpURL + 'campaigns/schedule.json';
	request({
		uri: urlPath,
		method: "POST",
		json: {
			"apikey" : global.config.mailchimp.apikey,
			"cid" : campaignId,
			"schedule_time" : dateString
		}
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Campaign sent!");
		} else if (error) {
			console.log("Error: " + error);
		} else {
			console.log("Response code: " + response.statusCode);
		}
		cb(error);
	});
}

var createCampaign = function(email, cb) {
	var urlPath = mailchimpURL + 'campaigns/create.json';
	request({
		uri: urlPath,
		method: "POST",
		json: {
			"apikey" : global.config.mailchimp.apikey,
			"type" : "regular",
			"options" : {
				"list_id" : global.config.mailchimp.subscribersListId,
				"subject" : "Vicinity",
				"title" : email + " campaign",
				"from_email" : global.config.mailchimp.fromEmail,
				"from_name" : "Vicinity",
				"template_id" : global.config.mailchimp.emailTemplateId,
			},
			"segment_opts" : {
				"match" : "any",
				"conditions" : [{
					"field" : "merge0",
					"op" : "eq",
					"value" : email
				}]
			},
			"content" : {
				"sections" : {

				}
			}
		}
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Campaign created!");
			sendCampaign(body.id, cb);
		} else if (error) {
			console.log("Error: " + error);
			cb(error);
		} else {
			console.log("Response code: " + response.statusCode);
			cb(error);
		}
	});
}

var deleteSentCampaigns = function() {
	var urlPath = mailchimpURL + 'campaigns/list.json';
	request({
		uri: urlPath,
		method: "POST",
		json: {
			"apikey" : global.config.mailchimp.apikey,
			"filters" : {
				"list_id" : global.config.mailchimp.subscribersListId,
				"status" : "sent"
			}
		}
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Sent campaigns list received!");
			for (var i = 0; i < body.data.length; i++) {
				console.log("Delete campaign with title: " + body.data[i].title);
				deleteCampaign(body.data[i].id);
			}
		} else if (error) {
			console.log("Error: " + error);
		} else {
			console.log("Response code: " + response.statusCode);
		}
	});
}

var deleteCampaign = function(campaignId) {
	console.log("Delete campaign with id: " + campaignId);
	var urlPath = mailchimpURL + 'campaigns/delete.json';
	request({
		uri: urlPath,
		method: "POST",
		json: {
			"apikey" : global.config.mailchimp.apikey,
			"cid" : campaignId
		}
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("Campaign deleted!");
		} else if (error) {
			console.log("Delete campaign error: " + error);
		} else {
			console.log("Delete campaign response code: " + response.statusCode);
		}
	});
}

function Mailchimp() {

}

Mailchimp.fn = Mailchimp.prototype;

Mailchimp.fn.scheduleEmail = function(email, cb) {
	// Delete sent campaigns 
	deleteSentCampaigns();

	console.log("apikey: " + global.config.mailchimp.apikey);
	console.log("subscribersListId: " + global.config.mailchimp.subscribersListId);
	// Create and schedule a new campaign
	var urlPath = mailchimpURL + 'lists/subscribe.json';
	request({
		uri: urlPath,
		method: "POST",
		json: {
			"apikey" : global.config.mailchimp.apikey,
			"id" : global.config.mailchimp.subscribersListId,
			"email" : {
				"email" : email
			},
			"double_optin" : false
		}
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log("List subscribed!");
			createCampaign(email, cb);
		} else if (error) {
			console.log("List subscribe error: " + error);
			cb(error);
		} else {
			console.log("List subscribe response code: " + response.statusCode);
			cb(error);
		}
	});
}

module.exports = new Mailchimp;
