var request = require("request");
var util = require("util");
var async = require("async");
var url = require("url");
var _ = require('lodash');
var geocoder = require("./geocoder");
var DEFAULT_RADIUS = 1000;

var allTypes = require('./gplaces_types');

var requestCounter = 0;

var placeCachingQueue = async.queue((task, cb) => {
	var parent = module.parent;
	task.type || (task.type = "")
	task.data || (task.data = null)
	task.data.place_id || (task.data.place_id = "")
	switch (task.type) {
		case 'addFromGoogle':

			if (parent.findOne) {
				var searchModule = parent;
			} else {
				var searchModule = require("../models/place");
			}
			searchModule.findOne({placeId: task.data.place_id}, (err, res) => {
				if (err) return cb(err);
				if (!res) {
					let place = searchModule.fromGoogle(task.data);
					place.save(cb)
				} else {
					cb(null, true);
				}
			})
			break;
		default:
			return cb()
	}
}, 2);

var Gplaces = function (options) {
	this.key = options.apiKey;
	this.options = options;
	this.userIp = options.userIp || global.__requestIp || undefined;
};

// get place info by placeid
// https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyBmc-f8xb6Mhvnpc0uX5O_pfxKBC734pJo&placeid=ChIJrTLr-GyuEmsRBfy61i59si0

// get photo by result.photos[{ photo_reference }]
// https://maps.googleapis.com/maps/api/place/photo?key=AIzaSyBmc-f8xb6Mhvnpc0uX5O_pfxKBC734pJo&maxwidth=800&photoreference=CmRdAAAAcWaF6iG-fvCxTjYptAJF-M1JPXcsbNn4pTgJfLzalnN_jBBKDjsfCTvjjMHaIjdee1ENrNdQCQ7xcXaJHc5ryhTfTz08Xbp2qTzB9m2cXSTkdbUAzFiKjqvUIHaCf0CVEhC_icAgeD1IyO5BsczaIY0OGhR8R1bFT-AJC-EgZ9cP3fN0bp0CYg

// https://maps.googleapis.com/maps/api/place/photo/?key=AIzaSyBmc-f8xb6Mhvnpc0uX5O_pfxKBC734pJo&maxwidth=1000&maxheight=1000&photoreference=CmRdAAAAJNzMsUTkvNjKIfPNFsoymkueZ8lYUrYh74ktLMAPuuZdNJVxby65bAjCwHyOojvP-sF4UbAVZRTMXmBq28TuAxqsYZUtlsWgyQb65hkeOtPcICzLL3dkV0oHX5_bxUOzEhBvUDOz5sgOsWdjCPNv9x-vGhSc2gGI6kbobJSnRsZnsIUsX22KeA

Gplaces.prototype.__uri = function (method, query) {

	query.key = this.key;
	//query.userIp = this.userIp;

	return url.format({
		protocol: "https",
		hostname: "maps.googleapis.com",
		pathname: "/maps/api/place/" + method + "/json",
		query: query
	});
};

Gplaces.prototype.__req = function (uri, cb) {
	requestCounter++;
	console.log('google request # ', requestCounter, uri);
	request({
		method: "get",
		uri: uri,
		json: true
	}, function (err, res, body) {
		global.logger.debug(uri, _.get(body,'status', 'unknown'));

		if (err) return cb(err);

		var result = {
			results: [],
			next_page_token: null
		};

		if (!_.isObject(body)) body = {
			response: res.body
		}


		if (Gplaces.ERROR_STATUSES[body.status]) {
			err = "Google Places response: " + body.status;
		}
		//Gplaces.cachePlaces(body.results, (err,res) => {
		//	if (err) return global.logger.error(err);
		//})

		cb(err, body);
	});
};

Gplaces.cachePlaces = ( places=[], cb= ()=>{} ) => {
	var _tasks = _.map(places, (googleData) => {
		//console.log(googleData)
		return {
			type: 'addFromGoogle',
			data: googleData
		}
	})
	placeCachingQueue.push( _tasks, cb)
};

Gplaces.prototype.__photoUri = function (photoRef, options={}) {
	if (!photoRef) return ""
	var userIp = this.userIp;
	//var maxwidth = options.maxheight || options.maxHeight || 1000;
	//var maxheight = options.maxheight || options.maxHeight || 1000;

	var query = {}
	query.key = this.key;
	query.photoreference = photoRef;
    query.maxwidth = 1000
    query.maxheight = 1000
	//if (userIp) query.userIp = userIp;

	return url.format({
		protocol: "https",
		hostname: "maps.googleapis.com",
		pathname: "/maps/api/place/photo",
		query: query
	});
};

Gplaces.prototype.query = function (lat, lon, text, npt, cb) {
	var opts = {
		location: util.format("%s,%s", lat, lon),
		radius: DEFAULT_RADIUS,
		query: text,
		pagetoken: npt
	};

	if (npt) opts = {pagetoken: npt};

	var uri = this.__uri("textsearch", opts);
	this.__req(uri, function (err, body={}) {
		let results = [];
		if (_.isObject(body)) {
			if (_.get(body, 'results.length',0) > 0) 		results = body.results;
			if (_.get(body, 'predictions.length',0) > 0) 	results = body.predictions;
		}

		cb(err, body.results, body.next_page_token);
	});
};

Gplaces.prototype.near = function ({lat, lon, npt, rank, radius, keyword, name}, cb) {
	var opts = {
		location: util.format("%s,%s", lat, lon),
		types: allTypes,
		rankby: "distance"
	};
	if (rank)     opts.rankby = "prominence"
	if (radius)  opts.radius = radius
	if (name)     opts.name = name
	if (keyword) opts.keyword = keyword

	if (npt) opts = {
		pagetoken: npt
	};

	var uri = this.__uri("nearbysearch", opts);
	this.__req(uri, function (err, body={}) {
		let results = [];
		if (_.isObject(body)) {
			if (_.get(body, 'results.length',0) > 0) 		results = body.results;
			if (_.get(body, 'predictions.length',0) > 0) 	results = body.predictions;
		}

		cb(err, results, body.next_page_token);
	});
};

Gplaces.prototype.getByPlaceId = function (placeId, cb) {
	var self = this;
	var uri = self.__uri("details", {
		placeid: placeId
	});
	var googlePlace;
	async.waterfall([
		self.__req.bind(self, uri),
		function (result, next) {
			googlePlace = result.result;
			if (result.address_components) {
				var landName = geocoder.parseRegionName(result.address_components);
				next(null, landName);
			} else if (googlePlace && googlePlace.geometry && googlePlace.geometry.location) {
				geocoder.getCity(googlePlace.geometry.location.lat, googlePlace.geometry.location.lng, next);
			} else {
				next(null, null)
			}
		},
		function (city, next) {
			if (googlePlace) {
				googlePlace.land = city;
			}
			next(null, googlePlace)
		}
	], cb);
}

Gplaces.prototype.getPhotoArray = function (gPhotos = []) {
	var photoArray = _.map(gPhotos, (photoObj) => {
		return this.__photoUri(photoObj.photo_reference)
	});
	return photoArray
};

Gplaces.ERROR_STATUSES = {
	"INVALID_REQUEST": 1,
	"REQUEST_DENIED": 1,
	"OVER_QUERY_LIMIT": 1
};
Gplaces.ALL_STATUSES = [
	"OK", //  ошибок нет, место обнаружено, и получен хотя бы один результат.
	"UNKNOWN_ERROR", // ошибка на стороне сервера. Повторная попытка может быть успешной.
	"ZERO_RESULTS", // ссылка более не указывает на корректный результат. SDFVSDF SDFV SDFV SDV SDFV SDFV SDFV SDFV SDFV
	"OVER_QUERY_LIMIT", // превышена квота.
	"REQUEST_DENIED", // означает, что запрос отклонен, как правило, из-за отсутствия или неверного значения параметра key.
	"INVALID_REQUEST", // обычно означает, что отсутствует запрос (reference).
	"NOT_FOUND" // указанное место отсутствует в базе данных Google Адресов.
]

module.exports = Gplaces;

var gPlaceExample = {
	"address_components": [
		{
			"long_name": "Sydney",
			"short_name": "Sydney",
			"types": [
				"locality",
				"political"
			]
		},
		{
			"long_name": "New South Wales",
			"short_name": "NSW",
			"types": [
				"administrative_area_level_1",
				"political"
			]
		},
		{
			"long_name": "Австралия",
			"short_name": "AU",
			"types": [
				"country",
				"political"
			]
		},
		{
			"long_name": "2000",
			"short_name": "2000",
			"types": [
				"postal_code"
			]
		}
	],
	"adr_address": "32 The Promenade, <span class=\"street-address\">King Street Wharf 5</span>, <span class=\"locality\">Sydney</span> <span class=\"region\">NSW</span> <span class=\"postal-code\">2000</span>, <span class=\"country-name\">Австралия</span>",
	"formatted_address": "32 The Promenade, King Street Wharf 5, Sydney NSW 2000, Австралия",
	"formatted_phone_number": "(02) 8296 7296",
	"geometry": {
		"location": {
			"lat": -33.867591,
			"lng": 151.201196
		}
	},
	"icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
	"id": "a97f9fb468bcd26b68a23072a55af82d4b325e0d",
	"international_phone_number": "+61 2 8296 7296",
	"name": "Australian Cruise Group",
	"opening_hours": {
		"open_now": false,
		"periods": [
			{
				"close": {
					"day": 0,
					"time": "2200"
				},
				"open": {
					"day": 0,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 1,
					"time": "2200"
				},
				"open": {
					"day": 1,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 2,
					"time": "2200"
				},
				"open": {
					"day": 2,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 3,
					"time": "2200"
				},
				"open": {
					"day": 3,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 4,
					"time": "2200"
				},
				"open": {
					"day": 4,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 5,
					"time": "2200"
				},
				"open": {
					"day": 5,
					"time": "0900"
				}
			},
			{
				"close": {
					"day": 6,
					"time": "2200"
				},
				"open": {
					"day": 6,
					"time": "0900"
				}
			}
		],
		"weekday_text": [
			"Понедельник: 9:00–22:00",
			"Вторник: 9:00–22:00",
			"Среда: 9:00–22:00",
			"Четверг: 9:00–22:00",
			"Пятница: 9:00–22:00",
			"Суббота: 9:00–22:00",
			"Воскресенье: 9:00–22:00"
		]
	},
	"photos": [
		{
			"height": 1331,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAVIcckzOjZH92daCGlZ-8Y9EgJh8EcqXDt1sWXxOWZ-krfYNErqyRlHN1RZpnQd8XUbMsl9nL-jcQt5nho5L5bOtc4Ye8FkwbkNL0L0B7XMy9WSy5KYCMg_SRWfncytVlEhC_VamkLNnFpcClKtSYmd6DGhTiPrllBE3Fix_g838fg1fnfu0jOQ",
			"width": 2000
		},
		{
			"height": 1320,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAd-5H6BTw7khosmHxJ5qxX6PeGGPJmzChughEYaz77x3_POcr8W8jfk1YmOUQcel7m9_zZo02ZRWyQitDuxFA7HFiBnlGSXJXHZS5VLgYtsn3vfWTvWv3-ImZcToqDIXlEhCD8wiY2AIvSSu826GzAr9lGhS1a5kr1sM-pdUD0U65C-2WmOzJBg",
			"width": 2048
		},
		{
			"height": 417,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAY_UDZQd38ABbPIK8FM2CogyP9zrTfxhVxorNh2MYWG7fSEeNXeRZMERpfdPey4m3N-KcGCPWvw-Eu9RwRN20_WrSry76GtY93cSEO--0iP9dOLNXEuAA0H2M8eLYmxQWEhDph0aaeEKLnIouni4ZfANwGhQPYDhc3Dv4_f3lJXoCAVnrWXloew",
			"width": 1334
		},
		{
			"height": 1331,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAlLEFGRc_beUxLDrKq61ENLAi32N4Mqam-M24im8LiTEtgl-SHLM2RXG3p7oRKCp62Gh7IwMJWB8p8KxkGLLMcjoXLATbkKKQn6_GASAlehJu06Yb6mi8uFwTIuDd7hhxEhAa3g_0Eb3oSvcDW18ao0iIGhSRrNUVpWTg0luVbTKd67BsKb86VA",
			"width": 2000
		},
		{
			"height": 1607,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAmcmGQr7NF0LYH4wLOTwTnTy5quNdnyer3ZzksrKruv-JDL1HmUklNUZukX13vijAkY6eBtQBM_V66CUWZaPd5P3p_epx70-ykxODUu5qvTCEC3bAVaRo7T4QTxwjlGUvEhAAdSaMf0dHtFyOk6rqSIyhGhQnDYG9_NyZk7eAjHFB0vaFiEdJrQ",
			"width": 1969
		},
		{
			"height": 300,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAA_enYESN5UD6qCp-SyW4IIg98IWP-au7dtlmqHHxS-7ORPqEuvDaYh_aJk2BJOfdkSeihlWX0kUjk-Y1m_I7U34s5PolD0OM-ta2Q73c1PDc38qxrfmXdyE2or6RhZWDhEhCw8LLCU6i2hfkCrmLZNVXWGhRM1za2Igtu_PgwqVr_Fy4PIWuh1g",
			"width": 300
		},
		{
			"height": 328,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAGw__kW9DZ9uT8GpYbUWJzQBFQmv2HYPW9w-xz8ynhVksncwuIS3ReZFr5s3YNw4r7U9IBC8x-e492IYTFV6WaZCvqK_khn4riVKFcyCJNsWcv8WwAbGnBUT4a-Lk4GTgEhCuZ7jkpOfctHW2wLrO8sZ-GhSrYoyqCeoTkXBFgKiKdfjCrnIm7g",
			"width": 329
		},
		{
			"height": 426,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAMAHNI0SIURwhjvHN0BPng3LA0FvRYVotcXH-TU_FL7RRtHUhOhbuh5sPENac3RqbIVvnsCvVJYohmXGnG7PusOZpmd9XuiYbJNWIijScEPGuz0c__H_5rEY-pLG8cKNSEhBDUfwuU0M5SP_N32i647HjGhRDIzKc46OmqvUY-6x2A_IocV_FRg",
			"width": 640
		},
		{
			"height": 250,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAApvOqc1hpPv_3S6GSLzZR9sMhzbWmeosiOuw0N690Y2jOYOI3C0WBtxBnpmXnLQbLuSv-6yVJYY6OgFvNjD7GFLi-4uaa2b0-oh7Vdu-e-y-HN2e_Fk41GBv3iTpukSE5EhDCAc-R0M6JP6rOwA-uPeolGhQ03mZ1r7XiR166fFbd3vRxkUedPw",
			"width": 379
		},
		{
			"height": 1119,
			"html_attributions": [
				"<a href=\"https://maps.google.com/maps/contrib/110751364053842618118\">Australian Cruise Group</a>"
			],
			"photo_reference": "CmRdAAAAzyQoPMP944Su2uG8L-mT8EMX-5KkzdgRiGmh5rU4WnysugaG2EcnZkJSmECqamECNVI2McOz4xh6MEyIgABmmNR9XjkfMFdhm2EXp6Yoqgw7obyDgFEEIOblDmvAAo6OEhBaOn0IeG7omq--UzfkVFz1GhQyV_1UNxiozs1rw2DxI8xT7aEXIA",
			"width": 2000
		}
	],
	"place_id": "ChIJrTLr-GyuEmsRBfy61i59si0",
	"reference": "CnRqAAAAIFNaVZVrOhSphgZ1OC2FfD1TWQHAmFOX4JJvoj2dmXcL2DAc3b1cwgKq_XKOt9Jn8d91DuDV1ihDwQTa3wnnUeCFaTeGjwJ0e956U4GF3hM2kIoEHkzdVc73V_V_NQMm6R1RBNtArtSgxPeo2HGS2RIQwouMMrJKacnOpPbYNRDPHBoUmyDE5eAN9zbanLvW3oF62ziZfUg",
	"scope": "GOOGLE",
	"types": [
		"restaurant",
		"travel_agency",
		"food",
		"point_of_interest",
		"establishment"
	],
	"url": "https://plus.google.com/110751364053842618118/about?hl=ru-RU",
	"utc_offset": 600,
	"vicinity": "32 The Promenade, King Street Wharf 5, Sydney",
	"website": "http://www.australiancruisegroup.com.au/"
};
var examples = `
https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=cruise&key=AIzaSyB5EsBwTfG-lnqRwqTHZVvLmfoXZuWpFas

https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=44,44&name=cruise&rankby=distance&radius=500&types=food&key=AIzaSyB5EsBwTfG-lnqRwqTHZVvLmfoXZuWpFas

https://maps.googleapis.com/maps/api/place/autocomplete/json?location=59.9342802,30.3350986&radius=2000&input=Vict&key=AIzaSyB5EsBwTfG-lnqRwqTHZVvLmfoXZuWpFas

`;
