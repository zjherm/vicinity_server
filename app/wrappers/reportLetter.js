var util = require("util");

var Letter = function (options) {
    this.subject = this.getSubject(options.reportType);

    this.text = util.format("Hey Vicinity!\n\n" +
        "%s\n\n" +
        "---------------------------------------\n" +
        "Send from:\n" +
        "%s \n" +
        "email: %s\n" +
        "User id: %s\n" +
        "Device: %s %s\n" +
        "Application: %s", options.text, options.user.fullname, options.user.email, options.user.userId, options.deviceType, options.os, "Vicinity " + options.version);
};


Letter.prototype.getSubject = function (reportType) {
    var type = "general";
    if (reportType === "request") type = "Request";
    if (reportType === "bugReport") type = "Bug Report";
    if (reportType === "general") type = "General";

    return "Vicinity App Feedback – " + type;

};

module.exports = Letter;