
var host = process.env.HOST;
var port = 3007;
var proxyPort = 3002;

module.exports = {

	"host": host,
	"port": port,
	"proxyPort": proxyPort,
	"mongodb": "mongodb://localhost:47017/vicinity",
	"showSwagger": true,
	"swaggerOrigin": `${host}:${port}`,
	"frontEnd": {
		protocol: 'http',
		host: `${host}:9002`,
		emailConfirmationPath: '/email-confirm',
		restorePasswordPath: '/restore-password'
	},
	"mail": {
		"reportEmails": [
			"info@getvicinityapp.com"
		],
		"gmail": {
			"user": "hello.body.bot@gmail.com",
			"pass": "tobydobolleh"
		}
	},
	"apn": {
		"pfx": "APNS_VicinityDev_production.p12",
		"production": true,
		"passphrase": ""
	},
	"mailchimp": {
		"apikey": "77191942abb8e5eec6a138496bef4ce0-us12",
		"subscribersListId": "b1a997be6a",
		"emailTemplateId": "77625",
		"fromEmail": "evgen.ded@gmail.com",
		"apiURL": "https://us12.api.mailchimp.com/2.0/"
	},
	"images": {
		"host": "0.0.0.0",
		"port": 8595,
		"baseUrl": `http://${host}:${proxyPort}`,
		"quality": 95,
		"mimeTypes": [
			"image/jpeg",
			"image/pjpeg",
			"image/png",
			"image/gif",
			"image/jpg"
		],
		"storagePath": "/var/www/vicinity/srv_dev/public/uploads",
		"urlPath": "/uploads/",
		"maxImageSize": {
			"height": 1000,
			"width": 1000
		},
		"maxThumbnailSize": {
			"height": 400,
			"width": 400
		},
		"maxFileSize": 10000000
	}
}
