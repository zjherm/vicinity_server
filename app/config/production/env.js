
var host = process.env.HOST;
var port = 3005;
var proxyPort = 80;

module.exports = {

	"host": host,
	"port": port,
	"proxyPort": proxyPort,
	"mongodb": "mongodb://localhost:27017/vicinity",
	"showSwagger": true,
	"swaggerOrigin": `${host}:${proxyPort}`,
	"frontEnd": {
		protocol: 'http',
		host: `${host}:9000`,
		emailConfirmationPath: '/email-confirm',
		restorePasswordPath: '/restore-password'
	},
	"mail": {
		"urlHost": "vicinity.getvicinityapp.com",
		"urlPort": 80,
		"reportEmails": [
			"info@getvicinityapp.com"
		],
		"gmail": {
			"user": "info@getvicinityapp.com",
			"pass": "zande!2015"
		}
	},
	"apn": {
		"pfx": "APNS_VicinityApp_production.p12",
		"production": true,
		"passphrase": ""
	},
	"mailchimp": {
		"apikey": "9246795b0e52f5dbae87cb0bed474601-us10",
		"subscribersListId": "86a32e5129",
		"emailTemplateId": "154549",
		"fromEmail": "info@getvicinityapp.com",
		"apiURL": "https://us10.api.mailchimp.com/2.0/"
	},
	"images": {
		"host": "0.0.0.0",
		"port": 8595,
		"baseUrl": `http://${host}:${proxyPort}`,
		"quality": 95,
		"mimeTypes": [
			"image/jpeg",
			"image/pjpeg",
			"image/png",
			"image/gif",
			"image/jpg"
		],
		"storagePath": "/var/www/vicinity_server/public/uploads",
		"urlPath": "/uploads/",
		"maxImageSize": {
			"height": 1000,
			"width": 1000
		},
		"maxThumbnailSize": {
			"height": 400,
			"width": 400
		},
		"maxFileSize": 50000000
	}
}