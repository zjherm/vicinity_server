var os = require("os");
var fs = require("fs");
var _ = require("underscore");

var env = { dev: "development", stage: "staging", prod: "production" };

var envNameCmd = !process.env.ENV ? "" : process.env.ENV.toLowerCase();
if (!envNameCmd || !env.hasOwnProperty(envNameCmd)) {
	console.log("Environment variable ENV:dev|stage|prod is requied to run process.");
	process.exit();
}

if (!process.env.HOST) {
	console.log("Environment variable HOST is requied to run process.");
	process.exit();
}
else {
	console.log('HOST=', process.env.HOST);
}

let ROOT_PATH = __dirname + "/../../";
let ENV_NAME = env[envNameCmd];
let envConfig = null;

try {
	envConfig = require(`./${ENV_NAME}/env`);
	console.log('ENV=', ENV_NAME);
}
catch (e) {
	console.log("Can't load '" + ENV_NAME + "' environment configuration. Check app/config/" + ENV_NAME + " folder exists.");
	process.exit();
}

if (!envConfig || !envConfig.apn) {
	console.log("Create app/config/{development|staging|production}.js file with apn section");
	process.exit();
}

let baseConfig =
{
	"host": "localhost",
	"port": 3007,
	"showSwagger": true,
	"swaggerOrigin": "localhost:3007", // deprecated - cofigure for each site (see AppSwaggerUI)
	"swaggerJSON": __dirname + "/swagger.json", // deprecated - cofigure for each site (see AppSwaggerUI)
	"swaggerPath":  ROOT_PATH + "/public/swagger/dist", // swagger UI full path
	"swaggerStart": "index.html", // swagger UI start page
	"frontEnd": {
		protocol: 'http',
		host: 'localhost:9000',
		emailConfirmationPath: '/email-confirm',
		restorePasswordPath: '/restore-password'
	},
	"root": ROOT_PATH,
	"cookieMaxAge": 24*60*60*1000, // 24 hours in ms
	"sessionTTLMinutes": 2 * 60, // 2 hours in min
	"sessionTimeoutMin": 2 * 24 * 60, // 48 hours in min
	"env": ENV_NAME,
	"mongodb": 'mongodb://localhost/vicinity',
	"apnSertDir": __dirname + "/../../cert/",
	"images": {
		"host": "0.0.0.0",
		"port": 8595,
		"baseUrl": "http://dev.yarax.ru",
		"quality": 95,
		"mimeTypes": ["image/jpeg", "image/pjpeg", "image/png", "image/gif", "image/jpg"],
		"storagePath": __dirname + "../../../public/uploads",
		"urlPath": "/uploads/",
		"maxImageSize": {"height": 1000, "width": 1000},
		"maxThumbnailSize": {"height": 400, "width": 400},
		"maxFileSize": 10000000
	},
	"googlePlaces": {
		"apiKey": "AIzaSyB5EsBwTfG-lnqRwqTHZVvLmfoXZuWpFas",
		"apiProductionKey": "AIzaSyA8CaLEl3rA2tOct49_mLuyqPnHcXPcfmE",
		"clientId": "970304476418-uob2siol7t679jeap8ujrfhn9v3er9k6.apps.googleusercontent.com",
		"clientSecret": "3LY6wE1mBlzdPP108tK_ZwcB"
	},
	"fb": {
		"appId": 1627242810825310,
		"appSecret": "f15d43774bb795ed3570d4f2e6a67073"
	},
	"stripe": {
		"apiKey": "sk_live_ZYslZnWL7GFkICR1TMtKUIuR",
		"pub": "pk_live_lUfEhq2GzmsA4vQ1Wz9hfqB3"
	},
	"mail": {
		"reportEmails": [
			"info@getvicinityapp.com"
		],
		"gmail": {
			"user": "hello.body.bot@gmail.com",
			"pass": "tobydobolleh"
		}
	}
};

let resultConfig = _.extend(baseConfig, envConfig);

let cfg = _.extend(baseConfig, envConfig);
cfg.apn.pfx = cfg.apnSertDir + cfg.apn.pfx;

module.exports = resultConfig;
