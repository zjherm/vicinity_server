var util = require('util');
var url = require("url");
var fs = require("fs");
var Validator = require('jsonschema').Validator;
var STATUS = require('./response-statuses');
var _ = require('lodash');

/**
 * Class for routing the coming request. Includes handling controllers and authorization
 * Supports only first security object
 * @param {Object} config - path to swagger.json
 * @param {Object} controller - {method1: function () {}, method2 : ...}
 * @param {Object} authorization - {session: function (req, res, next) {}, oauth: ...}
 * @constructor
 */
var Swagger = function (config, controller, authorization) {
    if (!config) throw new Error("No config given");
    this.config = JSON.parse(fs.readFileSync(config));
    this.controller = controller || {};
    this.authorization = authorization || {};
    this.env = process.env.NODE_ENV || (global.config ? global.config.env : null) || "development";

    this.validator = new Validator();
    var defs = Object.keys(this.config.definitions);
    var self = this;
    defs.forEach(function (def) {
        var item = self.config.definitions;
        item[def].id = "#/definitions/" + def;
        //console.log(item[def], item[def].id);
        self.validator.addSchema(item[def], item[def].id);
    });

};

/**
 * Searching for matches with /{param}
 * @param url
 * @param req
 */

Swagger.prototype.findWithParams = function (url, req) {
    var foundPath;
    var self = this;
    req.params = req.params || {};
    var result = Object.keys(this.config.paths).some(function (item) {
        // заменяем swagger-like параметры на regexp подмаски
        var urlRe = new RegExp(item.replace(/\{(.*?)\}/g, "([^\/]+)") + "$");
        // вытаскиваем имена swagger-like параметров
        var pathRe = new RegExp(/\{([a-z0-9\.\-]+)\}/i);
        foundPath = self.config.paths[item];
        global.logger.debug(url, urlRe, item, pathRe);
        var matchUrl = url.match(urlRe);
        var matchPath = item.match(pathRe);
        if (matchUrl) {
            // заполняем express словарь параметров
            for (var i = 1; i < matchUrl.length; i++) {
                req.params[matchPath[i]] = matchUrl[i];
            }
        }
        return matchUrl;
    });
    return result ? foundPath : false;
};

/**
 * Find item by given request
 * @param req
 * @returns {*}
 */
Swagger.prototype.findItem = function (req) {
    req.baseUrl = req.baseUrl || url.parse(req.originalUrl).pathname;
    var swagUrl = req.baseUrl.replace(this.config.basePath, "");
    var path = this.config.paths[swagUrl];
    if (!path) path = this.findWithParams(swagUrl, req);
    if (path) {
        var item = path[req.method.toLowerCase()];
        if (item) {
            return item;
        } else {
            return null;
        }
    } else {
        return null;
    }
};

/**
 * Checks required parameters in query and body according to swagger parameter object
 * Without checking data type
 * @param {Object} req
 * @param {Object} item
 */
Swagger.prototype.requiredParams = function (req, item) {

    var query = req.query || {};
    var body = req.body || {};
    var field = '';

    var err = item.parameters.some(function (param) {
        field = param.name;
        if (param.schema) return false;
        if (param.in === "query" && !query[param.name] && param.required) return true;
        if (param.in === "body" && !body[param.name] && param.required) return true;
    });

    if (err) {
        return {
            code: "BAD_INPUT_PARAMS",
            message: "No required param " + field + " " + req.url
        }
    }

};

/**
 * @TODO all Schema validation
 * Validate params, e.g max-min length
 * @param req
 * @param item
 */
Swagger.prototype.checkParams = function (req, item) {
    var query = req.query || {};
    var body = req.body || {};
    var err;

    var res = item.parameters.some(function (param) {
        var key = param.name;
        var value = query[key] || body[key];
        if (param.minLength !== undefined && value && value.length <= param.minLength) {
            err = key + " is too short";
            return true;
        }
        if (param.maxLength !== undefined && value && value.length >= param.maxLength) {
            err = key + " is too long";
            return true;
        }
    });

    if (res && err) {
        return {
            code: "BAD_INPUT_PARAMS",
            message: err
        }
    }
};
/**
 *
 * @param param
 * @param cont
 * @param req
 * https://github.com/swagger-api/swagger-spec/blob/master/versions/2.0.md#parameter-object
 */
Swagger.prototype.checkExistingParam = function (param, cont, req) {
    switch (cont) {
        case "path":
            return true;
        case "query":
            return req.query && req.query[param];
        case "header":
            return req.get(param);
        case "body":
            return req.body && req.body[param];
        case "formData":
            return req.body && req.body[param];
        default:
            return false;
    }
};

/**
 * Validation and authorization
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
Swagger.prototype.validate = function (req, res, next) {
    //console.log('=============== REQUEST VALIDATE LOGGING START ===============');
    //console.log('req.body:');
    //console.log(req.body);
    //console.log('req.params:');
    //console.log(req.params);
    //console.log('req.query:');
    //console.log(req.query);
    //console.log('req.files:');
    //console.log(req.files);
    //console.log('=============== REQUEST VALIDATE LOGGING END ===============');
    var item = this.findItem(req);
    var self = this;
    var error;
    if (!item) {
        next({
            code: "404",
            message: "Not found: " + req.url
        });
    } else {

        var allRequired = item.parameters.every(function (param) {
            if (param.schema) {
                return true;
            }
            if (param.required) {
                var resCheck = self.checkExistingParam(param.name, param.in, req);
                console.log(param.name, param.in, req[param.in], resCheck, req.body);
                return resCheck;
            }
            return true;
        });

        item.consumes = item.consumes || [];
        // Have no idea how to deal with multipart/form-data, just ignore it
        if (!allRequired && item.consumes.indexOf("multipart/form-data") === -1) error = {
            code: "BAD_INPUT_PARAMS",
            message: "Not all required params"

        };

        //@TODO !! var error = this.requiredParams(req, item) || this.checkParams(req, item);
        if (error) {
            next(error);
            return;
        }

        if (item.security) {
            var secName = Object.keys(item.security[0])[0];
            if (!this.authorization[secName]) {
                next({
                    code: "UNKNOWN_AUTH",
                    message: "Unknown auth method"
                });
            } else {
                this.authorization[secName](req, res, next);
            }
        } else {
            next();
        }
    }

};

/**
 * Handle controllers
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
Swagger.prototype.handle = function (req, res, next) {
    if (global.config.env !== "production") {
        console.log('=============== REQUEST LOGGING START ===============');
        console.log('req.body:');
        console.log(req.body);
        console.log('req.params:');
        console.log(req.params);
        console.log('req.query:');
        console.log(req.query);
        console.log('=============== REQUEST LOGGING END ===============');
    }

    var item = this.findItem(req);
    if (!item) throw new Error("Wasn't used validator");
    if (!this.controller[item.operationId]) {
        global.logger.error("No controller method " + item.operationId + " in given object", this.controller);
        //throw new Error();
    }
    this.controller[item.operationId](req, res, next);

};

/**
 * Catch errors from controller. Can be rewrite for customization
 * @param {Object} err
 * @param {Object} req
 * @param {Object} res
 */
Swagger.prototype.output = function (err, req, res, next) {
    if (err) {
        global.logger.warn(global.config.env, err);
        var _err = {
            code: err.code || STATUS.serverProblems.code,
            message: err.message || util.format(err)
        }
        var _data = err.code == STATUS.userLocked.code ? req.result : null;
        if (global.config.env !== "production") {
            if (err.errors) _err.errors = err.errors;
            if (err.stack) _err.stack = err.stack;
        }
        res.json({
            status: "error",
            data: _data,
            error: _err
        });
    } else {
        res.json({
            status: "success",
            data: req.result,
            error: null
        });
    }
};

//var STATUS = {
//  "BAD_SESSION" : 1,
//  "FACEBOOK_ERROR" : 1,
//  "BAD_INPUT_PARAMS" : 1,
//  "UNKNOWN_AUTH" : 1,
//  "NICKNAME_ALREADY_EXISTS" : 1,
//  "SERVER_PROBLEMS" : 1,
//  "DUPLICATE_ITEMS" : 1,
//  "ITEM_NOT_FOUND" : 1,
//  "404" : 1,
//  "BAD_COMMAND" : 1,
//  "WRONG_ROOM" : 1,
//  "NOT_JOINED" : 1, // ws
//  "PROTOCOL_ERROR" : 1 // ws
//};

module.exports = Swagger;
