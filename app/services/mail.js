var sendgrid = require('sendgrid')('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');
var config = require('../config');

var Mailer = function (options) {
	var recipients = [];
	for (var emailIdx = 0; emailIdx < options.emails.length; emailIdx++) {
		recipients.push({
			email: options.emails[emailIdx]
		});
	}

	this.request = sendgrid.emptyRequest({
      method: 'POST',
      path: '/v3/mail/send',
      body: {
        personalizations: [
          {
            to: recipients,
            subject: options.letter.subject,
          },
        ],
        from: {
          email: options.from || 'info@getvicinityapp.com',
        },
        content: [
          {
            type: 'text/plain',
            value: options.letter.text,
          },
        ],
      },
    });
};

Mailer.fn = Mailer.prototype;

Mailer.fn.send = function (cb) {
	sendgrid.API(this.request, cb);
};

module.exports = Mailer;