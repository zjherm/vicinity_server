/**
 * Created by Roman Bespalov on 11/22/15.
 */
var _ = require("underscore");

var ODataUtils = function() { };

/**
 * Creates JSON responce object for OData service consumer
 * @param value Result collection to return
 * @param count Items count to attach to result
 * @returns {{[odata.metadata]: string, value: (Object|Array)}}
 */
ODataUtils.createResult = function(value, count) {

    var options = (count != -1) ? {"odata.count": count} : null;
    var  data = {
        "odata.metadata" : "http://localhost:9000/admin/api/$metadata",
        "value" : value
    };

    if(options)
        data = _.extend(data, options);

    return data;
};

// filterField(filterValue) || filterField(filterValue)/$function || $function
ODataUtils.parseGETUriParams = function(filterParam, operation) {
    var result = { filter : null, func : null, field: null };
    if(!filterParam && !operation)
        return result;

    var getFunc = function(str){
        var funcExpr = /\$(\w+)/g.exec(str);
        return (funcExpr) ? funcExpr[1] : null;
    };

    if(operation) { // function or single field
        result.func = getFunc(operation); // function test
        if(!result.func) {
            var filedExpr = /(\w+)/g.exec(operation); // single field test
            if (filedExpr)
                result.field = filedExpr[1];
        }
    }

    if(filterParam) { // filter or function
        var filterExpr = /(\S+)\((\S+)\)/g.exec(filterParam); // filter test = "field(value)"
        if (filterExpr) {
            let fieldName = filterExpr[1];
            let fieldValue = filterExpr[2];
            result.filter = {};
            result.filter[fieldName] = fieldValue;
        }
        else if(!operation) { // no filter, maybe function?
            result.func = getFunc(filterParam);
        }
    }

    return result;
};

ODataUtils._getWord = function(str) {
    if (!str) return null;
    let expr = /(\w+)/g.exec(str);
    return (expr != null) ? expr[1] : null;
};

ODataUtils.parseDELETEUriParams = function(id, instance) {
    var result = {};
    result["id"] = ODataUtils._getWord(id);
    result["instance"] = ODataUtils._getWord(instance);
    return result;
};

ODataUtils.parsePOSTUriParams = function(id, operation) {
    var result = {};
    result["id"] = ODataUtils._getWord(id);
    result["operation"] = ODataUtils._getWord(operation);
    return result;
};

ODataUtils.parsePATCHUriParams = function(id, instance) {
    var result = {};
    result["id"] = ODataUtils._getWord(id);
    result["instance"] = ODataUtils._getWord(instance);
    return result;
};

ODataUtils.attachQueryParams = function(query, req, options) {
    // SELECT
    var selectFields = options.selectFields;
    var defaultOrder = options.defaultOrder;

    // single value result support
    var selectOptions = selectFields;
    if(options.isSingleFieldResult) {
        selectOptions = {"_id": 0};
        selectOptions[selectFields] = 1;
    }

    if(selectFields)
        query.select(selectOptions);

    // ORDER BY
    if (req.query["$orderby"])
        query.sort(ODataUtils._createOrderByStatement(req.query.$orderby));
    else if(defaultOrder) // apply default sort order
        query.sort(defaultOrder);

    // SKIP / TOP
    if (req.query["$skip"])
        query.skip(req.query["$skip"]);
    if (req.query["$top"])
        query.limit(req.query["$top"]);

    return query;
};

ODataUtils.execCount = function(req, model, where, callback) {
    if (req.query["$count"] === "true")
        model.count(where, callback);
    else
        callback(null, -1);
};

// ?$filter=stringToFind
ODataUtils.createWhereStatement = function(req, filterFields) {
    var result = null;
    var filterValue = req.query["$filter"];

    var hasQueryFilter = (filterValue && filterFields && filterFields.length); // serach phrase & search fields is available
    if (hasQueryFilter) {
        result = {"$or": [ ]};  // { $or : [{fullname: filterExp}, {nickname: filterExp}, {email: filterExp}] }
        let filterExp = new RegExp(filterValue, 'gi'); // filter value - the same for all fields

        filterFields.forEach((item) => { // combine fields and values
            var queryItem = {};
            queryItem[item] = filterExp;
            result.$or.push(queryItem);
        });
    }

    return result;
};

ODataUtils._createOrderByStatement = function(orderbyStr) {
    var arr = orderbyStr.split(' ');
    var expr = {};
    expr[arr[0]] = (arr.length > 1 && arr[1] == "desc") ? -1 : 1;
    return expr;
};

module.exports = ODataUtils;