var mongoStopWords = require("../config/mongoStopWords.json");

module.exports = {

    autoReplace : function (str) {
        if (!str) return str;
        return str.replace(/[\w\']+/g, function (word) {
            if (mongoStopWords[word]) {
                return "__" + word + "__";
            } else {
                return word;
            }
        });

    },

    normalize : function (str) {
        if (!str) return str;

        return str.replace(/__([\w\']+)__/g, function (_, str) {
            return str;
        });

    },

    getHashtag : function (tag) {
        if (mongoStopWords[tag]) {
            return "#__" + tag + "__";
        } else {
            return "#" + tag;
        }
    }

};