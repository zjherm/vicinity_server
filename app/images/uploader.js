// Welcome to world of holy shit

var util = require('util');
var events = require('events');
var _ = require('underscore');
var fs = require('fs');

var Busboy = require('busboy');
var temp = require('temp').track();

var DEFAULT_MAX_SIZE = 20000000;

function FileUpload(req, options) {
    var self = this;
    options = options || {};
    options.mimeTypes = options.mimeTypes || [];
    options.maxFileSize = options.maxFileSize || DEFAULT_MAX_SIZE;

    this.req = req;
    this.options = options;
}
util.inherits(FileUpload, events.EventEmitter);

FileUpload.fn = FileUpload.prototype;

FileUpload.fn.start = function () {
    var self = this;

    var options = self.options;
    var fileCount = 0;
    var wasFile = false;

    var sendResult = function () {
        self.emit.apply(self, arguments);
    };

    var busboy = new Busboy({ headers: self.req.headers });
    var err = null;

    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
        var data;
        try {
            data = JSON.parse(val);
            if (typeof data !== "object") {
                data = {};
                data[fieldname] = val;
            }
        } catch (e) {
            data = {};
            data[fieldname] = val;
        }
        self.req.body = _.extend(self.req.body, data);
    });

    busboy.on('finish', function () {
        sendResult("finish", wasFile);
    });

    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        wasFile = true;
        global.logger.debug("busboy file");
        ++fileCount;
        if (fileCount > 1) {
            sendResult('error', 'TOO_MANY_FILES');
            return;
        }
        global.logger.debug("RAX", options.mimeTypes, mimetype);
        var mimeIsValid = _.contains(options.mimeTypes, mimetype);
        if (!mimeIsValid) {
            sendResult('error', 'BAD_MIME_TYPE:' + mimetype);
            return;
        }

        //
        var bytesRead = 0;
        var tmp = temp.createWriteStream();
        file.on('data', function (data) {
            global.logger.debug("busboy file data");
            bytesRead += data.length;
            if (bytesRead > options.maxFileSize) {
                //sendResult('error', 'FILE_IS_TOO_BIG');
                // remove temp file from filesystem
                err = "FILE_IS_TO_BIG";
                return;
            }
            tmp.write(data);
        });
        file.on('end', function () {
            global.logger.debug("busboy file end", err);

            if (err) {
                tmp.end(function () {
                    fs.unlink(tmp.path);
                    sendResult('error', 'FILE_IS_TOO_BIG');
                });
            } else {
                tmp.end(function () {
                    sendResult('file', tmp.path);
                });
            }
        });
    });
    self.req.pipe(busboy);
};

module.exports = FileUpload;
