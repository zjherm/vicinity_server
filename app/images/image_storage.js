var buffer = require('buffer');
var path = require('path');
var mkdirp = require('mkdirp');
var crypto = require('crypto');
var async = require('async');
var gm = require('gm');
var fs = require("fs");
var copyFile2 = require("node-cp");


function ImageStorage(options) {
    this.config = options;
};

ImageStorage.fn = ImageStorage.prototype;

ImageStorage.fn.store = function (fileName, cb) {
    var self = this;
    var rand = (new Buffer(crypto.randomBytes(8), 'bin')).toString('hex');
    var outDir = path.join(this.config.storagePath, rand.substr(0, 2), rand.substr(2, 2));
    var format = null;
    var size = null;

    global.logger.debug(fileName, ' -> ', outDir);

    // check image and get it's size
    function getimageFormatAndSize(cb) {

        async.parallel([
                function (cb) {
                    gm(fileName).format(function (err, result) {
                        if (err) {
                            cb({error: {code: "BAD_IMAGE_FORMAT"}});
                            return;
                        }
                        format = result;
                        cb(null);
                    });
                },
                function (cb) {
                    gm(fileName).size(function (err, result) {
                        if (err) {
                            cb({error: {code: "BAD_IMAGE_SIZE"}});
                            return;
                        }
                        size = result;
                        cb(null);
                    });
                }
            ],
            function (err) {
                global.logger.debug('FORMAT=', format, 'SIZE=', size);
                cb(err);
            }
        );
    }

    // create output directory
    function createOutputDirectory(cb) {
        mkdirp(outDir, cb);
    }

    function copyFile(source, target, cb) {
        var cbCalled = false;

        var rd = fs.createReadStream(source);
        rd.on("error", function(err) {
            done(err);
        });
        var wr = fs.createWriteStream(target);
        wr.on("error", function(err) {
            done(err);
        });
        wr.on("close", function(ex) {
            done();
        });
        rd.pipe(wr);

        function done(err) {
            if (!cbCalled) {
                cb(err);
                cbCalled = true;
            }
        }
    }

    function justCopy (cb) {
        var name = path.join(outDir, rand.substr(4) + getExtensionByFormat(format));
        var url = self.config.urlPath + rand.substr(0, 2) + '/' + rand.substr(2, 2) + '/' +
            rand.substr(4) + getExtensionByFormat(format);
        global.logger.debug("just copy", fileName, name);
        copyFile(fileName, name, function (err) {
            global.logger.debug("Copied ", arguments);
            cb(err, url);
        });
    }

    // create resized images
    function resizeImage(cb) {
        var maxSize = self.config.maxImageSize;
        var name = path.join(outDir, rand.substr(4) + getExtensionByFormat(format));
        var url = self.config.urlPath + rand.substr(0, 2) + '/' + rand.substr(2, 2) + '/' +
            rand.substr(4) + getExtensionByFormat(format);
        var img = gm(fileName).quality(95);
        if (size.height > maxSize.height || size.width > maxSize.width) {
            img = img.resize(maxSize.width, maxSize.height + '>').gravity('Center');
        }
        img.write(name, function (err) {
            cb(err, url);
        });
    }

    // thumbnail
    function resizeThumbnail(cb) {
        var maxSize = self.config.maxThumbnailSize;
        var name = path.join(outDir, rand.substr(4) + '-thumbnail' + getExtensionByFormat(format));
        var url = self.config.urlPath + rand.substr(0, 2) + '/' + rand.substr(2, 2) + '/' +
            rand.substr(4) + '-thumbnail' + getExtensionByFormat(format);
        var img = gm(fileName).quality(95);
        if (size.height > maxSize.height || size.width > maxSize.width) {
            img = img.resize(maxSize.width, maxSize.height + '>').gravity('Center');
        }
        img.write(name, function (err) {
            cb(err, url);
        });
    }
    global.logger.debug("series");
    async.series([
        getimageFormatAndSize,
        createOutputDirectory,
        resizeImage,
        //resizeThumbnail
        justCopy
    ], function (err, res) {
        if (err) {
            global.logger.err('image store result', err, res);
            cb(err);
            return;
        }
        global.logger.debug('image store result', err, res);
        cb(null, self.config.baseUrl + res[2]);
    });
};

function getExtensionByFormat(format) {
    switch (format) {
        case 'JPEG':
            return '.jpg';
        case 'PNG':
            return '.png';
        case 'GIF':
            return '.gif';
        default:
            return '.bin';
    }
}


module.exports = ImageStorage;

