var url = require('url');
var crypto = require('crypto');
var url = require("url");
var async = require("async");
var _ = require('lodash');
var geolib = require("geolib");
var mailchimp = require("./wrappers/mailchimp");

var config = require('./config')
var STATUS = require('./response-statuses')

var Auth = require("./auth");

var User = require("./models/user");
var Company = require("./models/company");
var Place = require("./models/place");
var Post = require("./models/post");
var Deal = require("./models/deal");
var Notification = require("./models/notification");
var Report = require("./models/report");
var Comment = require("./models/comment");
var Session = require("./models/session");
var EmailConfirmationToken = require("./models/_tokens-email-confirm");
var RestorePasswordToken = require("./models/_tokens-password-restore");

var paginate = require("./wrappers/paginate");
var Gplaces = require("./wrappers/gplaces");
var Facebook = require("./wrappers/facebook");
var geocoder = require("./wrappers/geocoder");
var Mail = require("./wrappers/mail");
var Letter = require("./wrappers/reportLetter");
var Uploader = require("./images/uploader");
var ImageStorage = require("./images/image_storage");
var stopwords = require("./util/stopwords");
var templates = require('./templates');

var getPost = function (postId, cb) {
	Post.findOne({_id: postId}, function (err, post) {
		if (err) return cb(err);

		if (!post) return cb({
			code: "BAD_INPUT_PARAMS",
			message: "Post " + postId + " haven't found"
		});
		cb(err, post);
	});
};

var facebookPostsQueue = async.queue((function () {
	var performTask = function (task, cb) {
		var fbPostParams = task.fbPostParams;
		var session = task.session;
		var facebookToken = session.facebookToken;
		var fb = new Facebook(facebookToken);
		switch (task.taskName) {
			case 'create':
				fb.createWallPost(fbPostParams, cb);
				break
			case 'update':
				fb.updateWallPost(fbPostParams, cb);
				break;
			case 'delete':
				fb.deleteWallPost(fbPostParams, cb);
				break;
			default:
				cb('unknown facebookPostsQueue task')
		}
	}

	return function (task, cb) {
		performTask(task, cb);
	}
})(), 2)

var blockedAddFriendPush = {}; // need for Handlers.addFriend

var Handlers = {

	logReq: (req, res, next) => {
		var ip = req.__requestIp;
		console.log('=============== REQUEST LOGGING START ===============');
		console.log('req.body:');
		console.log(req.body);
		console.log('req.params:');
		console.log(req.params);
		console.log('req.query:');
		console.log(req.query);
		console.log('req.files:');
		console.log(req.files);
		console.log('=============== REQUEST LOGGING END ===============');
		next(null, 'logged')
	},

	login: (req, res, next) => {
        console.log('login')
        var _____ = 'sv'
		let ip = req.__requestIp;
		let token = req.body.token;
		let restorePasswordToken = req.body.restorePasswordToken;
		let login = req.body.login;
		let password = req.body.password;
		let authWithToken = !!token;
		let authWithEmail = !!login && !!password && !restorePasswordToken;
		let authWithPasswordRestore = !!login && !!password && !!restorePasswordToken;

		let auth = new Auth(token);

		if (!authWithToken && !authWithEmail && !authWithPasswordRestore) {
			return next({
				"code": "WRONG_PARAMS",
				"message": "wrong params set"
			});
		} else if (authWithToken) {
			auth.loginWithFbToken({}, function (err, session) {
				if (err) return next(err);
				if (!session) {
					return next(STATUS.fbTokenInvalid)
				} else {
					res.cookie("sessionId", session._id, {maxAge: global.config.cookieMaxAge});
					req.result = (session.getPublicObject());
				}
				next();
			}, req);
		} else if (authWithEmail) {
			auth.login(login, password, function (err, session) {
				if (err) return next(err);
				if (!session) {
					return next(STATUS.loginInvalid)
				} else {
					res.cookie("sessionId", session._id, {maxAge: global.config.cookieMaxAge});
					req.result = (session.getPublicObject());
				}
				next();
			});
		} else if (authWithPasswordRestore) {

			let newPasswordHash = crypto.createHash('md5')
				.update(password)
				.digest('hex');

			async.waterfall([
				(cb) => {
					RestorePasswordToken.findOne({
						token: restorePasswordToken
					}, (err, restorePasswordToken) => {
						if (err) return cb(err);
						if (!restorePasswordToken) next(STATUS.restorePasswordTokenInvalid);
						cb(null, restorePasswordToken)
					})
				}, (restorePasswordToken, cb) => {
					User.findOneAndUpdate({
							_id: restorePasswordToken.userId,
							$or: [
								{nickname: {$eq: login}},
								{email: {$eq: login}}
							]
						},
						{password: newPasswordHash},
						{new: true},
						(err, user) => {
							if (err) return cb(err);
							if (!user) return cb(STATUS.userNotFound);
							restorePasswordToken.remove();
							cb(null, user)
						});
				}, (user, cb) => {
					Session.create(user, cb)
				}
			], (err, session) => {
				if (err) return next(err);
				if (!session) {
					return next(STATUS.loginInvalid)
				} else {
					res.cookie("sessionId", session._id, {maxAge: global.config.cookieMaxAge});
					req.result = (session.getPublicObject());
				}
				next();
			});
		}
	},

	logout: (req, res, next) => {
		var ip = req.__requestIp;
		if (!req.session) return next()
		async.parallel([
			(cb) => {
				User.update({_id: req.session.userId}, {
				$set: {pushTokens: []}
				}, cb)
			}, (cb) => {
				req.session.set({deleted: true}).save(cb)
			}
		], (err, result) => {
			if (err) return next(err)
			req.result = {};
			next()
		})
	},

	createProfile: (req, res, next) => {
		var ip = req.__requestIp;
		var profileParams = _.extend(req.body, req.params);
		var wasFile = false;
		var currentSession = req.session || {};
		if (currentSession.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}

		async.waterfall([
			(cb) => {
				if (req.get("Content-type") === "application/x-www-form-urlencoded") return cb(null, null);
				var up = new Uploader(req, global.config.images);
				up.on('error', cb);
				up.on('file', function (tempfile) {
					wasFile = true;
					global.logger.debug('uploaded file', tempfile);
					var imageStorage = new ImageStorage(global.config.images);
					imageStorage.store(tempfile, cb)
				});
				up.on("finish", function (wasFile) {
					if (!wasFile) cb(null, null);
				});
				up.start();
			}, (image, cb) => {
				// req.body was updated via multipart with busboy
				if (image) profileParams.avatarUrl = image;
				let tasks = [
					User.findOne.bind(User, {email: profileParams.email, userStatus:{$nin:["Deleted"]}})
				]
				if (profileParams.nickname && profileParams.nickname.length) {
					tasks.push(User.findOne.bind(User,
					{
						nickname: profileParams.nickname,
						userStatus: {$nin: ["Deleted"]}
					}
				))}
				profileParams.nickname || (profileParams.nickname = '');
				async.parallel(tasks, cb)
			}, ([userByEmail, userByNick], cb) => {
				if (userByEmail) {
					return cb(STATUS.emailExists)
				} else if (userByNick) {
					return cb(STATUS.nicknameExists)
				} else {
					if (profileParams.password) {
						profileParams.password = crypto.createHash('md5')
							.update(profileParams.password)
							.digest('hex');
					}

					let cityNameAr = [];
					if (profileParams.city) cityNameAr.push(profileParams.city)
					if (profileParams.state) cityNameAr.push(profileParams.state)
					profileParams.city = cityNameAr.join(', ')

					profileParams.isBusiness = false;
					let user = new User(profileParams);
					user.save((err, res) =>  cb(err, user));

					mailchimp.scheduleEmail(user.email, function() {
						console.log("Mailchimp campaign scheduled");
					});
				}
			}, (user, cb) => {
				(new Auth()).confirmEmail({
					userId: user._id || user.userId,
					userName: user.fullname,
					email: user.email
				}, (err, info) => {
					console.log('createProfile confirmEmail result')
					console.log(err, info)
				});
				Session.create(user, cb)
			}
		], function (err, userSession) {
			if (err) return next(err);
			res.cookie("sessionId", userSession._id, {maxAge: global.config.cookieMaxAge});
			req.result = (userSession.getPublicObject());
			next();
		});
	},

	createBusinessProfile: (req, res, next) => {
		var ip = req.__requestIp;
		var profileParams = _.extend(req.body, req.params);
		var wasFile = false;
		var currentSession = req.session || {};
		if (currentSession.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}

		async.waterfall([
			(cb) => {
				if (req.get("Content-type") === "application/x-www-form-urlencoded") return cb(null, null);
				var up = new Uploader(req, global.config.images);
				up.on('error', cb);
				up.on('file', function (tempfile) {
					wasFile = true;
					global.logger.debug('uploaded file', tempfile);
					var imageStorage = new ImageStorage(global.config.images);
					imageStorage.store(tempfile, cb)
				});
				up.on("finish", function (wasFile) {
					if (!wasFile) cb(null, null);
				});
				up.start();
			}, (image, cb) => {
				// req.body was updated via multipart with busboy
				if (image) profileParams.avatarUrl = image;
				let tasks = [
					User.findOne.bind(User, {email: profileParams.email, userStatus:{$nin:["Deleted"]}})
				]
				if (profileParams.nickname && profileParams.nickname.length) {
					tasks.push(User.findOne.bind(User,
					{
						nickname: profileParams.nickname,
						userStatus: {$nin: ["Deleted"]}
					}
				))}
				profileParams.nickname || (profileParams.nickname = '');
				async.parallel(tasks, cb)
			}, ([userByEmail, userByNick], cb) => {
				if (userByEmail) {
					return cb(STATUS.emailExists)
				} else if (userByNick) {
					return cb(STATUS.nicknameExists)
				} else {
					if (profileParams.password) {
						profileParams.password = crypto.createHash('md5')
							.update(profileParams.password)
							.digest('hex');
					}

					let cityNameAr = [];
					if (profileParams.city) cityNameAr.push(profileParams.city)
					if (profileParams.state) cityNameAr.push(profileParams.state)
					profileParams.city = cityNameAr.join(', ')

					profileParams.isBusiness = true;
					let user = new User(profileParams);
					user.save((err, res) =>  cb(err, user));

					mailchimp.scheduleEmail(user.email, function() {
						console.log("Mailchimp campaign scheduled");
					});
				}
			}, (user, cb) => {
				(new Auth()).confirmEmail({
					userId: user._id || user.userId,
					userName: user.fullname,
					email: user.email
				}, (err, info) => {
					console.log('createBusinessProfile confirmEmail result')
					console.log(err, info)
				});
				Session.create(user, cb)
			}
		], function (err, userSession) {
			if (err) return next(err);
			res.cookie("sessionId", userSession._id, {maxAge: global.config.cookieMaxAge});
			req.result = (userSession.getPublicObject());
			next();
		});
	},

	/**
	 * POST /profile
	 * @param req
	 * @param res
	 * @param next
     */
	updateProfile: (req, res, next) => {
		var wasFile = false;
		var confirmEmail = false;

		//TODO refactor this
		async.waterfall([
				(cb) => {
					if (req.get("Content-type") === "application/x-www-form-urlencoded")
						return cb(null, null);

					var up = new Uploader(req, global.config.images);
					up.on('error', cb);
					up.on('file', function (tempfile) {
						wasFile = true;
						global.logger.debug('uploaded file', tempfile);
						var imageStorage = new ImageStorage(global.config.images);
						imageStorage.store(tempfile, cb)
					});

					up.on("finish", function (wasFile) {
						if (!wasFile) cb(null, null);
					});

					up.start();
				},
				(image, cb) => {
					// req.body was updated via multipart with busboy
					if (image)
						req.body.avatarUrl = image;

					let data = req.body;
					if (!data.newPassword && !data.email)
						return cb(null, null);

					User.findOne({
							_id: req.session.userId
						},
						(err, user) => {
							if (err)
								return cb(err);

							let withUpdatingPass = !!data.newPassword;
							let updateEmail = !!data.email && data.email !== user.email;
							if (!withUpdatingPass && !updateEmail)
								return cb(null, null);

							if (withUpdatingPass) {
								let oldPassword = data.oldPassword ? crypto.createHash('md5')
									.update((data.oldPassword || ""))
									.digest('hex') : undefined; // need to check for existence

								let newPassword = crypto.createHash('md5')
									.update((data.newPassword || ""))
									.digest('hex');

								if (user.password == oldPassword) {
									req.body.password = newPassword; // update users password
									if (!updateEmail)
										return cb(null, null);
								}
								else {
									return cb(STATUS.invalidPassword);
								}
							}

							if (updateEmail) {
								User.findOne({email: data.email}, (err, checkUser) => {
									if (checkUser) {
										delete req.body.email;
										return cb(STATUS.emailExists);
									}
									user.set('isEmailConfirmed', false);
									req.session.user.isEmailConfirmed = false;
									req.body.pendingEmail = req.body.email;
									req.body.email = user.email;

									async.parallel([
											user.save.bind(user),
											(_cb) => {
												Session.update({
													userId: user._id
												}, {
													isEmailConfirmed: false,
													pendingEmail: data.email
												}, _cb);
											}
										],
										(err, [[user, num], session]) => {
											(new Auth()).confirmEmail({
													userId: user._id,
													userName: user.fullname || '',
													email: req.body.pendingEmail || ''
												},
												(err, info) => {
													console.log('updateProfile confirmEmail result');
													console.log(err, info);
												});
											cb(err, null);
										});
								});
							}
						});
				},
				(empty, cb) => {
					User.updateProfile(req.session, req.body, cb);
				}
			],
			(err) => {
				if (err)
					return next(err);
				req.result = (req.session.getPublicObject());
				next();
			});
	},

	linkFacebookToProfile: (req, res, next) => {
		let token = req.params.token || req.query.token || req.body.token;
		let fb = new Facebook(token);
		let session = req.session;
		if (req.session.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}
		async.waterfall([
			(cb) => {
				fb.getMyProfile(cb)
			}, (fbUser, cb) => {
				// check if not Deleted user with this fb account already exists, if yes - return error.
				User.findOne({
					"facebook.id": fbUser.id, userStatus: { $ne: "Deleted" }
				}, (err, user) => {
					if (user && user._id.toString() !== session.userId.toString()) return cb(STATUS.facebookExists)

					cb(err, fbUser)
				})
			}, (fbUser, cb) => {
				User.updateProfile(session, {
					facebookToken: token,
					facebook: fbUser
				}, (err, [updatedUser, [updatedSession, num]]) => {
					cb(err, updatedSession)
				})
			}
		], (err, userSession) => {
			if (err) return next(err);
			if (!userSession) return next(null, null);
			req.result = userSession.getPublicObject();
			next();
		})
	},

	locations: (req, res, next) => {
		var ip = req.__requestIp;
		var autocomplete = (req.query.autocomplete == "true");
		if (req.session.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}

		var g = new Gplaces(global.config.googlePlaces);
		var method;
		var callback = function (err, items, npt) {
			if (err) return next(err);
			var result = items.map(function (item) {
				return Place.fromGoogle(item, req.query);
			});
			req.pagination = npt ? {
				next_page_token: npt
			} : null;
			req.result = result;
			next();
		};

		let placesQuery = {
			lat: req.query.lat,
			lon: req.query.lon,
			npt: req.query.next_page_token,
			keyword: req.query.text,
			name: req.query.name
		}
		g.near(placesQuery, callback);
	},

	locationsExtended: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}

		var queryLat = req.query.lat;
		var queryLon = req.query.lon;
		var queryText = req.query.text || "";
		var queryDistance = req.query.distance || 100000;
		var queryLimit = 20;
		var queryOffset = 0;

		//var nptSplitter = '||||'
		//req.query.next_page_token || (req.query.next_page_token="");
		//var nextPageParams = req.query.next_page_token.split(nptSplitter);
		//if (nextPageParams.length > 1) {
		//	var googleNextPageToken = nextPageParams.shift();
		//} else {
		//	var googleNextPageToken = nextPageParams[0];
		//	nextPageParams.shift();
		//}
		var googleNextPageToken = req.query.next_page_token;

		var getGplaces = (cb) => {
			var g = new Gplaces(global.config.googlePlaces);
			var method;
			var callback = function (err, items = [], npt = '') {
				if (err) return cb(null, {items, npt});

				var gPlaces = items.map(function (item) {
					return Place.fromGoogle(item, req.query);
				});
				cb(null, {gPlaces, npt});
			};

			let placesQuery = {
				lat: req.query.lat,
				lon: req.query.lon,
				npt: req.query.next_page_token,
				keyword: req.query.text,
				name: req.query.name
			}
			g.near(placesQuery, callback);

		}
		var getCompanies = (cb) => {
			if (googleNextPageToken) return cb(null, []);
			var query = {
				lat: queryLat,
				lon: queryLon,
				distance: queryDistance,
				limit: queryLimit,
				offset: queryOffset,
				text: queryText
			}
			Company.getByLocation(query, function (err, companies) {
				if (err) return cb(err)
				var companiesPlaces = companies.map((company) => {
					return Place.fromCompany(company)
				})
				cb(null, companiesPlaces)
			})
		}

		async.parallel([
			getCompanies,
			getGplaces
		], (err, [companies=[], {gPlaces: gPlaces, npt:npt }={gPlaces: [], npt: ''} ]) => {

			if (err) return next(err)

			companies.forEach((company, i) => {
				_.remove(gPlaces, (gPlace = {}) => gPlace.placeId == company.placeId);
				gPlaces.push(company);
			})

			var mergedResult = _.chain(gPlaces)
				.compact()
				.sortByOrder(
				['distance', 'companyId'],
				['asc', 'asc']
			);
			req.result = mergedResult;
			req.pagination = npt ? {
				next_page_token: npt
			} : null;
			next()
		})
	},

	getProfile: (req, res, next) => {
		var ip = req.__requestIp;
		req.result = (req.session.getPublicObject());
		next();
	},

	getUserById: (req, res, next) => {
		if (req.session.userStatus == "Locked") {
			return next(STATUS.userLocked);
		}
		var userId = req.query.userId || req.params.userId || req.body.userId;
		var currentUser = req.session || {};
		User.getById(userId, (err, user) => {
			if (err) return next(err);
			if (user) req.result = user.getPublicObject(currentUser);
			next();
		})
	},

	getUsersFacebookFriends: (req, res, next) => {
		var currentUser = req.session;

		if (!currentUser) return next(STATUS.unauthorized);
		if (!_.get(currentUser, 'facebook.id')) return next(STATUS.noFbAccount);
		if (!_.get(currentUser, 'facebookToken')) return next(STATUS.noFbAccessToken);
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var userId = currentUser.userId;
		var fbId = _.get(currentUser, 'facebook.id');
		var facebookToken = _.get(currentUser, 'facebookToken');

		var fb = new Facebook(facebookToken);
		//fb.getFriendsFromFacebook(fbId, cb)

		async.waterfall([
			(cb) => {
				fb.getFriendsFromFacebook(fbId, cb);
			}, (fbFriends, cb) => {
				var fbIds = _.pluck(fbFriends, 'id');
				User.find({
					"facebook.id": {
						$in: fbIds
					}
				}, function (err, users) {
					async.map(users, function (user, cb) {
						cb(null, user.getPublicObject(currentUser));
					}, function (err, users) {
						cb(err, users);
					});
				});
			}
		], (err, friends) => {
			if (err) return next(err);
			req.result = friends;
			next();
		});
	},

	getFriends: (req, res, next) => {
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		async.waterfall([
			(cb) => {
				User.find(User.createFindOptions({ "_id": { $in: req.session.friends, $nin: req.session.blockedUsers }}, true))
					.exec((err, friends) => {
					if (err) return cb(err);
					cb(null, friends);
				});
			},
			(friends, cb) => {
				if (!friends || !friends.length > 0) return cb(null, []);
				async.map(friends, (friend, _cb) => {
					_cb(null, friend.getPublicObject(req.session.user))
				}, (err, friends) => {
					if (err) return cb(err);
					cb(null, friends);
				})
			}
		], (err, friends) => {
			if (err) return next(err);
			req.result = friends;
			next()
		});
	},

	getFollowers: (req, res, next) => {
		var currentUser = req.session || {};
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		async.waterfall([
			(cb) => {
				User.find(User.createFindOptions({ "friends": { $in: [req.session.userId] } }, true))
					.exec((err, followers) => {
					if (err)
						return cb(err);
					cb(null, followers);
				});
			}, (followers, cb) => {
				if (!followers || !followers.length > 0) return cb(null, []);
				async.map(followers, (follower, _cb) => {
					_cb(null, follower.getPublicObject(currentUser))
				}, (err, followers) => {
					if (err) return cb(err);
					cb(null, followers);
				})
			}
		], (err, followers) => {
			if (err) return next(err);
			req.result = followers;
			next()
		});
	},

	addFriend: (req, res, next) => {
		var currentUser = req.session;
		var currentUserId = req.session.userId;
		var invitedUsers = _.get(currentUser, 'invitedUsers', [])
		var friends = _.get(currentUser, 'friends', [])
		var friendId = req.params.userId;
		var alreadyFreind = _.contains(friends,friendId);
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		// You can not add yourself to list of your friends
		if (String(req.session.userId) === String(friendId)) {
			return next(STATUS.yourselfToFriends)
		}
		// if already added, then just return current user
		if (alreadyFreind) {
			req.result = req.session.getPublicObject()
			return next()
		}

		// need push only if 'add friend' operation with this friendId was not earlier then { blockPeriod } ago
        var blockPeriod = 1000 * 60 // 1 minute
		var now = Date.now()
		var needPush = !blockedAddFriendPush[friendId] || blockPeriod < (now - blockedAddFriendPush[friendId]);

		async.waterfall([
			(cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$addToSet: {
						friends: friendId,
						invitedUsers: friendId
					}
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				if (user && needPush) {
                    blockedAddFriendPush[friendId] = Date.now();
					global.server.emit("push.friend", {
						actor: user,
						friendId: friendId
					});
				}
				Session.create(user, (err, session) => {
					if (err) return cb(err);
					cb(err, session);
				})
			}
		], (err, session) => {
			if (err) return next(err)
			req.result = session.getPublicObject();
			next()
		})
	},

	removeFriend: (req, res, next) => {
		let friendId = req.params.userId;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		async.waterfall([
			(cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$pull: {
						friends: friendId
					}
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				Session.create(user, cb)
			}
		], (err, session) => {
			if (err) return next(err)
			req.result = session.getPublicObject();
			next()
		})
	},

	addFriends: (req, res, next) => {
		var ip = req.__requestIp;
		// TODO what to return if no params, all or nothing ?
		//if (_.isEmpty(req.query)) return next()
		var currentUser = req.session || {};
		var friendEmails = req.body.email.split(",")
		console.log(friendEmails);
		var friends = _.get(currentUser, 'friends', [])
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		async.waterfall([
			(cb) => {
				User.find({
					email: {
						$in: friendEmails
					}
				}, (err, users) => {
					if (err) return cb(err);
					cb(null, users.map(function(user) {
						return user._id;
					}));
				})
			}, (userIds, cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$pushAll: {
						friends: userIds,
						invitedUsers: userIds
					}
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				if (user) {
					for (var i = 0; i < user.friends.length; i++) {
						var friendId = user.friends[i];
						if (!_.contains(friends, friendId)) {
							// need push only if 'add friend' operation with this friendId was not earlier then { blockPeriod } ago
							var blockPeriod = 1000 * 60 // 1 minute
							var now = Date.now()
							var needPush = !blockedAddFriendPush[friendId] || blockPeriod < (now - blockedAddFriendPush[friendId]);
							if (needPush) {
								blockedAddFriendPush[friendId] = Date.now();
								global.server.emit("push.friend", {
									actor: user,
									friendId: friendId
								});
							}
						}
					}
				}
				Session.create(user, (err, session) => {
					if (err) return cb(err);
					cb(err, session);
				})
			}, (session, cb) => {
				User.find(User.createFindOptions({ "_id": { $in: req.session.friends, $nin: req.session.blockedUsers }}, true))
					.exec((err, friends) => {
					if (err) return cb(err);
					cb(null, friends);
				});
			},
			(friends, cb) => {
				if (!friends || !friends.length > 0) return cb(null, []);
				async.map(friends, (friend, _cb) => {
					_cb(null, friend.getPublicObject(req.session.user))
				}, (err, friends) => {
					if (err) return cb(err);
					cb(null, friends);
				})
			}
		], (err, friends) => {
			if (err) return next(err)
			req.result = friends;
			next()
		})
	},

	getUserByNickname: (req, res, next) => {
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var nickName = req.query.nickName || req.params.nickName || req.body.nickName;
		var currentUser = req.session || {};

		async.waterfall(
			[
				//TODO: add check on the status

				(cb) => {
					User.findOne(User.createFindOptions({nickname: nickName}, false), (err, user) => {
						if (err)
							return cb(err);
						cb(null, user);
					});
				},
				(user, cb) => {
					if (!user) // if user is not found - go to output
						return cb(null, user);

					// user found - register in viewed
					User.findOneAndUpdate({
						_id: currentUser.userId,
						viewedProfiles: {
							$ne: user._id
						}
					}, {
						$push: {
							viewedProfiles: {
								$each: [user._id],
								$position: 0
							}
						}
					}, (err) => {
						if (err)
							return cb(err);
						cb(null, user);
					});
				}
			],
			(err, user) => {
				if (err) return next(err);
				req.result = user ? user.getPublicObject(currentUser) : null;
				next();
			}
		);
	},

	findUsers: (req, res, next) => {
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var ip = req.__requestIp;
		// TODO what to return if no params, all or nothing ?
		//if (_.isEmpty(req.query)) return next()
		var currentUser = req.session || {};
		User.search({ text: req.query.text,  email: req.query.email },
			function (err, users) {
			if (err) return next(err);
			req.result = users.map(function (user) {
				return user.getPublicObject(currentUser);
			});
			next();
		});
	},

	// TODO: not used, wipe
	//findUserByNickName: (req, res, next) => {
	//	var ip = req.__requestIp;
	//	if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
	//	var nickName = req.params['nickname']
	//	if (!nickName) return next();
	//	var currentUser = req.session || {};
	//	User.findByNickName(nickName, function (err, user) {
	//		if (err) return next(err);
	//		if (user) req.result = user.getPublicObject(currentUser);
	//		else req.result = null
	//		next();
	//	});
	//},

	getViewedUsers: (req, res, next) => {
		var ip = req.__requestIp;
		var currentUser = req.session || {};
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var viewedIds = [];
		async.waterfall([
			(cb) => {
				User.findOne(User.createFindOptions({_id: req.session.userId}, false), (err, user) => {
					if (err)
						return cb(err);
					cb(null, user);
				});
			},
			(user, cb) => {
				console.log("1: " + user.viewedProfiles);
				viewedIds = user.viewedProfiles;
				User.find({
					"_id": {
						$in: user.viewedProfiles
					},
					"userStatus": "Normal"
				})
				.exec((err, viewed) => {
					if (err) return cb(err)
					cb(null, viewed)
				})
			}, (viewed, cb) => {
				if (!viewed || !viewed.length > 0) return cb(null, []);
				async.map(viewed, (profile, _cb) => {
					_cb(null, profile.getPublicObject(currentUser))
				}, (err, viewed) => {
					if (err) return cb(err);
					cb(null, viewed);
				})
			}
		], (err, viewed) => {
			if (err) return next(err)
			// Fix sort order
			var result = [];
			for (var i = 0; i < viewedIds.length; i++) {
				var viewedId = viewedIds[i].toString();
				var index = _.findIndex(viewed, function(o) {return o.userId == viewedId});
				if (index >= 0) {
					result[result.length] = viewed[index];
				}
			}
			req.result = result;
			next()
		})
	},

	clearViewedUsers: (req, res, next) => {
		var ip = req.__requestIp;
		var currentUser = req.session || {};
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		async.waterfall([
			(cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$set: {
						viewedProfiles: []
					}
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				Session.create(user, (err, session) => {
					if (err) return cb(err);
					cb(err, session);
				})
			}
		], (err, session) => {
			if (err) return next(err)
			req.result = session.getPublicObject();
			next()
		})
	},

	blockUser: (req, res, next) => {
		var userId = req.params.userId;
		var currentUser = req.session || {};
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		async.waterfall([
			(cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$addToSet: {
						blockedUsers: userId
					}
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				Session.create(user, (err, session) => {
					if (err) return cb(err);
					cb(err, session);
				})
			}
		], (err, session) => {
			if (err) return next(err)
			req.result = session.getPublicObject();
			next()
		})
	},

	unblockUser: (req, res, next) => {
		let userId = req.params.userId;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		async.waterfall([
			(cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					$pull: {
						blockedUsers: userId
					},
				}, {
					new: true
				}, (err, user) => {
					if (err) return cb(err);
					cb(null, user)
				})
			}, (user, cb) => {
				Session.create(user, cb)
			}
		], (err, session) => {
			if (err) return next(err)
			req.result = session.getPublicObject();
			next()
		})
	},

	createPost: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var newPost;
		var uploadedImage;
		async.waterfall([
			(cb) => {
				if (req.get("Content-type") === "application/x-www-form-urlencoded") return cb(null, null);

				var up = new Uploader(req, {
					mimeTypes: global.config.images.mimeTypes,
					maxFileSize: global.config.images.maxFileSize
				});
				up.on('error', cb);
				up.on('file', function (tempfile) {
					global.logger.debug('uploaded file', tempfile);
					var imageStorage = new ImageStorage(global.config.images);
					imageStorage.store(tempfile, cb)
					if (req.body.wallPost === 'true') {

					}
				});
				up.on('finish', function (isFile) {
					if (!isFile) cb(null, null);
				});
				up.start();
			},(image, cb) => {
				uploadedImage = image;
				let wallPost = req.body.wallPost;
				var placeId = req.body.placeId;
				var facebookPlaceId = req.body.facebookPlaceId;

				if (placeId) return cb(null, {placeId, facebookPlaceId});
				else {
					// Придумываем placeId чтобы создать псевдоплейс (и закешировать его)
					let placeId = global.mongoose.Types.ObjectId();
					geocoder.getCity(req.body.lat, req.body.lon, function (err, city) {
						var place = Place.fromGoogle({
							geometry: {
								location: {
									lat: req.body.lat,
									lng: req.body.lon
								}
							},
							land: city,
							place_id: placeId
						});
						place.set("fake", true);
						place.save(function (err, place) {
							cb(err, {placeId, facebookPlaceId});
						});
					});
				}
			}, ({placeId, facebookPlaceId}, cb) => {
				var obj = {
					location: {
						"type": "Point",
						"coordinates": [req.body.lon, req.body.lat]
					},
					photo: uploadedImage,
					authorId: req.session.userId,
					companyId: req.body.companyId || null,
					description: stopwords.autoReplace(req.body.description) || null,
					isCheckedIn: !!req.body.facebookPlaceId,
					facebookPostType: req.body.facebookPostType,
					ts: new Date(),
					subscribers: [req.session.userId],
					unsubscribers: [],
					isLandscapePhoto: req.body.isLandscapePhoto === 'true' ? true : false,
					notifiedUsers: [],
					isDeal: req.body.isDeal
				};
				if (placeId) obj.placeId = placeId;
				if (facebookPlaceId) obj.facebookPlaceId = facebookPlaceId;

				var post = new Post(obj);
				post.save((err, post) => {
					newPost = post;
					cb(err, post);
				});
			}, (post, cb) => {
				if (req.body.wallPost === 'true') {
					var fbQueuePost = {
						taskName: 'create',
						fbPostParams: {
							postMessage: stopwords.normalize(post.description),
							photo: post.photo,
							placeId: post.placeId,
							facebookPlaceId: post.facebookPlaceId,
							isCheckedIn: post.isCheckedIn,
							facebookPostType: post.facebookPostType,
							user: req.session
						},
						session: req.session,
					}
					var fbPostCallback = function (post, fbError, fbPost) {
						if (fbError || !fbPost || !fbPost.id) {
							global.logger.warn('FB POSTING FAILED:')
							console.warn(fbError);
						} else {
							global.logger.info('FB POSTING SUCCESS', fbPost);
							Post.update(
								{
									_id: post._id
								}, {
									facebookPostId: fbPost.id,
									isPostedOnFacebook: true

								}, function (err, res) {
									if (err) global.logger.warn(err)
								}
							)
						}
					}.bind(this, post);

					facebookPostsQueue.push(fbQueuePost, fbPostCallback);
				}
				cb(null, post);
			}
		], function (err, post) {
			if (err) return next(err);

			global.server.emit("push.post", {
				authorId: newPost.authorId,
				actor: req.session,
				postId: newPost._id,
				postText: req.body.description,
				postLocation: {lat: newPost.location.coordinates[1], lon: newPost.location.coordinates[0]},
				notifiedUsers: newPost.notifiedUsers
			});
			post.getPublicObject(req, function (err, publicPost) {
				req.result = publicPost;
				next();
			});
		});
	},

	findPosts: (req, res, next) => {
		var ip = req.__requestIp;
		var lat = req.query.lat || null;
		var lon = req.query.lon || null;

		var withLocation = !!lat && !!lon && !req.query.hashtag && !req.query.createdBy && !req.query.likedBy && !req.query.placeId;
		var withHashTag = !!req.query.hashtag;
		var withAuthor = !!req.query.createdBy;
		var withLikes = !!req.query.likedBy;
		var byPlaceid = !!req.query.placeId;

		var deals;
		if (typeof req.query.deals !== 'undefined') {
			deals = req.query.deals == '1' ? true : false;
		}

		var currentUser = req.session || {};

		var processList = function (err, results) {
			if (err) return next(err);
			var limited = paginate(results, req.query);
			async.map(limited, function (item, cb) {
				item.getPublicObject(req, (err, item) => {
					if (!!lat && !!lon && !!item.location.lat && !!item.location.lon) {
						var distance = geolib.getDistance(
								{latitude: +lat, longitude: +lon},
								{latitude: +item.location.lat, longitude: +item.location.lon}
							) || 0;
						item.distance = distance;
					}
					cb(null, item)
				});
			}, function (err, items) {
				if (err) return next();
				req.result = (items);
				next();
			});
		};

		if (withLocation) {
			if (req.session.userStatus == "Locked") {
				return next(STATUS.userLocked);
			}
			
			return Post.nearby(deals, lat, lon, req.query.distance, currentUser, processList);
		} else if (withHashTag) {
			if (req.session.userStatus == "Locked") {
				return next(STATUS.userLocked);
			}
			
			return Post.hashtag(deals, req.query.hashtag, currentUser, processList);
		} else if (withAuthor) {
			return Post.findByAuthorId(deals, req.query.createdBy, processList);
		} else if (withLikes) {
			if (req.session.userStatus == "Locked") {
				return next(STATUS.userLocked);
			}
			
			return Post.findLikedByAuthorId(deals, req.query.likedBy, processList);
		} else if (byPlaceid) {
			if (req.session.userStatus == "Locked") {
				return next(STATUS.userLocked);
			}
			
			return Post.getByPlaceId(deals, req.query.placeId, currentUser, processList)
		} else {
			return next({
				code: "BAD_INPUT_PARAMS",
				message: "No params"
			});
		}
	},

	getPost: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		async.waterfall([
			Post.findOne.bind(Post, {_id: req.params["postId"]}),
			function (post, cb) {
				if (!post) return cb("Post not found");
				req.returnAllComments = true;
				post.getPublicObject(req, cb);
			},
			function (post, cb) {
				req.result = post;
				cb();
			}
		], next);
	},

	deletePost: (req, res, next) => {
		var ip = req.__requestIp;

		var postId = req.params["postId"]

		async.waterfall([
			Post.findOne.bind(Post, {_id: postId}),
			function (post, cb) {
				// respond with success status, post not exists or already deleted
				if (!post) return cb(null, "success");

				// check for existance facebook post and if got
				if (post.facebookPostId) {
					var fbQueuePost = {
						taskName: 'delete',
						fbPostParams: {
							id: post.facebookPostId
						},
						session: req.session
					};
					var fbPostCallback = function () {

					}.bind(this, post);

					facebookPostsQueue.push(fbQueuePost, fbPostCallback);
				}

				Notification.removeByPostID(post._id, function () {
					post.remove(cb);
				});
			}
		], next);
	},

	updatePost: (req, res, next) => {
		var ip = req.__requestIp;
		var postId = req.params["postId"]
		var newPost;
		var uploadedImage;
		async.waterfall([
			function (cb) {
				if (req.get("Content-type") === "application/x-www-form-urlencoded") return cb(null, null);

				var up = new Uploader(req, {
					mimeTypes: global.config.images.mimeTypes,
					maxFileSize: global.config.images.maxFileSize
				});
				up.on('error', cb);
				up.on('file', function (tempfile) {
					global.logger.debug('uploaded file', tempfile);
					var imageStorage = new ImageStorage(global.config.images);
					imageStorage.store(tempfile, cb)
				});
				up.on('finish', function (isFile) {
					if (!isFile) cb(null, null);
				});
				up.start();
			},
			function (image, cb) {
				uploadedImage = image;
				if (req.body.placeId) return cb(null, req.body.placeId);
				else {
					// Придумываем placeId чтобы создать псевдоплейс (и закешировать его)
					var placeId = global.mongoose.Types.ObjectId();
					geocoder.getCity(req.body.lat, req.body.lon, function (err, city) {
						var place = Place.fromGoogle({
							land: city,
							place_id: placeId,
							geometry: {
								location: {
									lat: req.body.lat,
									lng: req.body.lon
								}
							}
						});
						place.set("fake", true);
						place.save(function (err, place) {
							cb(err, placeId);
						});
					});
				}
			},
			function (placeId, cb) {
				var newArguments = req.body

				Post.findOne({_id: postId}, function (err, post) {
					post.set(newArguments)
					post.save(function (err, post) {
						newPost = post;
						cb(err, post);
					});
				})
			},
			function (post, cb) {
				var updateWallPost = !!post.isPostedOnFacebook && !!post.facebookPostId;
				var publishWallPost = !!post.isPostedOnFacebook && !post.facebookPostId;
				var unPublishWallPost = !post.isPostedOnFacebook && !!post.facebookPostId;
				var doNothingWithWallPost = !post.isPostedOnFacebook && !post.facebookPostId;

				if (updateWallPost) {
					var fbQueuePost = {
						taskName: 'update',
						fbPostParams: {
							id: post.facebookPostId,
							postMessage: stopwords.normalize(post.description),
							photo: post.photo,
							placeId: post.placeId,
							user: req.session
						},
						session: req.session
					}
				}
				if (publishWallPost) {
					var fbQueuePost = {
						taskName: 'create',
						fbPostParams: {
							postMessage: stopwords.normalize(post.description),
							photo: post.photo,
							placeId: post.placeId,
							user: req.session
						},
						session: req.session
					}
				}
				if (unPublishWallPost) {
					var fbQueuePost = {
						taskName: 'delete',
						fbPostParams: {
							id: post.facebookPostId
						},
						session: req.session
					}
				}

				if (doNothingWithWallPost) return cb(null, post);

				var fbPostCallback = function (post, fbError, fbPost) {
					if (fbError) {
						global.logger.warn('FB UPDATING FAILED')
						global.logger.info(fbPost)
					} else {
						global.logger.info('FB UPDATING SUCCESS', fbPost)
						Post.update({_id: post._id}, {facebookPostId: fbPost.id}, function (err, res) {
							if (err) global.logger.warn(err)
						})
					}
				}.bind(this, post);

				facebookPostsQueue.push(fbQueuePost, fbPostCallback);
				cb(null, post);

			}
		], function (err, post) {
			if (err) return next(err);

			global.server.emit("push.post", {
				authorId: newPost.authorId,
				actor: req.session,
				postId: newPost._id,
				postText: req.body.description,
				postLocation: {lat: newPost.location.coordinates[1], lon: newPost.location.coordinates[0]},
				notifiedUsers: newPost.notifiedUsers
			});
			post.getPublicObject(req, function (err, publicPost) {
				req.result = publicPost;
				next();
			});
		});
	},

	createComment: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var postId = req.params['postId'];

		getPost(postId, function (err, post) {
			if (err) return next(err);
			var commentObj = req.body;
			if (!req.session || !req.session.userId) return next('unauthorised')
			commentObj.userId = req.session.userId
			var comment = new Comment(commentObj);
			//global.logger.debug(comment);
			post.comments.push(comment);
			// post.subscribers.addToSet(req.session.userId);
			User.findOne({
				_id: post.authorId
			}, (err, postAuthor) => {
				if (err) return next(err);

				global.server.emit("push.comment", {
					authorId: post.authorId,
					author: postAuthor,
					actor: req.session,
					postId: post._id,
					subscribers: post.subscribers,
					unsubscribers: post.unsubscribers || [],
					commentText: req.body.text
				});
			})
			post.save(function (err, res) {
				if (err) return next(err);
				comment.getPublicObject(function (err, publicComment) {
					if (err) return next(err);
					req.result = publicComment;
					next();
				})
			});
		});

	},

	getComments: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var postId = req.params['postId'];

		getPost(postId, function (err, post) {
			if (err) return next(err);
			async.map(post.comments, function (rawData, next) {
				var comment = new Comment(rawData);
				comment.getPublicObject(next);
			}, function (err, comments) {
				if (err) return next(err);
				req.result = (comments);
				next();
			});
		});

	},

	getComment: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		//db.posts.find({_id: ObjectId("55d165f0fc8be75f034d268d"), "comments._id": ObjectId("55d16f3a9347b6df03d90184") }, {_id:0,"comments.$":1})
		var postId = req.params['postId'];
		getPost(postId, function (err, post) {
			if (err) return next(err);
			async.map(post.comments, function (rawData, next) {
				var comment = new Comment(rawData);
				comment.getPublicObject(next);
			}, function (err, comments) {
				if (err) return next(err);
				req.result = (comments);
				next();
			});
		});

	},

	updateComment: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var postId = req.params['postId'];
		var commentId = req.body['commentId'];

		getPost(postId, function (err, post) {
			if (err) return next(err);
			Post.update(
				{
					'comments._id': commentId
				},
				{
					'$set': {
						'comments.$.text': req.body.text
					}
				},
				function (err, updateResult) {
					global.server.emit("undate.comment", {
						authorId: post.authorId,
						actor: req.session,
						postId: post._id,
						commentText: req.body.text
					});
					var commentObj = req.body;
					_.extend(commentObj, {
						_id: commentId,
						userId: req.session.userId
					})
					var comment = new Comment(commentObj);
					req.result = comment;
					next();
				}
			);
		});
	},

	deleteComment: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var postId = req.params['postId'];
		var commentId = req.query['commentId'];

		getPost(postId, function (err, post) {
			if (err) return next(err);
			Post.update(
				{
					_id: postId
				},
				{
					'$pull': {
						'comments': {
							_id: commentId
						}
					}
				},
				function (err, updateResult) {
					global.server.emit("delete.comment", {
						authorId: post.authorId,
						actor: req.session,
						postId: post._id,
						commentText: req.body.text
					});
					next(null, 'success');
				}
			);
		});

	},

	subscribePost: (req, res, next) => {
		var postId = req.params.postId;
		var userId = req.session.userId;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		Post.findOneAndUpdate({
			_id: postId
		}, {
			$addToSet: {
				subscribers: userId
			},
			$pull: {
				unsubscribers: userId
			}
		}, {new: true}, (err, post) => {
			if (err) return next(err);
			post.getPublicObject(req, (err, publicPost) => {
				if (err) return next(err);
				req.result = publicPost;
				next();
			});
		})
	},

	unsubscribePost: (req, res, next) => {
		var postId = req.params.postId;
		var userId = req.session.userId;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		Post.findOneAndUpdate({
			_id: postId
		}, {
			$addToSet: {
				unsubscribers: userId
			},
			$pull: {
				subscribers: userId
			}
		}, {new: true}, (err, post) => {
			if (err) return next(err);
			post.getPublicObject(req, (err, publicPost) => {
				if (err) return next(err);
				req.result = publicPost;
				next();
			});
		})
	},

	like: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var postId = req.body.postId || req.query.postId || req.params.postId;

		//@TODO привести в богочинный вид этот пиздец
		Post.findOne({_id: postId}, function (err, post) {
			if (err || !post) return global.logger.error(err || "Post not found");
			var isWheneverLiked = post.wheneverLiked.some(function (liker) {
				return liker.toString() == req.session.userId;
			});

			var isLiked = post.likes.some(function (liker) {
				return liker.toString() == req.session.userId;
			});

			// this user already likes, do nothing
			if (isLiked) {
				req.result = ({});
				return next();
			} else {
				post.likes.push(req.session.userId);
				if (!isWheneverLiked) {
					post.wheneverLiked.push(req.session.userId);
				}
				post.save(function (err) {
					if (err) return next(err);
					global.logger.debug("RAX2", arguments);
					req.result = ({});
					next();
				});
			}
			if (!isWheneverLiked) {
				global.server.emit("push.like", {
					authorId: post.authorId,
					subscribers: post.subscribers,
					unsubscribers: post.unsubscribers,
					actor: req.session,
					postId: post._id,
					postText: stopwords.normalize(post.description)
				});
			}
		});

	},

	dislike: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var userId = mongoose.Types.ObjectId(req.session.userId);

		Post.update({_id: req.query.postId}, {
			$pull: {likes: userId}
		}, function (err) {
			if (err) return next(err);

			req.result = ({});
			next();
		});
	},

	getLikedUser: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		var postId = req.query.postId || req.params.postId || req.body.postId;
		getPost(postId, function (err, post) {
			if (err) return next(err);

			async.map(post.likes, function (userId, cb) {
				User.findOne({_id: userId}, function (err, user) {
					if (err) return cb(err);
					cb(null, user.getPublicObject());
				})
			}, function (err, users) {
				if (err) return next(err);

				req.result = (users);
				next();
			});

		});
	},

	report: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);
		req.body.user = req.session;
		var letter = new Letter(req.body);
		var mail = new Mail({
			emails: global.config.mail.reportEmails,
			gmail: global.config.mail.gmail,
			letter: letter,
			from: req.session.email

		});
		mail.send(function (err) {
			if (err) return next(err);
			req.result = {};
			next();
		});
	},

	/**
	 * Set device (push) token for current user
	 * @param req
	 * @param res
	 * @param next
     */
	pushToken: (req, res, next) => {
		var currentUserId = req.session.userId;
		var token = req.body.token;

		async.waterfall([
				(callback) => { // update current user token
					User.update({_id: currentUserId}, {$set: {pushTokens: [token]}}, callback);
				},
				(updateResult, callback) => {
					if (updateResult.n > 0) // remove assigned token from other users
						User.update({_id: {$ne: currentUserId}}, {$pullAll: {pushTokens: [token]}}, {multi: true}, callback);
					else
						callback(null, updateResult);
				}
			],
			(err) => {
				if (err)
					return next(err);

				req.result = {};
				next();
			});
	},

	getNotificationSettings: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var userId = req.session.userId;
		User.getById(userId, (err, user) => {
			if (err) return next(err);
			if (user) req.result = user.getPushNotificationSettings();
			next();
		});
	},

	updateNotificationSettings: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var settings = req.body;
		var userId = req.session.userId;
		User.updatePushNotificationSettings(userId, settings, (err, user) => {
			if (err) return next(err);
			req.result = user.getPushNotificationSettings();
			next();
		});
	},

	updateNotificationsLocation: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var location = req.body;
		var userId = req.session.userId;
		User.updatePushNotificationLocation(userId, location, (err, user) => {
			if (err) return next(err);
            next();
		});
	},

	getNotifications: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var params = _.extend({}, req.params, req.query);
		var offset = parseInt(params.offset);
		var limit = parseInt(params.limit);

		var notificationsQuerySelector = {"receivers": {$elemMatch: {userId: req.session.userId}}};
		if (!params.buildNumber) { /* Backward compatibility */
			notificationsQuerySelector.notifType = { $nin: [ 'post.connection', 'post.vicinity' ] };
		}

		var notificationsQuery = Notification
					.find(notificationsQuerySelector)
					.sort({'ts': -1});

		if (!isNaN(offset) && !isNaN(limit)) {
			notificationsQuery = notificationsQuery
				.skip(offset)
				.limit(limit);
		}

		notificationsQuery.exec((err, notifications) => {
			async.map(
				notifications,
				function (notification, next) {
					Notification.getPublicObject(notification, req.session.userId, next);
				},
				function (err, results) {
					if (err) return next(err);
					req.result = results;
					next();
				});
	  });
	},

	updateNotifications: (req, res, next) => {
		var ip = req.__requestIp;
		var currentUser = req.session || {};

		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var response = function() {
			if (currentUser) {
				User.findOne({_id: currentUser.userId}, function (err, user) {
					User.getCountUnviewedNotifications(currentUser.userId, function (count) {
						req.result = ({count: count || 0});
						next();
					});
				});
			} else {
				next();
			}
		}

		if (req.params["userId"]) {
			User.updateNotifications(currentUser, req.query["userId"], response);
		} else {
      Post.updateNotifications(currentUser, req.query["postId"], response);
		}
	},

	setViewed: (req, res, next) => {
		var ip = req.__requestIp;

		Post.update({_id: req.params["postId"]}, {
			$addToSet: {viewedUsers: req.session.userId}
		}, function (err) {
			if (err) return next(err);

			req.result = ({});
			next();
		});

	},

	notificationsSeen: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		async.doWhilst((next) => {
			Notification.update({
				"receivers.userId": req.session.userId,
				"receivers.viewed": false
			}, {$set :{ "receivers.$.viewed": true}}, { multi: true }, next);
		}, (res) => res.nModified > 0, next);
	},

	notificationsUnviewed: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		User.findOne({_id: req.session.userId}, function (err, user) {
			User.getCountUnviewedNotifications(req.session.userId, function( count){
			req.result =  ({ count: count || 0});
			next()});
		});
	},

	wipe: (req, res, next) => {
		var ip = req.__requestIp;
		async.parallel([
			User.remove.bind(User, {}),
			Post.remove.bind(Post, {})
		], next);


	},

	createCompany: (req, res, next) => {
		var ip = req.__requestIp;
		req.body.location || (req.body.location = {})
		var companyObj = req.body
		companyObj.location = {
			type: "Point",
			coordinates: [req.body.location.lon, req.body.location.lat]
		}
		companyObj.createdAt = new Date()
		Company.create(companyObj, function (err, company) {
			if (err) return next(err);
			req.result = company
			next()
		})
	},

	getCompany: (req, res, next) => {
		var ip = req.__requestIp;
		var companyId = req.params["companyId"] || req.query["companyId"] || req.body["companyId"];
		Company.getById(companyId, function (err, company) {
			if (err) return next(err)
			req.result = company;
			next()
		})
	},

	getCompanies: (req, res, next) => {
		var ip = req.__requestIp;
		var companySearchParams = _.extend({}, req.params, req.query);
		Company.search(companySearchParams, function (err, result) {
			if (err) return next(err)
			req.result = result;
			next();
		})
	},

	getCompanyByGooglePlaceId: (req, res, next) => {
		var ip = req.__requestIp;
		var googlePlaceId = req.query.googlePlaceId || req.params.googlePlaceId;

		var gPlaces = new Gplaces(global.config.googlePlaces);
		gPlaces.getByPlaceId(googlePlaceId, function (err, gPlace) {
			if (err) {
				console.error(err)
				return next(err)
			}
			req.result = gPlace
			next()
		})
	},

	getGooglePlaceByGooglePlaceId: (req, res, next) => {
		var ip = req.__requestIp;
		var googlePlaceId = req.query.googlePlaceId || req.params.googlePlaceId;

		// get full gplace obj
		var gPlaces = new Gplaces(global.config.googlePlaces);
		gPlaces.getByPlaceId(googlePlaceId, function (err, gPlace) {

			// get model place obj ( without caching now )
			//Place.getByGooglePlaceId(googlePlaceId,function (err, gPlace) {
			if (err) {
				console.error(err)
				return next(err)
			}
			req.result = gPlace
			next()
		})
	},

	updateCompany: (req, res, next) => {
		var ip = req.__requestIp;
		var companyId = req.params["companyId"] || req.query["companyId"] || req.body["companyId"];
		if (req.body["companyId"]) delete req.body["companyId"]

		req.body.location || (req.body.location = {})
		var updateCompanyObj = req.body
		updateCompanyObj.location = {
			type: "Point",
			coordinates: [req.body.location.lon, req.body.location.lat]
		}
		updateCompanyObj.filledWithGoogle = false
		updateCompanyObj.updatedAt = new Date()
		Company.findOneAndUpdate(
			{
				_id: companyId
			},
			updateCompanyObj,
			{
				new: true
			},
			function (err, company) {
				if (err) return next(err);
				req.result = company
				next()
			}
		)
	},

	uploadFile: (req, res, next) => {
		var ip = req.__requestIp;
		var uploadedImage;
		async.waterfall([
			function (cb) {
				if (req.get("Content-type") === "application/x-www-form-urlencoded") return cb(null, null);

				var up = new Uploader(req, {
					mimeTypes: global.config.images.mimeTypes,
					maxFileSize: global.config.images.maxFileSize
				});
				up.on('error', cb);
				up.on('file', function (tempfile) {
					global.logger.debug('uploaded file', tempfile);
					var imageStorage = new ImageStorage(global.config.images);
					imageStorage.store(tempfile, cb)
				});
				up.on('finish', function (isFile) {
					if (!isFile) cb(null, null);
				});
				up.start();
			},
			function (image, cb) {
				uploadedImage = image;
				var imageObj = {
					url: image
				}
				cb(null, imageObj)
			}
		], function (err, image) {
			if (err) return next(err);

			global.server.emit("uploaded.file", image);
			req.result = image;
			next();
		});
	},

	getDeal: (req, res, next) => {
		var ip = req.__requestIp;
		var dealId = req.params["dealId"] || req.query["dealId"] || req.body["dealId"];
		Deal.getById(dealId, function (err, deal) {
			if (err) return next(err)
			req.result = deal;
			next()
		})
	},

	getDeals: (req, res, next) => {

		var ip = req.__requestIp;
		var searchParams = _.extend({}, req.params, req.query);
		Deal.search(searchParams, function (err, result) {
			if (err) return next(err)
			req.result = result;
			next();
		})
	},

	updateDeal: (req, res, next) => {
		var ip = req.__requestIp;
		var dealId = req.params["dealId"] || req.query["dealId"] || req.body["dealId"];
		if (req.body["dealId"]) delete req.body["dealId"]

		var updateDealObj = req.body
		if (req.body.location && req.body.location.lon && req.body.location.lat) {
			updateDealObj.location = {
				type: "Point",
				coordinates: [req.body.location.lon, req.body.location.lat]
			}
		}
		updateDealObj.updatedAt = new Date()
		delete updateDealObj._company

		Deal.findOneAndUpdate(
			{
				_id: dealId
			},
			updateDealObj,
			{
				new: true
			},
			function (err, deal) {
				if (err) return next(err);
				req.result = deal
				next()
			}
		)
	},

	createDeal: (req, res, next) => {
		var ip = req.__requestIp;
		req.body.location || (req.body.location = {})
		var dealObj = req.body
		dealObj.location = {
			type: "Point",
			coordinates: [req.body.location.lon, req.body.location.lat]
		}
		dealObj.createdAt = new Date()
		Deal.create(dealObj, function (err, deal) {
			if (err) return next(err);
			req.result = deal
			next()
		})
	},

	emailTest: (req, res, next) => {
		var mail = new Mail({
			from: `user <${ _.get(req, 'session.email', 'anonym') }>`,
			emails: req.body.emails.split(','),
			letter: {
				subject: req.body.subject,
				text: req.body.text
			}
		});
		mail.send(function (err, info) {
			if (err) return next(err);
			req.result = info;
			next();
		});

	},

	emailConfirm: (req, res, next) => {
		var query = req.query;

		var redirectUrl = {
			protocol: config.frontEnd.protocol,
			host: config.frontEnd.host,
			pathname: config.frontEnd.emailConfirmationPath,
			query: {}
		};

		async.waterfall([
			(cb) => {
				EmailConfirmationToken.findOne({
					userId: query.userId,
					token: query.token
				}, (err, confirmation) => {
					if (err) return cb(err)
					if (!confirmation) {
						redirectUrl.query = {
							invalidEmail: true
						}
						return cb(STATUS.emailTokenInvalid);
					}

					if (!!confirmation && !!confirmation.confirmed) { // aleready confirmed
						if (!!confirmation.restored) { // restored previously confirmed email
							redirectUrl.query = {
								restoreEmail: true,
								email: confirmation.email
							}
						} else {
							redirectUrl.query = {
								secondTime: true
							}
						}
						// if (global.config.env == 'production' || global.config.env == 'staging') { // redirect to result page
							return res.redirect(url.format(redirectUrl));
						// }

						redirectUrl.query = {
							invalidEmail: true
						}

						return cb(STATUS.emailTokenInvalid)
					};
					cb(null, confirmation);
				})
			}, (confirmation, cb) => {
				confirmation.set({confirmed: true}).save((err, confirmation) => {
					cb(err, confirmation)
				});
			}, (confirmation, cb) => {
				User.findOne({
					_id: query.userId
				}, (err, user) => {
					cb(err, user)
				})
			}, (user, cb) => {
				var correctEmail = user.pendingEmail || user.email || '';
				User.findOneAndUpdate({
					_id: query.userId
				}, {
					email: correctEmail,
					pendingEmail: null,
					isEmailConfirmed: true
				}, {
					new: true
				}, (err, user) => {
					cb(err, user)
				})
			}, (user, cb) => {
				if (!user) return cb(STATUS.userNotFound)
				//update all users sessions in background
				Session.update({
					email: user.email
				}, {
					isEmailConfirmed: true
				}, (err, res) => {
					// return current users updated session
					Session.create(user, (err, session) => {
						cb(err, session)
					});
				})
			}
		], (err, session) => {
			if (err) {
				redirectUrl.query = {
				invalidEmail: true
			}
			return res.redirect(url.format(redirectUrl));
			}

			var isConfirmed = !err && !!session;
			var publicSession = session.getPublicObject();

			redirectUrl.query = {
				confirmed: isConfirmed,
				userId: _.get(publicSession, 'user.userId', ""),
			}
			if (session.fullname) redirectUrl.query.username = session.fullname.split(' ')[0];

			global.server.emit("push.confirm_email", {
                userId: _.get(publicSession, 'user.userId', ""),
            });

			return res.redirect(url.format(redirectUrl));
		})
	},

	restoreOriginalEmail: (req, res, next) => {
		async.waterfall([
			(cb) => {
				EmailConfirmationToken.findOne({
					userId: req.session.userId,
					confirmed: false
				}, (err, confirmation) => {
					if (err) return cb(err)
					cb(null, confirmation);
				})
			}, (confirmation, cb) => {
				confirmation.set({confirmed: true, restored:true}).save((err, confirmation) => {
					cb(err, confirmation)
				});
			}, (confirmation, cb) => {
				User.findOneAndUpdate({
					_id: req.session.userId
				}, {
					isEmailConfirmed: true,
					pendingEmail: null
				}, {
					new: true
				}, (err, user) => {
					cb(err, user)
				})
			}, (user, cb) => {
				if (!user) return cb(STATUS.userNotFound)
				//update all users sessions in background
				Session.update({
					email: user.email
				}, {
					isEmailConfirmed: true,
					pendingEmail: null
				}, (err, res) => {
					// return current users updated session
					Session.create(user, (err, session) => {
						cb(err, session)
					});
				})
			}
		], (err, session) => {
			if (err) return next(err);
			let publicSession = session.getPublicObject();

			req.result = publicSession;
			next();
		})
	},

	restorePassword: (req, res, next) => {
		var login = req.query.login || req.body.login

		async.waterfall([
			(cb) => {
				User.findOne({
					$or: [
						{nickname: {$eq: login}},
						{email: {$eq: login}}
					]
				}, (err, user) => {
					if (err) return cb(err);
					if (!user) return cb(STATUS.userNotFound);
					cb(null, user)
				})
			}, (user, cb) => {
				crypto.randomBytes(48, (ex, buf) => {
					let token = buf.toString('hex');
					cb(null, {user, token})
				});

			}, ({user, token}, cb) => {
				RestorePasswordToken.findOne({
					userId: String(user._id),
				}, (err, restorePassToken) => {
					if (err) return cb(err);
					if (restorePassToken) {
						restorePassToken.update({
							createdAt: new Date()
						})
						return cb(null, {user, restorePassToken});
					}
					// restorePassToken not found, create new
					RestorePasswordToken.create({
						userId: String(user._id),
						token: token,
						createdAt: new Date()
					}, (err, restorePassToken) => {
						cb(err, {user, restorePassToken})
					})
				})
			}, ({user, restorePassToken}, cb) => {
				let iosProtocol = (global.config.env == 'production') ? 'comvicinityvicinity-app' : 'comvicinityvicinity-panda-app';

				let link = url.format({
					protocol: iosProtocol,
					host: "//",
					pathname: "",
					query: {
						token: restorePassToken.token,
						login: login
					}
				});

				let fallbackLink = url.format({
					protocol: config.frontEnd.protocol,
					host: config.frontEnd.host,
					pathname: config.frontEnd.restorePasswordPath,
					query: {
						token: restorePassToken.token,
						login: login,
						protocol: iosProtocol
					}
				});

				let mail = new Mail({
					emails: [user.email],
					letter: {
						subject: 'CHANGE PASSWORD on Vicinity - The Local Network',
						html: templates.emails.resetPassword({link, fallbackLink})
					}
				});
				mail.send((err, info) => {
					if (err) return next(err);
					console.log('restorePassword email sending result')
					console.log(info)
					cb(err, user.getPublicObject());
				});
			}
		], (err, result) => {
			if (err) return next(err)
			req.result = {}
			next();
		})
	},

	removeUsers: (req, res, next) => {
		let ids = _.get(req, 'body.userIds.split') ? req.body.userIds.split(',') : []
		User.__remove(ids, (err, removeResult) => {
			if (err) return next(err);
			req.result = removeResult;
			next();
		})
	},

	createReport: (req, res, next) => {
		var ip = req.__requestIp;
		if (req.session.userStatus == "Locked") return next(STATUS.userLocked);

		var obj = {
			ts: new Date(),
		    senderId: req.session.userId,
		    userId: req.body.userId,
		    reportType: req.body.reportType,
		    target: req.body.target, // user | post | comment
		    postId: req.body.postId,
			commentId: req.body.commentId
		};

		Report.create(obj, function (err, report) {
			if (err) return next(err);
			report.getPublicObject(req, function (_err, publicReport) {
				req.body.user = req.session;
				req.body.text = publicReport.reportText;
				var letter = new Letter(req.body);
				var mail = new Mail({
					emails: global.config.mail.reportEmails,
					gmail: global.config.mail.gmail,
					letter: {
						subject: publicReport.reportText,
						text: letter.text
					},
					from: req.session.email
				});
				mail.send(function (err) {
					if (err) return next(err);
					req.result = publicReport;
					next();
				});
			});
		});
	},

	checkUsername: (req, res, next) => {
		var nickname = req.body.username;
		User.findOne({
			"nickname": nickname
		}, (err, user) => {
			if (user) return next(STATUS.nicknameExists);
			next();
		})
	},

	getNotificationtable: (req, res, next) => {

	Notification.find({},function(err, res) {

		var Excel = require('exceljs');
		var workbook = new Excel.Workbook();
		var sheet = workbook.addWorksheet('My Sheet');

		sheet.columns = [
			{ header: '_id', key: '_id', width: 10 },
			{ header: 'text', key: 'text', width: 10 },
			{ header: 'eventText', key: 'eventText', width: 10 },
			{ header: 'postId', key: 'postId', width: 10 },
			{ header: 'actorId', key: 'actorId', width: 10 },
			{ header: 'notifType', key: 'notifType', width: 10 },
			{ header: '__v', key: '__v', width: 10 },
			{ header: 'ts', key: 'ts', width: 10 }
		];

		var dataRes = [];
		res.forEach( function(element, index, array) {
			dataRes.push({
				_id: element._id,
				text: element.text,
				eventText: element.eventText,
				postId: element.postId,
				actorId: element.actorId,
				notifType: element.notifType,
				__v: element.__v,
				ts: element.ts.toString()
			});
		});

		sheet.addRows(dataRes);


		workbook.xlsx.writeFile('data.xlsx')
			.then(function() {
				// done
			});

		var mongoXlsx = require('mongo-xlsx');

		var model = mongoXlsx.buildDynamicModel(dataRes);

		console.log('model',model);

		/* Generate Excel */
		mongoXlsx.mongoData2Xlsx(dataRes, model, function(err, data) {
			console.log('File saved at:', data.fullPath);
		});
	});
		}
};

module.exports = Handlers;
