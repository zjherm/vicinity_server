var bunyan = require('bunyan');
//var sms = require('../wrappers/sms');

module.exports = function () {
    var logger = bunyan.createLogger({
        name: 'HELLO'
    });

    var logLevel = global.config.env === 'production' ? "ERROR" : "TRACE";
    logger.level(logLevel);

    var error = logger.error;

    logger.error = function () {

        if (global.config.env === "production") {
            //sms.common(global.config.adminPhone, JSON.stringify(arguments), function () {});
        }

        error.apply(logger, Array.prototype.slice.call(arguments));

    };

    logger.err = function () {
        var args = Array.prototype.slice.call(arguments);
        args.push(new Error().stack);
        logger.error.apply(logger, args);
    };

    logger.middleware = function (req, res, next) {

        logger.warn(req.method, req.url);
        next();

    };

    return logger;
};
