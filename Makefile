REPORTER = spec


test:
	@NODE_ENV=test NODE_TLS_REJECT_UNAUTHORIZED=0 ./node_modules/mocha/bin/mocha \
    		--require should \
    		--reporter $(REPORTER) \
    		--timeout 5000 \
    		--growl \
    		tests/$(test).js

all:
	@NODE_ENV=test NODE_TLS_REJECT_UNAUTHORIZED=0 ./node_modules/mocha/bin/mocha \
		--require should \
		--reporter $(REPORTER) \
		--timeout 5000 \
		--growl \
		tests/*.js